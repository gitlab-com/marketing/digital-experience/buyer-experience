/label ~"dex-approval::2-standard"

<!---
This MR will have `dex-approval::2-standard` automatically applied, but please update it according the change described in Marketing Site Approval Process. 

https://handbook.gitlab.com/handbook/marketing/digital-experience/engineering/marketing-site-approval-process/ 
--->
/label ~"dex-approval::2-standard"

## 1. Change Summary
[Describe what is changing, why, and the impact]
- Related [ISSUE]

<!-- Draft content from Contentful used
#### Contentful
- [ ] Use Contentful Preview API
- Contentful Entry: [LINK]
-->

#### Review App
| Production | Review app | Screenshot |
| --- | ----------- |----------- |
| https://about.gitlab.com/ | WIP | Screenshot|

## 2. QA Checklist
[Provide concise steps to test the changes]

- [ ] Accessibility: Axe tools run and issues addressed
- [ ] Cross-browser compatibility: Works on Safari, Chrome, and Firefox
- [ ] Analytics and SEO: Compatible with Google Analytics and SEO tools
- [ ] Homepage, pricing, GitLab Duo checked for regressions
- [ ] Localization checked for regressions

## 3. Deployment Steps
[List key deployment steps or configurations]



