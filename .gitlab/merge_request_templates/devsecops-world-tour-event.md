**Build Variables:**
- [ ] Use Contentful Preview API

## Step 1: What is changing in this MR? 

1. DevSecOps World Tour [City] landing page https://about.gitlab.com/events/devsecops-world-tour/CITY
2. DevSecOps World Tour [Leadership] landing page https://about.gitlab.com/events/devsecops-world-tour/CITY/partner-leadership-summit/ (not all WT events have this)
3. Event added to  DevSecOps World Tour  landing page @ https://about.gitlab.com/events/devsecops-world-tour/
4. Event added to primary event landing page @ https://about.gitlab.com/events/

| CTF Entry | Review App | 
| ------ | ------ |
| City CTF Entry Title(link) | Review app URL |
| [DevSecOps World Tour landing page cards](https://app.contentful.com/spaces/xz1dnu24egyd/environments/master/entries/5UQ8gTcDh8nEEgd3J95qO3?focusedField=internalName) | https://BRANCH-NAME.about.gitlab-review.app/events/devsecops-world-tour/ |
| City Event Card for Landing Page (link)      |  https://BRANCH-NAME.about.gitlab-review.app/events/      |

## Step 2: Creator & Reviewer Checklist:

- [ ] I, the Assignee, have run [Axe tools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd) on any updated pages, and fixed the relevant accessibility issues
- [ ] These changes meet a specific OKR or item in our Quarterly Plan.
- [ ] These changes work on both Safari and Chrome.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes comply with [SEO best practices](https://about.gitlab.com/handbook/marketing/inbound-marketing/search-marketing/seo-content-manual/#digital-content-publication-seo-checklist).
- [ ] Changes with links meet the [data attributes requirement](https://handbook.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#click-tracking) for Google Analytics tracking.
- [ ] Changes with interactive elements that aren't links (video, drop-down menu, form, etc.) sends a [dataLayer event](https://handbook.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#data-layer-tracking) for Google Analytics tracking.
- [ ] These changes have been documented as expected.
- [ ] Marketing ops has reviewed any new Marketo forms. [Link to documentation](https://handbook.gitlab.com/handbook/marketing/marketing-operations/campaign-operations/#working-with-us).

## Step 3: Releasing

- [ ] Stakeholder signoff 
- [ ] If there are code changes, merge this MR
- [ ] Turn off webhook, publish all entries and they're references
- [ ] Verify release in production