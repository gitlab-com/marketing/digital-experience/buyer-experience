# Navigation Release
To be used for major and minor navigation releases.

## 1. Change Summary
[Describe what is changing, why, and the impact]
- Related [ISSUE]
- Related [NAVIGATION_RELEASE_MR]

<!-- Draft content from Contentful used
#### Contentful
- [ ] Use Contentful Preview API
- Contentful Navigation Entry: [NAVIGATION LINK]
- Contentful Footer Entry: [NAVIGATION LINK]
-->

#### Review App
| Production | Review app | Screenshot |
| --- | ----------- |----------- |
| https://about.gitlab.com/ | WIP | Screenshot|

## 3. QA Checklist
_Chrome_
- [ ] Test a link in each dropdown of the **desktop** header (Especially login/free trial buttons)
- [ ] Test a link each section of the **desktop** footer (Especially Edit in IDE/Page source links)
- [ ] Test a link in each section of the **mobile** header
- [ ] Test a link in each section of the **mobile** footer
- [ ] Test search on desktop
- [ ] Test search on mobile
- [ ] Test tabbing through header links
- [ ] Test tabbing through footer links

_Safari_
- [ ] Test a link in each dropdown of the **desktop** header (Especially login/free trial buttons)
- [ ] Test a link each section of the **desktop** footer (Especially Edit in IDE/Page source links)
- [ ] Test a link in each section of the **mobile** header
- [ ] Test a link in each section of the **mobile** footer
- [ ] Test search on desktop
- [ ] Test search on mobile
- [ ] Test tabbing through header links
- [ ] Test tabbing through footer links

_Firefox_
- [ ] Test a link in each dropdown of the **desktop** header (Especially login/free trial buttons)
- [ ] Test a link each section of the **desktop** footer (Especially Edit in IDE/Page source links)
- [ ] Test a link in each section of the **mobile** header
- [ ] Test a link in each section of the **mobile** footer
- [ ] Test search on desktop
- [ ] Test search on mobile
- [ ] Test tabbing through header links
- [ ] Test tabbing through footer links

## 3. Deployment Steps
[List key deployment steps or configurations]




