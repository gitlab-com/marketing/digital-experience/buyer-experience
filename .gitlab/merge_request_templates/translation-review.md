/label ~"dex-L10N"

**Build Variables:**
- [ ] Use Contentful Preview API

- If pages are built in Contentful, **avoid custom JSON**. This is important.
  - Custom JSON creates delays in the translation process, which slows down the velocity of our progress
- Make sure to link source entries from different locales to their English version. This is crucial for the continuous translation process. If you create new entries in new locale, please link them to the source entry.
- [Overflowing text](https://gitlab.com/gitlab-com/localization/localization-team/-/issues/171) due to the length of different character averages among locales

Handbook page: https://handbook.gitlab.com/handbook/marketing/localization/

**Reviewer Checklist:**
- [ ] I, the Assignee, have run [Axe tools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd) on any updated pages, and fixed the relevant accessibility issues
- [ ] These changes work on both Safari and Chrome.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes comply with [SEO best practices](https://about.gitlab.com/handbook/marketing/inbound-marketing/search-marketing/seo-content-manual/#digital-content-publication-seo-checklist).
- [ ] Changes with links meet the [data attributes requirement](https://handbook.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#click-tracking) for Google Analytics tracking.
- [ ] Changes with interactive elements that aren't links (video, drop-down menu, form, etc.) sends a [dataLayer event](https://handbook.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#data-layer-tracking) for Google Analytics tracking.
- [ ] These changes have been documented as expected.
- [ ] Marketing ops has reviewed any new Marketo forms. [Link to documentation](https://handbook.gitlab.com/handbook/marketing/marketing-operations/campaign-operations/#working-with-us).

**Translator Checklist:**
- [ ] Closes `place issue link here`
- [ ] Direct link to review app page where changes are made `place review app URL`


