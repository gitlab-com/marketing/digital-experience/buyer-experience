# GitLab Core Marketing Site  

This is one of the repositores generating the GitLab's Marketing Site. It is maintained by the Digital Experience team at GitLab. Learn more about our full marketing site architecture in the handbook [here](https://handbook.gitlab.com/handbook/marketing/digital-experience/#marketing-site-deployment-process). It's built with [Nuxt.js](https://nuxtjs.org/) using the [create-nuxt-app](https://nuxtjs.org/docs/2.x/get-started/installation#using-create-nuxt-app) workflow. We selected the [TypeScript](https://www.typescriptlang.org/) option, so when you look up reference materials about Nuxt, be sure to follow any TypeScript related instructions.

### Table of Contents

- [Quick Start](#quick-start)
    - [Dependencies](#dependencies)
    - [Install & Build](#install-and-build)
    - [Run](#run)
- [Developer Documentation](#developer-documentation)
- [Supporting Documentation](#supporting-documentation)
- [Issues](#issues)

## <a name="quick-start">Quick Start</a>

### <a name="dependencies">Dependencies</a>

1. [Node](https://nodejs.org/en/). Check `.nvmrc` for the current required Node version.
1. [Yarn](https://yarnpkg.com/)
1. [Access to Contentful Data Source](https://app.contentful.com/spaces/xz1dnu24egyd/entries)

#### Content Data Source

The Marketing site operates on a dual-content sourcing strategy. English content is seamlessly pulled from a trusted third-party vendor, known as [Contentful](https://www.contentful.com/), offering dynamic and up-to-date content management capabilities.

Make sure you have CTF_PREVIEW_ACCESS_TOKEN as well as USE_CONTENTFUL_PREVIEW_API = 'true' within your environment variables to view in-draft content locally. Published content can be viewed locally using the `CTF_CDA_ACCESS_TOKEN` and by removing or commenting out `USE_CONTENTFUL_PREVIEW_API`.

In contrast, localized content, tailored to specific regional needs and preferences, is diligently managed within our own repository. This localized content resides in the repository's dedicated `content` directory and is efficiently retrieved using the [Content Module](https://nuxt.com/modules/content) integrated into the Nuxt framework.

For an in-depth exploration of our content within Contentful, including the ability to view and make edits, you can access the content via our Contentful space by visiting the [Contentful site](https://app.contentful.com/spaces/xz1dnu24egyd/entries). This provides a comprehensive interface for seamless content management and real-time updates.

### <a name="install-and-build">Install & Build</a>

Clone the repository into your machine and compile with yarn.

```bash
# Clone
git clone https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience.git
cd buyer-experience

# install dependencies
$ yarn install
```

### <a name="run">Run</a>

Inside the project's root directory, we have various commands set up for running and generating the project locally.

```bash
# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project inside /dist
$ yarn generate
```

## <a name="developer-documentation">Developer Documentation</a>

Check out all of our documentation for development and deployments in the [/docs directory](/docs/development.md).

## <a name="supporting-documentation">Supporting Documentation</a>

We have supporting user documentation for our features in the handbook. Here are some hightlights: 

- [Digital Experience Team](https://handbook.gitlab.com/handbook/marketing/digital-experience/)
- [Accessibility](https://handbook.gitlab.com/handbook/marketing/digital-experience/accessibility/)
- [Analytics](https://handbook.gitlab.com/handbook/marketing/digital-experience/analytics/)
- [Contentful CMS](https://handbook.gitlab.com/handbook/marketing/digital-experience/contentful-cms/)
- [Engineering A/B tests](https://handbook.gitlab.com/handbook/marketing/digital-experience/engineering/engineering-ab-tests/)
- [Localization](https://handbook.gitlab.com/handbook/marketing/digital-experience/engineering/localization/)
- [Marketing Cookies](https://handbook.gitlab.com/handbook/marketing/digital-experience/engineering/marketing-cookie/)
- [Navigation Repository](https://handbook.gitlab.com/handbook/marketing/digital-experience/engineering/navigation-repository/)
- [OneTrust](https://handbook.gitlab.com/handbook/marketing/digital-experience/onetrust/)

## <a name="issues">Issues</a>

If you find a bug, please [open an issue](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/?sort=closed_at_desc&state=opened&first_page_size=100) and use the `marketing-site-bug` issue template.


