import { CtfEntry, CtfImage } from '../models';
import { LOCALES, LOCALIZATION_TAGS_MAPPER } from '../common/constants';

export function capitalizeFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function numberToDigitString(numbr: number) {
  return `${numbr > 9 ? numbr : `0${numbr}`}`;
}

/**
 * Converts a locale code to a case sensitive locale code (i.e en-us to en-US)
 * @param localeCode
 */
export function convertToCaseSensitiveLocale(localeCode: string) {
  const parts = localeCode.split('-');

  if (parts.length === 2) {
    return `${parts[0].toLowerCase()}-${parts[1].toUpperCase()}`;
  }

  return localeCode.toLowerCase();
}

export function getUrlFromContentfulImage(
  contentfulImage: CtfEntry<CtfImage>,
  width?: number,
  height?: number,
) {
  const baseUrl = contentfulImage?.fields?.file?.url;

  if (!baseUrl) return null;

  const params = new URLSearchParams();
  if (width) params.append('w', width.toString());
  if (height) params.append('h', height.toString());

  return `${baseUrl}?${params.toString()}`;
}

export function getAvailableLanguagesFromTags(tags: Array<any>) {
  const defaultLocale = LOCALES.DEFAULT;

  if (!tags?.length) {
    return [defaultLocale];
  }

  const availableLanguages = tags
    .map(({ sys }) => {
      const language = LOCALIZATION_TAGS_MAPPER.find(
        (item) => item.id === sys.id,
      );

      return language?.locale;
    })
    .filter((item) => item);

  return !availableLanguages.length ? [defaultLocale] : availableLanguages;
}

export function getTagFromLocale(locale: string): string {
  const currentLocale = LOCALIZATION_TAGS_MAPPER.find(
    (item) => item.locale === locale.toLowerCase(),
  );

  if (!currentLocale) return null;

  const { id } = currentLocale;

  return id;
}

export function kebabToTitleCase(kebabStr: string) {
  return kebabStr
    .split('-')
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
    .join(' ');
}
