---
  title: Warum GitLab Premium?
  description: Optimiere die Produktivität und Koordination in deinem Team mit GitLab Premium.
  side_menu:
    anchors:
      text: Auf dieser Seite
      data:
      - text: Übersicht
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Zusammenfassung
          href: "#wp-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Schlüssellösungen
          href: "#wp-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Kunden-Fallstudien
          href: "#wp-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: ROI-Rechner
        href: "#wp-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Premium-Funktionen
        href: "#wp-premium-features"
        data_ga_name: premium-features
        data_ga_location: side-navigation
        nodes:
        - text: Schnellere Code Reviews
          href: "#wp-faster-code-reviews"
          data_ga_name: faster-code-reviews
          data_ga_location: side-navigation
        - text: Erweitertes CI/CD
          href: "#wp-advanced-ci-cd"
          data_ga_name: advanced-ci-cd
          data_ga_location: side-navigation
        - text: Enterprise Agile Delivery
          href: "#wp-enterprise-agile-planning"
          data_ga_name: nterprise-agile-planning
          data_ga_location: side-navigation
        - text: Release-Kontrollen
          href: "#wp-release-controls"
          data_ga_name: release-controls
          data_ga_location: side-navigation
        - text: Selbstverwaltete Zuverlässigkeit
          href: "#wp-self-managed-reliability"
          data_ga_name: self-managed-reliability
          data_ga_location: side-navigation
        - text: Weitere Premium-Funktionen
          href: "#wp-other-premium-features"
          data_ga_name: other-premium-features
          data_ga_location: side-navigation
      - text: Kontakt aufnehmen
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Nächste Schritte
      data:
      - text: Jetzt Premium kaufen
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-premium
        data_ga_location: side-navigation
      - text: Mehr über Ultimate
        href: /pricing/ultimate/
        variant: secondary
        icon: true
        data_ga_name: learn-about-ultimate
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wp-summary
      title: Warum Premium?
      subtitle: Ideal zur Verbesserung der Produktivität und Koordination deines Teams
      text: "GitLab\_Premium ist sowohl als SaaS- als auch als Self-Managed-Bereitstellung verfügbar und trägt mit den folgenden Funktionen dazu bei, die Produktivität und Zusammenarbeit von Teams zu verbessern:"
      list: 
        - Schnellere Code Reviews
        - Release-Kontrollen
        - Priority-Support
        - Erweitertes CI/CD
        - Live-Upgrade-Unterstützung
        - Enterprise Agile Delivery
        - Technische Kundenbetreuung für berechtigte Kund(inn)en
        - Hochverfügbarkeit und Notfallwiederherstellung für Self-Managed-Instanzen
      cta: 
        text: Alle Funktionen vergleichen
        url: /pricing/feature-comparison/
      buttons:
        - variant: primary
          icon: false
          text: Jetzt Premium kaufen
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: buy-premium-now
          data_ga_location: body
        - variant: secondary
          icon: true
          text: Mehr über Ultimate
          url: /pricing/ultimate/
          data_ga_name: learn-about-ultimate
          data_ga_location: body
  - name: guest-calculator
    data:
      id: wp-guest-calculator
      title: Berechne die Kosten für dein Unternehmen
      input_title: GitLab bietet unbegrenzt kostenlose Gastbenutzer(innen) für Ultimate-Abonnements
      caption: |
        *Alle Abonnements werden jährlich in Rechnung gestellt. Die aufgeführten Preise können lokalen Steuern und Quellensteuern unterliegen. Beim Kauf über einen Partner oder Wiederverkäufer können die Preise variieren. Weitere Informationen findest du auf unserer [Preisseite](/pricing/){data-ga-name="pricing" data-ga-location="guest calculator"}.
      seats:
        title: Anzahl der GitLab-Plätze
        tooltip: GitLab-Plätze sind Personen mit Reporter-, Entwickler-, Betreuer- oder Eigentümerberechtigungen. Gastbenutzer(innen) besetzen keinen Platz in GitLab Ultimate. Erfahre <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined" data-ga-name="seat usage documentation" data-ga-location="user calculator tooltip">hier</a> mehr über die Nutzung von Plätzen.
      guests:
        title: Anzahl der Gastbenutzer(innen)
        tooltip: Gastbenutzer(innen) können Tickets erstellen und zuweisen, bestimmte Analysen anzeigen, optional Code anzeigen und vieles mehr. <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions" data-ga-name="project members documentation" data-ga-location="user calculator tooltip">Hier</a> erfährst du mehr.
      premium:
        title: Monatliche Kosten für Premium*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Premium kaufen
      ultimate:
        title: Monatliche Kosten für Ultimate*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Ultimate kaufen
  - name: 'by-solution-value-prop'
    data:
      id: wp-key-solutions
      light_background: true
      small_margin: true
      large_card_on_bottom: true
      title: GitLab Premium hilft dir bei folgenden Punkten
      cards:
        - icon:
            name: increase
            alt: Erhöhen
            variant: marketing
          title: Betrieblichen Effizienz steigen
          description: GitLab Premium führt Funktionen ein, mit denen Unternehmen Team-, Projekt- und Gruppentrends analysieren können, um Muster aufzudecken und konsistente Standards zur Verbesserung der Gesamtproduktivität zu implementieren.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Geschwindigkeit
          title: Bessere Produkte schneller liefern
          description: Mit erweitertem CI/CD und schnelleren Code-Reviews hilft GitLab Premium dir dabei, komplexe Anwendungs-Pipelines besser zu erstellen, zu warten, bereitzustellen und zu überwachen, um Produkte schneller zu liefern.
        - icon:
            name: lock-alt-5
            alt: Schloss
            variant: marketing
          title: Sicherheits- und Compliance-Risiken reduzieren
          description: Die Release-Kontrollen in GitLab Premium stellen sicher, dass Teams qualitativ hochwertigen und sicheren Code liefern.
  - name: 'education-case-study-carousel'
    data:
      id: 'wp-customer-case-studies'
      case_studies:
        - logo_url: /nuxt-images/logos/nvidia-logo.svg
          text: Wie GitLab Geo die Innovation von NVIDIA unterstützt
          img_url: /nuxt-images/blogimages/nvidia.jpg
          case_study_url: /customers/nvidia/
          data_ga_name: Nvidia case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
          img_url: /nuxt-images/blogimages/fujitsu.png
          text: Fujitsu Cloud Technologies verbessert die Bereitstellungsgeschwindigkeit und funktionsübergreifende Workflows mit GitLab
          case_study_url: /customers/fujitsu/
          data_ga_name: fujitsu case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/credit-agricole-logo-1.png
          img_url: /nuxt-images/blogimages/creditagricole-cover-image.jpg
          text: Wie die Crédit Agricole Corporate & Investment Bank (CACIB) ihren globalen Workflow mit GitLab verändert hat
          case_study_url: /customers/credit-agricole
          data_ga_name: credit-agricole case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/esa-logo.svg
          img_url: /nuxt-images/blogimages/ESA_case_study_image.jpg
          text: Wie die Europäische Weltraumorganisation GitLab nutzt, um sich auf Weltraummissionen zu konzentrieren
          case_study_url: /customers/european-space-agency
          data_ga_name: european-space-agency case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/sopra_steria-logo.wine.svg
          img_url: /nuxt-images/blogimages/soprasteria.jpg
          text: Wie GitLab zum Eckpfeiler der digitalen Unterstützung für Sopra Steria wurde
          case_study_url: /customers/sopra_steria
          data_ga_name: sopra_steria case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/hemmersbach_logo.svg
          img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
          text: Hemmersbach hat seine Build-Kette neu organisiert und die Build-Geschwindigkeit um das 59-fache erhöht
          case_study_url: /customers/hemmersbach
          data_ga_name: hemmersbach case study
          data_ga_location: case study carousel
  - name: roi-calculator-block
    data:
      id: wp-roi-calculator
      header:
        gradient_line: true
        title:
          text: ROI-Rechner
          anchor: wp-roi-calculator
        subtitle: Wie viel kostet dich deine Toolchain?
      calc_data_src: de-de/calculator/index
  features_block:
    id: wp-premium-features
    tier: premium
    header:
      gradient_line: true
      title:
        text: Premium-Funktionen
        anchor: wp-premium-features
        button:
          text: Alle Funktionen vergleichen
          data_ga_name: Compare all features
          data_ga_location: body
          url: /de-de/pricing/feature-comparison/
    pricing_themes:
      - id: wp-faster-code-reviews
        theme: Schnellere Code Reviews
        text: stellen eine hohe Codequalität in allen Teams durch nahtlose Code-Review-Workflows sicher.
        link:
          text: Mehr erfahren
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/development/code_review.html
      - id: wp-advanced-ci-cd
        theme: Erweitertes CI/CD
        text: ermöglicht es dir, komplexe Pipelines zu erstellen, zu warten, bereitzustellen und zu überwachen.
        link:
          text: Mehr erfahren
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: /solutions/continuous-integration/
      - id: wp-enterprise-agile-planning
        theme: Enterprise Agile Delivery
        text: unterstützt dich bei der Planung und Verwaltung deiner Projekte, Programme und Produkte mit integriertem Support für Agilität.
        link:
          text: Mehr erfahren
          data_ga_name: Agile Planning learn more
          data_ga_location: body
          url: /solutions/agile-delivery/
      - id: wp-release-controls
        theme: Release-Kontrollen
        text: stellt sicher, dass Teams qualitativ hochwertigen und sicheren Code liefern.
        link:
          text: Mehr erfahren
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
      - id: wp-self-managed-reliability
        theme: Selbstverwaltete Zuverlässigkeit
        text: stellt Notfallwiederherstellung, Hochverfügbarkeit und Lastausgleich für deine selbstverwaltete Bereitstellung sicher.
        link:
          text: Mehr erfahren
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/administration/reference_architectures/#traffic-load-balancer
      - id: wp-other-premium-features
        text: Weitere Premium-Funktionen