---
  title: Platform
  description: Learn more about how the GitLab platform can help teams collaborate and build software faster.
  hero:
    note: The most comprehensive
    header: AI-powered
    sub_header: DevSecOps Platform
    description: Deliver better software faster with one platform for your entire software delivery lifecycle.
    image:
      src: /nuxt-images/platform/loop-shield-duo.svg
      alt: The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).
    button:
      text: Get Free Trial
      href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/platform&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: Learn about pricing
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero
  table_data:
    - name: "Planning"
      link: "https://about.gitlab.com/solutions/visibility-measurement/"
      icon: "plan-alt-2"
      features:
        - name: "DevOps Reports"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#devops-reports"
        - name: "DORA Metrics"
          link: "https://about.gitlab.com/solutions/value-stream-management/dora/"
        - name: "Value Stream Management"
          link: "https://about.gitlab.com/solutions/value-stream-management/"
        - name: "Value Stream Forecasting"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#value-stream-forecasting"
        - name: "Service Desk"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#service-desk"
        - name: "Wiki"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#wiki"
        - name: "Portfolio Management"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#portfolio-management"
        - name: "Team Planning"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#team-planning"
        - name: "Generate issue description"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#generate-issue-description"
        - name: "Discussion Summary"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#discussion-summary"
        - name: "Design Management"
          link: "/solutions/visibility-measurement/#team-planning"
      replacement:
        text: Replacement for
        product: Jira
    - name: "Source Code Management"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      icon: "cog-code"
      features:
        - name: "Remote Development"
          link: "https://about.gitlab.com/solutions/delivery-automation/#remote-development"
        - name: "Source Code Management"
          link: "https://about.gitlab.com/solutions/source-code-management/"
        - name: "Web IDE"
          link: "https://about.gitlab.com/solutions/delivery-automation/#web-ide"
        - name: "GitLab CLI"
          link: "https://about.gitlab.com/solutions/delivery-automation/#gitlab-cli"
        - name: "Code Review Workflow"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-review-workflow"
        - name: "Code Suggestions"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-suggestions"
        - name: "Code Explanation"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-explanation"
        - name: "Code Review Summary"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-review-summary"
        - name: "Test Generation"
          link: "https://about.gitlab.com/solutions/delivery-automation/#test-generation"
        - name: "Code Refactorization"
          link: "/solutions/source-code-management/#capabilities"
        - name: "GitLab Duo for the CLI"
          link: "/solutions/source-code-management/#capabilities"
      replacement:
        text: Replacement for
        product: GitHub
    - name: "Continuous Integration"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      icon: "automated-code-alt"
      features:
        - name: "Secrets Management"
          link: "https://about.gitlab.com/solutions/delivery-automation/#secrets-management"
        - name: "Review Apps"
          link: "https://about.gitlab.com/solutions/delivery-automation/#review-apps"
        - name: "Code Testing and Coverage"
          link: "https://about.gitlab.com/solutions/delivery-automation/#code-testing-and-coverage"
        - name: "Merge Trains"
          link: "https://about.gitlab.com/solutions/delivery-automation/#merge-trains"
        - name: "Suggested Reviewers"
          link: "https://about.gitlab.com/solutions/delivery-automation/#suggested-reviewers"
        - name: "Merge Request Summary"
          link: "/solutions/delivery-automation/#merge-requests"
        - name: "Root Cause Analysis"
          link: "https://about.gitlab.com/solutions/delivery-automation/#root-cause-analysis"
        - name: "Discussion Summary"
          link: "https://about.gitlab.com/solutions/delivery-automation/#discussion-summary"
        - name: "Merge Commit Message Generation"
          link: "/solutions/delivery-automation/#merge-requests"
        - name: "Pipeline Composition and Component Catalog"
          link: "/solutions/delivery-automation/#cicd-components"
      replacement:
        text: Replacement for
        product: Jenkins 
    - name: "Security"
      link: "https://about.gitlab.com/solutions/security-compliance/"
      icon: "secure-alt-2"
      features:
        - name: "Container Scanning"
          link: "https://about.gitlab.com/solutions/security-compliance/#container-scanning"
        - name: "Software Composition Analysis"
          link: "https://about.gitlab.com/solutions/security-compliance/#software-composition-analysis"
        - name: "API Security"
          link: "https://about.gitlab.com/solutions/security-compliance/#api-security"
        - name: "Coverage-guided Fuzz Testing"
          link: "https://about.gitlab.com/solutions/security-compliance/#fuzz-testing"
        - name: "DAST"
          link: "https://about.gitlab.com/solutions/security-compliance/#dast"
        - name: "Code Quality"
          link: "https://about.gitlab.com/solutions/security-compliance/#code-quality"
        - name: "Secret Detection"
          link: "https://about.gitlab.com/solutions/security-compliance/#secret-detection"
        - name: "SAST"
          link: "https://about.gitlab.com/solutions/security-compliance/#sast"
        - name: "Vulnerability Explanation"
          link: "https://about.gitlab.com/solutions/security-compliance/#vulnerability-explanation"
        - name: "Vulnerability Resolution"
          link: "https://about.gitlab.com/solutions/security-compliance/#vulnerability-resolution"
        - name: "GitLab Advisory Database"
          link: "/solutions/security-compliance/#gitlab-advisory-database"
      replacement:
        text: Replacement for
        product: Snyk
    - name: "Compliance"
      icon: "protect-alt-2"
      link: "https://about.gitlab.com/solutions/security-compliance/"
      features:
        - name: "Release Evidence"
          link: "https://about.gitlab.com/solutions/security-compliance/#release-evidence"
        - name: "Compliance Management"
          link: "https://about.gitlab.com/solutions/security-compliance/#compliance-management"
        - name: "Audit Events"
          link: "https://about.gitlab.com/solutions/security-compliance/#audit-events"
        - name: "Software Bill of Materials"
          link: "https://about.gitlab.com/solutions/security-compliance/#software-bill-of-materials"
        - name: "Dependency Management"
          link: "https://about.gitlab.com/solutions/security-compliance/#dependency-management"
        - name: "Vulnerability Management"
          link: "https://about.gitlab.com/solutions/security-compliance/#vulnerability-management"
        - name: "Security Policy Management"
          link: "https://about.gitlab.com/solutions/security-compliance/#security-policy-management"
    - name: "Artifact Registry"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      icon: "package-alt-2"
      features:
        - name: "Virtual Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#virtual-registry"
        - name: "Container Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#container-registry"
        - name: "Helm Chart Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#helm-chart-registry"
        - name: "Package Registry"
          link: "https://about.gitlab.com/solutions/delivery-automation/#package-registry"
        - name: "Model Registry (Beta)"
          link: "/solutions/delivery-automation/#model-registry"
        - name: "Dependency Proxy"
          link: "/solutions/delivery-automation/#package-registry"      
      replacement:
        text: Replacement for
        product: JFrog
    - name: "Continuous Delivery"
      icon: "continuous-delivery-alt"
      link: "https://about.gitlab.com/solutions/delivery-automation/"
      features:
        - name: "Release Orchestration"
          link: "https://about.gitlab.com/solutions/delivery-automation/#release-orchestration"
        - name: "Infrastructure as Code"
          link: "https://about.gitlab.com/solutions/delivery-automation/#infrastructure-as-code"
        - name: "Pages"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#pages"
        - name: "Feature Flags"
          link: "https://about.gitlab.com/solutions/delivery-automation/#feature-flags"
        - name: "Environment Management"
          link: "https://about.gitlab.com/solutions/delivery-automation/#environment-management"
        - name: "Deployment Management"
          link: "https://about.gitlab.com/solutions/delivery-automation/#deployment-management"
        - name: "Auto DevOps"
          link: "https://about.gitlab.com/solutions/delivery-automation/"
      replacement:
        text: Replacement for
        product: Harness
    - name: "Observability"
      icon: "monitor-alt-2"
      link: "https://about.gitlab.com/solutions/visibility-measurement/"
      features:
        - name: "On-call Schedule Management"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#oncall-schedule-management"
        - name: "Incident Management"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#incident-management"
        - name: "Error Tracking"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#error-tracking"
        - name: "Product Analytics Visualization"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#product-analytics-visualization"
        - name: "AI Product Analytics"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#ai-product-analytics"
        - name: "AI Impact Dashboard"
          link: "/solutions/visibility-measurement/#ai-product-analytics"
        - name: "Metrics"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#metrics"
        - name: "Distributed Tracing"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#distributed-tracing"
        - name: "Logs"
          link: "https://about.gitlab.com/solutions/visibility-measurement/#logs"
      replacement:
        text: Replacement for
        product: Sentry
  benefits:
    title: One platform
    subtitle: to empower Dev, Sec, and Ops teams
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    tabs:
      - tabButtonText: Development
        data_ga_name: development
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: AI-powered workflow
              text: Boost efficiency and reduce cycle times of every user with the help of AI in every phase of the software development lifecycle - from planning and code creation to testing, security, and monitoring.
              headerCtas:
                - externalUrl: /gitlab-duo/
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
              ctaHeader: "See it in action:"
              ctas:
                - externalUrl: https://player.vimeo.com/video/855805049
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/894621401
                  text: Code Suggestions
                  data_ga_name: Code Suggestions
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/927753737
                  text: Chat
                  data_ga_name: Chat
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Single application
              text: GitLab brings all DevSecOps capabilities into one application with a unified data store so everything is all in one place.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: GitLab's use of DORA metrics video
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: GitLab's Value Streams Dashboard video
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Enhanced developer productivity
              text: GitLab's single application delivers a superior user experience, which improves cycle time and helps prevent context switching.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925629920
                  text: GitLab's Portfolio Management video
                  data_ga_name: GitLab's Portfolio Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925632272
                  text: GitLab's OKR Management video
                  data_ga_name: GitLab's OKR Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925633691
                  text: Design Uploads to GitLab issues video
                  data_ga_name: Design Uploads to GitLab issues
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Better automation
              text: GitLab's automation tools are more reliable and feature rich, helping remove cognitive load and unnecessary grunt work.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023715
                  text: GitLab's CD Overview video
                  data_ga_name: GitLab's CD Overview
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://docs.gitlab.com/ee/operations/error_tracking.html
                  text: Error tracking documentation
                  data_ga_name: Error tracking documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Incident management documentation
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/2DKPuM0RDt2jdfzIaTroKS/1aeee8ed2e7939523ef89e7046d5a468/iron-mountain.svg
                quote: The vision that GitLab has in terms of tying strategy to scope and to code is very powerful. I appreciate the level of investment they are continuing to make in the platform.
                metrics:
                  - number: $150k
                    text: approximate cost savings per year
                  - number: 20 hours
                    text: saved in onboarding time per project
                author:
                  headshot: https://images.ctfassets.net/xz1dnu24egyd/1G9lo7UyVxvW6m1CTNND9b/7b8b0449fc8ae1299e3b4fc56f4dd363/JasonManoharan.png
                  name: Jason Monoharan.
                  title: VP of Technology.
                  company: Iron Mountain
                cta: 
                  text: Read the study
                  url: '/customers/iron-mountain/'
                  dataGaName: iron mountain case study
                  dataGaLocation: body
          cta:
            title: Harness the power of AI with
            highlight: GitLab Duo
            text: Learn more
            externalUrl: /gitlab-duo/
            data_ga_name: GitLab Duo
            data_ga_location: body
            additionalIcon: true
      - tabButtonText: Security
        data_ga_name: security
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Security is built in, not bolted on
              text: GitLab's security capabilities - such as DAST, fuzz testing, container scanning, and API screening - are integrated end-to-end.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925635707
                  text: Dynamic Application Security Testing (DAST) video
                  data_ga_name: Dynamic Application Security Testing (DAST) video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925676815
                  text: Container scanning video
                  data_ga_name: Container scanning video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925677603
                  text: API security and web API Fuzzing video
                  data_ga_name: API security and web API Fuzzing video
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Compliance and precise policy management
              text: GitLab offers a comprehensive governance solution allowing for separation of duties between teams. GitLab's policy editor allows customized approval rules tailored to each organization's compliance requirements, reducing risk.
              ctas:
                - externalUrl: /solutions/security-compliance/#compliance-management
                  text: Compliance Management documentation
                  data_ga_name: Compliance Management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://player.vimeo.com/video/925679314
                  text: GitLab's Compliance Frameworks video
                  data_ga_name: GitLab's Compliance Frameworks
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925679982
                  text: GitLab's Requirements Management video
                  data_ga_name: GitLab's Requirements Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Security automation
              text: GitLab's advanced automation tools enable velocity with guardrails, ensuring code is automatically scanned for vulnerabilities.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925680640
                  text: GitLab's Security Dashboard video
                  data_ga_name: GitLab's Security Dashboard
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/4mEBdmMQrpjOn1VJ2idd9h/39fbbf0fc290dda417a35d1e437de4a3/hackerone.svg
                quote: GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
                metrics:
                  - number: 7.5x
                    text: faster pipeline time
                  - number: 4 hours
                    text: development time per engineer saved/weekly
                author:
                  name: Mitch Trale
                  title: Head of Infrastructure
                  company: Hackerone
                cta: 
                  text: Read the study
                  url: '/customers/hackerone/'
                  dataGaName: hackerone case study
                  dataGaLocation: body
          cta:
            title: See how to add security scans to your
            highlight: CI pipeline
            text: Launch demo
            data_ga_name: ci pipeline
            data_ga_location: body
            modal: true
            iconName: slp-laptop-video
            demo:
              subtitle: Add security scans to your CI/CD Pipeline
              video:
                url: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
              scheduleButton:
                text: Schedule a custom demo
                href: /sales/
                data_ga_name: demo
                data_ga_location: body
      - tabButtonText: Operations
        data_ga_name: operations
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Scale Enterprise workloads
              text: GitLab easily supports the Enterprise at any scale with the ability to manage and upgrade with nearly zero downtime.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/user/infrastructure/iac/
                  text: Infrastructure as code (IaC) documentation
                  data_ga_name: Infrastructure as code (IaC) documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Incident management documentation
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Unparalleled metrics visibility
              text: GitLab's unified data store provides analytics for the entire software development lifecycle in one place, eliminating the need for additional product integrations.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: GitLab's use of DORA metrics video
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062
                  text: GitLab's Value Streams Dashboard video
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Cloud-native, multi-cloud and legacy support
              text: GitLab provides a complete DevSecOps platform that allows teams to have the same productivity metrics and governance, regardless of your infrastructure mix.
              ctas:
                - externalUrl: /topics/multicloud/
                  text: Multicloud documentation
                  data_ga_name: Multicloud documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: /solutions/gitops/
                  text: GitOps documentation
                  data_ga_name: GitOps documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Lower Total Cost of Ownership
              text: ''
              ctas:
                - description: "Learn how the world's largest defence contractor uses GitLab to shrink toolchains, speed production, and improve security:"
                  externalUrl: /customers/lockheed-martin/
                  text: Lockheed Martin case study
                  data_ga_name: Lockheed Martin case study
                  data_ga_location: body
                  iconName: gl-doc-text
                - description: 'Learn how CARFAX trimmed their DevSecOps toolchain and improved security with GitLab:'
                  externalUrl: /customers/carfax/
                  text: CARFAX case study
                  data_ga_name: CARFAX case study
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/rdWpo3pKJBUFLzDFmTNIE/d69e61550635ebba0556216c44d401a7/forrester-logo.svg
                quote: Improved development and delivery efficiency by over 87%,resulting in over $23 million in savings. GitLab enabled the organizations to drastically reduce time spent across each phase of the entire DevOps lifecycle.
                metrics:
                  - number: $200.5M
                    text: total benefits over three years
                  - number: 427%
                    text: total return on investment (ROI)
                cta: 
                  text: Read the study
                  url: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
                  dataGaName: resources study forrester
                  dataGaLocation: body
          cta:
            title: How much is your toolchain
            highlight: costing you?
            text: Try our ROI Calculator
            externalUrl: /calculator/
            data_ga_name: try our roi calculator
            data_ga_location: body
  video_spotlight:
    header: Want to increase velocity? Consolidate your toolchain today.
    list:
      - text: Improve collaboration
        iconName: gl-check
      - text: Reduce admin burden
        iconName: gl-check
      - text: Increase security
        iconName: gl-check
      - text: Lower total cost of ownership
        iconName: gl-check
      - text: Scale seamlessly
        iconName: gl-check
    blurb: |
      **Don't know where to start?**
      Our sales team can help guide you.
    video:
      title: Why Gitlab
      url: https://player.vimeo.com/video/799236905?h=4eee39a447
      text: Learn More
      data_ga_name: Learn More
      data_ga_location: body
      image: https://images.ctfassets.net/xz1dnu24egyd/7KcXktuJTizzgMwF5o8n66/eddd4708263c41d1afa49399222d8f55/platform-video.png
    button:
      externalUrl: /sales/
      text: Talk to sales
      data_ga_name: sales
      data_ga_location: body
  recognition:
    heading: Industry leaders trust GitLab
    subtitle: GitLab ranks as a G2 Leader across DevOps categories.
    badges:
          - src: https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/b3632635c9e3bf13a2d02ada6374029b/DevOpsPlatforms_Leader_Leader.png
            alt: G2 - Winter 2024 - Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/17EE9DHTQpLyGbJAQslSEk/e04bb1e6e71d895af94d10ffb85471e3/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 - Winter 2024 - Easiest to Use
          - src: https://images.ctfassets.net/xz1dnu24egyd/1SNrMzYNTrOvEnOuNucAMD/be22fc71855a96d9c05627c7085148a9/users-love-us.png
            alt: G2 - Winter 2024 - Users love us
          - src: https://images.ctfassets.net/xz1dnu24egyd/3UhXS8fSwKieddbliKZEGn/9bd6e04223769aa0869470befd78dc62/ValueStreamManagement_BestUsability_Total.png
            alt: G2 - Winter 2024 - Best Usability
          - src: https://images.ctfassets.net/xz1dnu24egyd/7AODDnwkskODEsylwXkBys/908eb94a40d8ab2f64bdcacbd18f35a3/DevOpsPlatforms_Leader_Europe_Leader.png
            alt: G2 - Winter 2024 - Leader Europe
          - src: https://images.ctfassets.net/xz1dnu24egyd/4xk6CmKqUndyuir0m4rF5b/b069fcc665b3b11c2a017fb36aa55130/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 - Winter 2024 - Leader Enterprise
          - src: https://images.ctfassets.net/xz1dnu24egyd/7jG9DylTxCnElENurGpocF/4241ca192ba6ec8c1d73d07c5a065d76/DevOpsPlatforms_MomentumLeader_Leader.png
            alt: G2 - Winter 2024 - Momentum Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/I7XhKHEf8rB7o1zYwYyBf/0b62ae05ecb749e83cd453de13972482/DevOps_Leader_Americas_Leader.png
            alt: G2 - Winter 2024 - Leader Americas
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab is a Leader in the 2024 Gartner® Magic Quadrant™ for DevOps Platforms'
        link:
          text: Read the report
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab is the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
        link:
          text: Read the report
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
  pricing:
    header: Find out which pricing plan works best for your growing team
    ctas:
      - button:
          externalUrl: /pricing/premium/
          text: Why GitLab Premium?
          data_ga_name: why gitlab premium
          data_ga_location: body
      - button:
          externalUrl: /pricing/ultimate/
          text: Why GitLab Ultimate?
          data_ga_name: why gitlab ultimate
          data_ga_location: body
