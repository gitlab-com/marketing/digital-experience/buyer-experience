---
title: GitLab Cookie Policy
description: This page provides information on GitLab's Cookie Policy. Learn more!
side_menu:
  anchors:
    text: "On this page"
    data:
      - text: Why do we use cookies?
        href: "#why-do-we-use-cookies"
        data_ga_name: why do we use cookies
        data_ga_location: side-navigation
      - text: Who sets cookies on GitLab?
        href: "#who-sets-cookies-on-gitlab"
        data_ga_name: who sets cookies on gitlab
        data_ga_location: side-navigation
      - text: How do I manage my cookies?
        href: "#how-do-i-manage-my-cookies"
        data_ga_name: how do i manage my cookies
        data_ga_location: side-navigation
      - text: Global Privacy Control
        href: "#global-privacy-control"
        data_ga_name: global privacy control
        data_ga_location: side-navigation
      - text: Self-Regulatory Programs
        href: "#self-regulatory-programs"
        data_ga_name: self regulatory programs
        data_ga_location: side-navigation
      - text: Web Beacons
        href: "#web-beacons"
        data_ga_name: web beacons
        data_ga_location: side-navigation
      - text: Mobile Advertising IDs
        href: "#mobile-advertising-ids"
        data_ga_name: mobile advertising ids
        data_ga_location: side-navigation
      - text: What types of cookies do we use?
        href: "#what-types-of-cookies-do-we-use"
        data_ga_name: what types of cookies do we use
        data_ga_location: side-navigation
  hyperlinks:
    text: ''
    data: []
hero_title: GitLab Cookie Policy
copy: |
  This Cookie Policy supplements the information in our Privacy Policy and explains how we use cookies and related technologies to manage and provide GitLab websites, products, and services, collectively referred to as "Services."

  Cookies are small text files stored on your computer. Pixels are small amounts of code on a webpage or in an email that provide a method for delivering content, like an image on a webpage.
sections:
  - header: Why do we use cookies?
    id: why-do-we-use-cookies
    text: |
      GitLab uses cookies for the following purposes:

      - To help you access and use the Services
      - To understand how visitors use and interact with our Services
      - To set your preferences
      - To provide relevant, interest-based advertising
      - To understand if you opened and engaged with an email
      - To analyze and improve our Services
  - header: Who sets cookies on GitLab?
    id: who-sets-cookies-on-gitlab
    text: |
      Cookies are sometimes placed by GitLab, known as first-party cookies, and sometimes third-party cookies are set by our service providers, such as Google Analytics who provides analytics and interest-based advertising.  When third-party cookies are set by service providers, they are providing a service or function to GitLab, as well as achieving the service provider's own purposes. GitLab cannot control how third-party cookies are used. You can review how Google Analytics collects and processes data by visiting: <https://policies.google.com/technologies/partner-sites>.  If you don't want Google Analytics to be used in your browser, you can install the Google Analytics Opt-out Browser Ad-On by visiting: <https://tools.google.com/dlpage/gaoptout/>.  In addition, you can disable Google Signals by following the instructions for turning off Ad Personalization found [here](https://support.google.com/My-Ad-Center-Help/answer/12155656?hl=en&co=GENIE.Platform%3DAndroid#turn-on-or-off-personalized-ads).  

      We also use the “Enhanced Conversions” extension of Google in connection with Google Ads. Through this process, input data in form fields of our website or digital offers are recorded in tags and transferred in an encrypted form to the Google Ads service via an interface. If you are logged in to your Google account at the same time you enter data into one of our forms, Google combines the data collected from our websites with Google’s existing user usage data and offers more knowledge about the usage behavior in order to target our advertising measures appropriately. This only applies to your visits on our webpages.

      In applicable jurisdictions, the collection of the input data takes place only after you provide express consent (legal basis under Article 6(1)(1)(a) GDPR) and only after the input has been completed and transmitted to us. You can revoke your consent or opt-out of this collection at any time by updating your preferences under the Cookie Preferences link (titled Do Not Sell or Share My Personal Information for the residents of certain US-States) located in the footer or header of each webpage. Please note that these settings must be changed for each browser and device.

      For more information about Google Enhanced Conversions, please visit: <https://support.google.com/google-ads/answer/9888656?hl=en>.
  - header: How do I manage my cookies?
    id: how-do-i-manage-my-cookies
    text: |
      GitLab has unified our cookie management tool across all GitLab domains providing users with a central location to manage your cookie preferences. With the exception of "strictly necessary" cookies that are essential for the Services to operate, you will be able to opt-in, opt-out, or adjust your cookie preferences for all other cookie categories. Please note that disabling some cookies may cause certain features of the Services to not function properly. This tool can be accessed by clicking on the **Cookie Preferences link** (titled **Do Not Sell or Share My Personal Information** for the residents of certain US-States) located in the footer or header of each webpage.

      Most web browsers also allow you to delete cookies already placed, which may delete the settings and preferences controlled by those cookies, including advertising preferences. You can find instructions to remove any cookies that have been created in the cookie folder of your browser at <https://allaboutcookies.org/manage-cookies/>.
  - header: Global Privacy Control
    id: global-privacy-control
    text: |
      "Global Privacy Control" (GPC) is a privacy preference you can set in your web browser to notify websites that you do not want your Personal Data shared with or sold to independent third-parties without your consent.  GitLab honors GPC in those jurisdictions where its recognition is required by applicable law, such as California, Colorado and Connecticut. At this time, we do not respond to web browser "Do Not Track" signals.
  - header: Self-Regulatory Programs
    id: self-regulatory-programs
    text: |
      Service providers may participate in self-regulatory programs that provide ways to opt out of analytics and interest-based advertising, which you can access at:

      - United States: NAI (<http://optout.networkadvertising.org>) and DAA (<http://optout.aboutads.info/>)
      - Canada: Digital Advertising Alliance of Canada (<https://youradchoices.ca/>)
      - Europe:  European Digital Advertising Alliance (<http://www.youronlinechoices.com>)
  - header: Web Beacons
    id: web-beacons
    text: |
      Most email clients have settings which allow you to prevent the automatic downloading of images, which will disable web beacons in the email messages you read.
  - header: Mobile Advertising IDs
    id: mobile-advertising-ids
    text: |
      On mobile devices, advertising IDs provided by the platform may be collected and used similar to cookie IDs. You may use the controls on iOS and Android operating systems that allow you to limit tracking and/or reset the advertising IDs.
  - header: What types of cookies do we use?
    id: what-types-of-cookies-do-we-use
    text: |
      GitLab utilizes four categories of cookies:  strictly necessary cookies, functional cookies, performance and analytics cookies, as well as targeting and advertising cookies. You can read more about each of these types of cookies and view the cookies within each category that can be found in the Services by clicking on the **Cookies Preferences link** (titled **Do Not Sell or Share My Personal Information** for the residents of certain US-States) located in the footer or header of each webpage.


