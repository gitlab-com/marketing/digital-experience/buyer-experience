{
  "metadata": {
    "title": "GitLab Duo",
    "description": "El conjunto de capacidades de IA que impulsan sus flujos de trabajo",
    "og:url": "https://about.gitlab.com/gitlab-duo/"
  },
  "heroCentered": {
    "button": {
      "href": "/solutions/gitlab-duo-pro/sales/",
      "text": "Comenzar",
      "dataGaName": "get started",
      "dataGaLocation": "hero",
      "variant": "primary"
    },
    "description": "Envíe un software más seguro de forma más rápida con IA a lo largo de todo el ciclo de desarrollo del software",
    "banner": {
        "href": "/solutions/gitlab-duo-pro/sales/",
        "show": true,
        "text": "GitLab Duo Enterprise ya está disponible. Envíe software más seguro de forma más rápida con IA a lo largo de todo el SDLC.",
        "data_ga_name": "gitlab duo pill",
        "data_ga_location": "hero"
    },
    "video": {
      "video_url":"https://player.vimeo.com/video/945979565?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
      "mp4_url":"/nuxt-images/gitlab-duo/hero.mp4",
      "webm_url":"/nuxt-images/gitlab-duo/hero.webm"
    }
  },
  "sidenav": {
    "hyperlinks": [
      {
        "title":"Beneficios",
        "href":"#benefits"
      },
      {
        "title":"Funcionalidades",
        "href":"#features"
      },
      {
        "title":"Precios",
        "href":"#pricing"
      },
      {
        "title":"Preguntas frecuentes",
        "href":"#faq"
      },
      {
        "title":"Recursos",
        "href":"#resources"
      }
    ]
  },
  "why": {
    "header": "¿Por qué elegir GitLab Duo?",
    "card": [
      {
        "title": "Acelere su ruta hacia el mercado",
        "iconName":"speed-alt-2",
        "description":"Desarrolle e implemente un software seguro de forma más rápida con IA en cada fase del ciclo de vida de desarrollo del software, desde la planificación y la creación de código hasta las pruebas, la seguridad y la supervisión."
      },
      {
        "title":"Adopte la IA con medidas de protección",
        "iconName":"ai-vulnerability-resolution",
        "description":"Con GitLab Duo, usted controla qué usuarios, proyectos y grupos pueden utilizar las capacidades con tecnología de IA. Además, los datos y el código propietario de su organización no se utilizan para entrenar modelos de IA."
      },
      {
        "title":"Mejore la experiencia del desarrollador",
        "iconName":"values-three",
        "description":"Ofrezca a sus desarrolladores una plataforma única que integre el mejor modelo de IA para cada caso de uso en todo el flujo de trabajo, desde la comprensión del código hasta la corrección de las vulnerabilidades de seguridad."
      },
      {
        "title":"Compromiso con una IA transparente",
        "iconName":"eye",
        "description":"Para que las organizaciones y los equipos confíen en la IA, ésta debe ser transparente. El [AI Transparency Center](https://about.gitlab.com/ai-transparency-center/){data-ga-name =\"ai transparency center\" data-ga-location =\"body\"} de GitLab detalla cómo defendemos la ética y la transparencia en nuestras funcionalidades con tecnología de IA."
      }
    ],
    "highlight": {
      "eyebrow": "Lo que viene",
      "header": "Presentamos GitLab Duo Workflow",
      "text": "La próxima generación de desarrollo basado en IA. Workflow es un agente inteligente y siempre activo que supervisa, optimiza y protege proyectos de forma autónoma, lo que permite a los desarrolladores centrarse en la innovación.",
      "video_src": "https://player.vimeo.com/video/967982166",
      "video_thumbnail": "https://images.ctfassets.net/xz1dnu24egyd/1i5ZSm9ITugyFYHjEBsiar/30f35a754636bdf5a7595b6a91586faa/Image.jpg",
      "cta": {
        "text": "Leer la publicación del blog",
        "url": "/blog/2024/06/27/meet-gitlab-duo-workflow-the-future-of-ai-driven-development/",
        "dataGaName": "gitlab duo workflow blog post",
        "dataGaLocation": "body"
      }

    }
  },
  "benefits": {
    "fields": {
      "header": "IA a lo largo del ciclo de vida de desarrollo del software",
      "description": "Desde la planificación y la codificación hasta la seguridad y la implementación, Duo es la única solución de IA que apoya a los desarrolladores en cada etapa de su flujo de trabajo.",
      "card": [
      {
        "title": "IA centrada en la privacidad",
        "iconName": "ai-vulnerability-resolution",
        "description": "Con GitLab Duo, usted controla qué usuarios, proyectos y grupos pueden utilizar las capacidades con tecnología de IA. Además, los datos y el código propietario de su organización no se utilizan para entrenar modelos de IA."
      },
      {
        "title": "Mejor experiencia del desarrollador",
        "iconName": "values-three",
        "description": "Ofrezca a sus desarrolladores una plataforma única que integre el mejor modelo de IA para cada caso de uso en todo el flujo de trabajo, desde la comprensión del código hasta la corrección de las vulnerabilidades de seguridad."
      },
      {
        "title": "Compromiso con una IA transparente",
        "iconName": "eye",
        "description": "Para que las organizaciones y los equipos confíen en la IA, ésta debe ser transparente. El [AI Transparency Center](/ai-transparency-center/){data-ga-name=\"ai transparency center\" data-ga-location=\"body\"} de GitLab detalla cómo defendemos la ética y la transparencia en nuestras funcionalidades con tecnología de IA. "
      }
    ]
    }
  },
  "features": {
    "sections": [
      {
        "video" : {
          "title":"Video de Vimeo 2",
          "internalName":"features-video-1",
          "thumbnail":"/nuxt-images/gitlab-duo/code-suggestions.png",
          "mp4_url":"/nuxt-images/gitlab-duo/code-suggestions.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/code-suggestions.webm"
        },
        "header": "Potencie la productividad con la asistencia de código inteligente",
        "text": "Escriba código seguro más rápido con sugerencias con tecnología de IA en más de 20 idiomas, disponibles en su IDE favorito. Automatice las tareas rutinarias y acelere los ciclos de desarrollo.",
        "pill": "Programación",
        "icon": "ai-code-suggestions",
        "left": true
      },
      {
        "video": {
          "title":"Video de Vimeo 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/chat.png",
          "mp4_url":"/nuxt-images/gitlab-duo/chat.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/chat.webm"
        },
        "header": "Su compañero de IA durante todo el desarrollo",
        "text": "Obtenga orientación en tiempo real a lo largo de todo el ciclo de vida de desarrollo del software. Genere pruebas, explique el código, refactorice de manera eficiente y chatee directamente en su IDE o interfaz web.",
        "pill": "Búsqueda",
        "icon": "ai-gitlab-chat",
        "left": false
      },
      {
        "video": {
          "title":"Video de Vimeo 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/root-cause-analysis.png",
          "mp4_url":"/nuxt-images/gitlab-duo/root-cause-analysis.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/root-cause-analysis.webm"
        },
        "header": "Resuelva rápidamente los problemas del pipeline de CI/CD",
        "text": "Ahorre tiempo en la solución de problemas con el análisis de causa raíz asistido por IA para fallas de jobs de CI/CD. Obtenga sugerencias de soluciones y concéntrese en las tareas críticas.",
        "pill": "Solución de problemas",
        "icon": "ai-root-cause-analysis",
        "left": true
      },
      {
        "video": {
          "title":"Video de Vimeo 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/vulnerability.png",
          "mp4_url":"/nuxt-images/gitlab-duo/vulnerability.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/vulnerability.webm"
        },
        "header": "Refuerce su código con seguridad con tecnología de IA",
        "text": "Comprenda y corrija las vulnerabilidades de manera más eficiente. Obtenga explicaciones detalladas y solicitudes de fusión generadas automáticamente para mitigar los riesgos de seguridad.",
        "pill": "Seguridad",
        "icon": "ai-vulnerability-bug",
        "left": false
      },
      {
        "video": {
          "title":"Video de Vimeo 2",
          "internalName":"features-video-2",
          "thumbnail":"/nuxt-images/gitlab-duo/ai.png",
          "mp4_url":"/nuxt-images/gitlab-duo/ai.mp4",
          "webm_url":"/nuxt-images/gitlab-duo/ai.webm"
        },
        "header": "Mida el ROI de su inversión en IA",
        "text": "Realice un seguimiento de la eficacia de la IA en tiempo real. Vea mejoras concretas en la duración del ciclo y las frecuencias de implementación, y cuantifique su retorno de la inversión.",
        "pill": "Medición",
        "icon": "ai-value-stream-forecast",
        "left": true
      }
    ]
  },
  "promo":{
    "header": "GitLab fue nombrado líder en el Magic Quadrant™ 2024 de Gartner® para asistentes de código con IA",
    "button": {
      "href": "/gartner-mq-ai-code-assistants/",
      "text": "Leer el informe",
      "dataGaName": "Read the report",
      "dataGaLocation": "body"
    }
  },
  "categories": {
    "header": "Funcionalidades con tecnología de IA a lo largo del ciclo de vida de desarrollo del software",
    "section": [
      {
        "name":"Para desarrollar funcionalidades",
        "data": "developing",
        "icon":"package-alt-2"
      },
      {
        "name":"Para proteger las aplicaciones",
        "data": "securing",
        "icon":"secure-alt-2"
      },
      {
        "name":"Para facilitar la colaboración",
        "data": "facilitating",
        "icon":"plan-alt-2"
      },
      {
        "name":"Para la solución avanzada de problemas",
        "data": "troubleshooting",
        "icon":"monitor-alt-2"
      }
    ],
    "card": [
      {
        "icon":"ai-gitlab-chat",
        "header":"Chat",
        "description":"Procesa y genera texto y código de manera conversacional. Le ayuda a identificar rápidamente información útil en grandes volúmenes de tickets, épicas, código y documentación de GitLab.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo_chat/index.html",
        "category":"developing",
        "text": "Leer más"
      },
      {
        "icon":"ai-root-cause-analysis",
        "header":"Explicación del código",
        "description":"Esta funcionalidad le ayuda a comprender el código explicándolo en lenguaje natural.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-explanation",
        "category":"developing",
        "text": "Leer más"
      },
      {
        "icon":"ai-git-suggestions",
        "header":"Sugerencias de código",
        "description":"Ayuda a los desarrolladores a escribir código seguro de manera más eficiente y acelerar la duración del ciclo al ocuparse de las tareas de programación repetitivas y rutinarias.",
        "url":"https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html",
        "category":"developing",
        "text": "Leer más"
      },
      {
        "icon":"ai-git-suggestions",
        "header":"GitLab Duo para CLI",
        "description":"Descubra o recuerde los comandos de Git cuando y donde los necesite.",
        "url":"https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html#gitlab-duo-commands",
        "category":"developing",
        "text": "Leer más"
      },
      {
        "icon":"ai-tests-in-mr",
        "header":"Generación de pruebas",
        "description":"Automatiza las tareas repetitivas y ayuda a detectar errores en etapas tempranas.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/#test-generation",
        "category":"developing",
        "text": "Leer más"
      },
      {
        "icon":"ai-vulnerability-bug",
        "header":"Explicación de vulnerabilidades",
        "description":"Le ayuda a corregir las vulnerabilidades de manera más eficiente, mejorar sus habilidades y escribir código más seguro.",
        "url":"https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability",
        "category":"securing",
        "text": "Leer más"
      },
      {
        "icon":"ai-vulnerability-resolution",
        "header":"Resolución de vulnerabilidades",
        "description":"Genera una solicitud de fusión que contiene los cambios necesarios para mitigar una vulnerabilidad.",
        "url":"https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#vulnerability-resolution",
        "category":"securing",
        "text": "Leer más"
      },
      {
        "icon":"ai-value-stream-forecast",
        "header":"Panel de impacto de la IA",
        "description":"Vea mejoras en tiempo real en la duración del ciclo y las frecuencias de implementación",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/#ai-impact-dashboard",
        "category":"facilitating",
        "text": "Leer más"
      },
      {
        "icon":"ai-issue",
        "header":"Resumen de la discusión",
        "description":"Esta funcionalidad ayuda a mantener a todos informados sobre conversaciones extensas para garantizar que todos estén en sintonía.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary",
        "category":"facilitating",
        "text": "Leer más"
      },
      {
        "icon":"ai-summarize-mr-review",
        "header":"Resumen de la revisión de código",
        "description":"Facilita el traspaso de solicitudes de fusión entre autores y revisores. Además, ayuda a los revisores a comprender las sugerencias de manera más eficiente.",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#summarize-a-code-review",
        "category":"facilitating",
        "text": "Leer más"
      },
      {
        "icon":"ai-merge-request",
        "header":"Resumen de la solicitud de fusión",
        "description":"Esta funcionalidad comunica de manera eficiente el impacto de los cambios en su solicitud de fusión.",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html",
        "category":"facilitating",
        "text": "Leer más"
      },
      {
        "icon":"ai-generate-issue-description",
        "header":"Generación de descripciones de tickets",
        "description":"Genera descripciones de los tickets.",
        "url":"https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html#summarize-a-code-review",
        "category":"facilitating",
        "text": "Leer más"
      },
      {
        "icon":"ai-root-cause-analysis",
        "header":"Análisis de causa raíz",
        "description":"Le ayuda a determinar la causa raíz de una falla en el pipeline y de una compilación de CI/CD fallida.",
        "url":"https://docs.gitlab.com/ee/user/gitlab_duo/index.html#root-cause-analysis",
        "category":"troubleshooting",
        "text": "Leer más"
      }
    ],
    "disclaimer": "Una licencia Ultimate permitirá probar ciertas capacidades enumeradas como Experimentos o en Beta sujetas al [Acuerdo de pruebas de GitLab](https://handbook.gitlab.com/handbook/legal/testing-agreement/). Una vez que una funcionalidad de IA pasa de Beta a Disponibilidad General, los clientes con una licencia Premium o Ultimate pueden seguir utilizando las capacidades de GitLab Duo con la compra del complemento GitLab Duo Pro o GitLab Duo Enterprise."
  },
  "faq": {
    "header":"Preguntas frecuentes",
    "texts":{
      "show":"Mostrar todo",
      "hide":"Ocultar todo"
    },
    "questions": [
      {
        "question":"¿Qué lenguajes de programación se admiten en las sugerencias de código?",
        "answer":"Los mejores resultados de las sugerencias de código se esperan para los lenguajes que las [API de Google Vertex AI Codey](https://cloud.google.com/vertex-ai/generative-ai/docs/code/code-models-overview#supported_coding_languages) admiten directamente: C++, C#, Go, Google SQL, Java, JavaScript, Kotlin, PHP, Python, Ruby, Rust, Scala, Swift y TypeScript. "
      },
      {
        "question":"¿Qué modelos de lenguaje utiliza GitLab Duo?",
        "answer":"Nuestra capa de abstracción nos permite potenciar las capacidades de IA con el modelo adecuado para el caso de uso apropiado. Explore los modelos de lenguaje utilizados para cada funcionalidad de GitLab Duo [aquí](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html)."
      },
      {
        "question":"¿Se utilizará mi código para entrenar modelos de IA?",
        "answer":"GitLab no capacita modelos de IA generativa basados en datos privados (no públicos). Los proveedores con los que trabajamos tampoco entrenan modelos con datos privados. [Obtenga más información aquí](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#training-data){data-ga-name=\"learn more about ai training models\" data-ga-location=\"faq\"}."
      },
      {
        "question":"¿GitLab Duo es de núcleo abierto?",
        "answer":"Sí. GitLab tiene un modelo empresarial de núcleo abierto que nos permite desarrollar con nuestros clientes, que pueden contribuir con nuevas capacidades a nuestro producto."
      },
      {
        "question":"¿Cómo puedo usar los resultados generados por GitLab Duo?",
        "answer":"El resultado generado por GitLab Duo se puede utilizar a su discreción y, si surge una reclamación de terceros por el uso del resultado generado por GitLab Duo, GitLab intervendrá y lo defenderá."
      },
      {
        "question":"¿Cómo controlan los clientes qué usuarios pueden acceder a las funcionalidades de GitLab Duo Pro?",
        "answer":"Los controles organizativos permiten a los administradores asignar cupos de licencia a usuarios específicos a través de una nueva interfaz de usuario (UI) de administrador. El usuario puede activar o desactivar las sugerencias de código directamente en la configuración de la extensión IDE. Las funcionalidades de IA experimental/beta, como el chat, se controlan con una configuración de grupo principal. [Obtenga más información aquí](https://docs.gitlab.com/ee/user/gitlab_duo/index.html#experimental-features){data-ga-name=\"learn more about experiemental ai features\" data-ga-location=\"faq\"}."
      },
      {
        "question":"¿GitLab Duo Pro está disponible en instancias de GitLab de conectividad limitada/sin conexión?",
        "answer":"No, las sugerencias de código y el chat requieren conectividad a Internet y licencias en la nube para que los clientes self-managed puedan acceder."
      },
      {
        "question":"Al proporcionar la indicación en un comentario (en el archivo fuente), ¿puede estar en un idioma que no sea inglés?",
        "answer":"No restringimos intencionalmente el lenguaje natural, sin embargo, esperamos que el inglés proporcione los mejores resultados. A partir de la experimentación, escribir la indicación en un idioma que no sea inglés proporciona una sugerencia de código adaptada a ese idioma (respetando la sintaxis del idioma específico y las palabras clave reservadas)."
      },
      {
        "question":"¿Cómo garantizan las sugerencias de código la protección de la propiedad intelectual y la privacidad de los datos?",
        "answer":"Lea todo sobre las sugerencias de código, la privacidad de datos y la protección de la propiedad intelectual [aquí](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#data-privacy){data-ga-name=\"data privacy and ip protection\" data-ga-location=\"faq\"}. El uso de sugerencias de código y chat se rige por el [Acuerdo de pruebas de GitLab](https://about.gitlab.com/handbook/legal/testing-agreement/){data-ga-name =\"gitlab testing agreement\" data-ga-location = \"faq\"} mientras todavía es de uso gratuito y el chat todavía está en versión beta. Obtenga información sobre el [uso de datos al usar las sugerencias de código aquí](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name=\"coe suggestions data usage\" data-ga-location=\"faq\"} y el [uso de datos del chat aquí.](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name=\"chat data usage\" data-ga-location=\"faq\"}."
      }
    ]
  },
  "pricing": {
    "header":"Precios"
  },
  "resources": {
    "id":"resources",
    "align":"left",
    "grouped": "true",
    "column_size": 4,
    "title":"Más información sobre GitLab Duo",
    "header_cta_text":"Ver todos los recursos",
    "header_cta_href":"/resources/",
    "header_cta_ga_name":"View all resources",
    "header_cta_ga_location":"body",
    "cards":[
      {
        "event_type":"Video",
        "header":"Presentamos GitLab Duo",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/wS2NkIeyOYrkiqadM1Jkd/eeabed3c2d08fa50047c70989d70b14d/Meet_GitLab_Duo_Thumbnail.png",
        "href":"https://player.vimeo.com/video/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"meet gitlab duo"
      },
      {
        "event_type":"Video",
        "header":"Sugerencias de código",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/7paG5yX4I2FErEKb3022EN/cd480897c76cd1c5dd2a51426c8e0013/Code_Suggestions_Thumbnail.png",
        "href":"https://player.vimeo.com/video/894621401?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479",
        "data_ga_name":"code suggestions"
      },
      {
        "event_type":"Video",
        "header":"Chat",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/4y29zj5spjIUh2DaGdxk5q/f7d972269fd3b16501ef4d3b013f6c09/Chat_Thumbnail.png",
        "href":"https://player.vimeo.com/video/927753737?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"chat"
      },
      {
        "event_type":"Video",
        "header":"Resumen de la revisión de código",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/kfMhe63t5l6jHIxmmlq5z/a68a12a38b7faf7b37c821c5e3204ec1/Code_Review_Summary_Thumbnail.png",
        "href":"https://player.vimeo.com/video/929891003?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"code review summary"
      },
      {
        "event_type":"Video",
        "header":"Resumen de la discusión",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/4gvRwDXZ3zDwKKtCHiYhy6/c26a91ddfa12817c2bcee4e6a991260f/Discussion_Summary_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/928501915?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"discussion summary"
      },
      {
        "event_type":"Video",
        "header":"Explicación de vulnerabilidades",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/64mYwKfirCjkHiapVtZlN4/830c78fa1902ed0a61ffdc9da822145c/Vulnerability_Explanation_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/930066123?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"vulernability explanation"
      },
      {
        "event_type":"Video",
        "header":"Explicación del código",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/1N6clW3Mq7Fu40uIEt2iav/6b487d1916276934b3641a456211835c/Code_Explanation_Thumbnail.jpg",
        "href":"https://player.vimeo.com/video/930066090?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"code explanation"
      },
      {
        "event_type":"Video",
        "header":"Revisores sugeridos",
        "link_text":"Ver ahora",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/5L9AwAebsBO8eIQxSH96nz/a388c454b847bc134082a352bea32082/Suggested_Reviewers_Thumbnail.png",
        "href":"https://player.vimeo.com/video/930066108?badge=0&autopause=0&player_id=0&app_id=58479",
        "data_ga_name":"suggested reviewers"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"Blog",
        "header":"Comprender y resolver vulnerabilidades con GitLab Duo con tecnología de IA",
        "link_text":"Leer más",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/1bIUeH2qhLxfgPJIZbBzD9/c916adff20eab529510a0e4eea5d9f4d/GitLab-Duo-Blog-Image-1.png",
        "href":"/blog/2024/02/21/understand-and-resolve-vulnerabilities-with-ai-powered-gitlab-duo/",
        "data_ga_name":"Understand and resolve vulnerabilities with AI-powered GitLab Duo"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"Blog",
        "header":"Medición de la efectividad de la IA más allá de las métricas de productividad de los desarrolladores",
        "link_text":"Leer más",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/2IjSEZ7qEEcaDf2PlvJKRp/bb97ac8129ca9404b74ea0c13b5ed437/GitLab-Duo-Blog-Image-2.png",
        "href":"/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/",
        "data_ga_name":"Measuring AI effectiveness beyond developer productivity metrics"
      },
      {
        "icon":{
          "name":"blog",
          "variant":"marketing"
        },
        "event_type":"Blog",
        "header":"Un nuevo informe sobre herramientas asistidas por IA indica un aumento de las apuestas para DevSecOps",
        "link_text":"Leer más",
        "image":"https://images.ctfassets.net/xz1dnu24egyd/bH5lKxYxiDkZ4LULySnmO/ab7437e32e77d6b2f39bd70184edf913/GitLab-Duo-Blog-Image-3.png",
        "href":"/blog/2024/02/14/new-report-on-ai-assisted-tools-points-to-rising-stakes-for-devsecops/",
        "data_ga_name":"New report on AI-assisted tools points to rising stakes for DevSecOps"
      }
    ]
  }
}
