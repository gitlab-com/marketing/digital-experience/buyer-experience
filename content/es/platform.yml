---
  title: Plataforma
  description: Obtenga más información sobre cómo la plataforma GitLab puede ayudar a los equipos a colaborar y crear software más rápido.
  hero:
    note: La plataforma de DevSecOps
    header: con tecnología de IA
    sub_header: más completa
    description: Entregue un mejor software más rápido con una plataforma para todo su ciclo de vida de entrega de software.
    image:
      src: /nuxt-images/platform/loop-shield-duo.svg
      alt: The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).
    button:
      text: Obtener prueba gratuita
      href: https://gitlab.com/-/trial_registrations/new?glm_source=/solutions/ai/&glm_content=default-saas-trial
      variant: secondary
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      text: Más información sobre los precios
      href: /pricing/
      variant: tertiary
      icon: chevron-lg-right
      data_ga_name: pricing
      data_ga_location: hero
  table_data:
    - name: "Planificación"
      link: "/solutions/agile-delivery/"
      icon: "plan-alt-2"
      features:
        - name: "Informes de DevOps"
          link: "https://docs.gitlab.com/ee/administration/analytics/dev_ops_reports"
        - name: "Métricas DORA"
          link: "/solutions/value-stream-management/dora/"
        - name: "Gestión del flujo de valor"
          link: "/solutions/value-stream-management/"
        - name: "Pages"
          link: "https://docs.gitlab.com/ee/user/project/pages/"
        - name: "Wiki"
          link: "https://docs.gitlab.com/ee/user/project/wiki/"
        - name: "Gestión de portafolios"
          link: "https://docs.gitlab.com/ee/topics/plan_and_track.html#portfolio-management"
        - name: "Planificación de equipos"
          link: "https://docs.gitlab.com/ee/topics/plan_and_track.html#team-planning"
        - name: "Resumen de épicas"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
        - name: "Resumen de tickets"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
        - name: "Generar descripción de tickets"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#issue-description-generation"
        - name: "Resumen de la discusión"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
      replacement: Sentry
    - name: "Gestión del código fuente"
      link: "/solutions/source-code-management/"
      icon: "cog-code"
      features:
        - name: "Desarrollo remoto"
          link: "https://docs.gitlab.com/ee/user/project/remote_development/"
        - name: "Web IDE"
          link: "https://docs.gitlab.com/ee/user/project/web_ide/"
        - name: "GitLab CLI"
          link: "https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html"
        - name: "Flujo de trabajo de la revisión del código"
          link: "https://docs.gitlab.com/ee/user/project/merge_requests/"
        - name: "Sugerencias de código"
          link: "https://docs.gitlab.com/ee/user/project/repository/code_suggestions/"
        - name: "Explicación del código"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-explanation-in-the-ide"
        - name: "Resumen de la revisión del código"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#code-review-summary"
        - name: "Generación de pruebas"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#test-generation"
      replacement: GitHub
    - name: "Integración continua"
      link: "/solutions/continuous-integration/"
      icon: "automated-code-alt"
      features:
        - name: "Gestión de secretos"
          link: "https://docs.gitlab.com/ee/ci/secrets/"
        - name: "Review Apps"
          link: "https://docs.gitlab.com/ee/ci/review_apps/"
        - name: "Cobertura y pruebas de código"
          link: "https://docs.gitlab.com/ee/ci/testing/"
        - name: "Trenes de fusión"
          link: "https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html"
        - name: "Revisores sugeridos"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#suggested-reviewers"
        - name: "Resumen de solicitudes de fusión"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#merge-request-summary"
        - name: "Análisis de causa raíz"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#root-cause-analysis"
        - name: "Resumen de la discusión"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#discussion-summary"
      replacement: Jenkins
    - name: "Seguridad"
      link: "/solutions/security-compliance/"
      icon: "secure-alt-2"
      features:
        - name: "Análisis de contenedores"
          link: "https://docs.gitlab.com/ee/user/application_security/container_scanning/"
        - name: "Análisis de la composición de software"
          link: "https://docs.gitlab.com/ee/user/application_security/dependency_scanning/"
        - name: "Seguridad de la API"
          link: "https://docs.gitlab.com/ee/user/application_security/api_security/"
        - name: "Fuzzing guiado por cobertura"
          link: "https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/"
        - name: "DAST"
          link: "https://docs.gitlab.com/ee/user/application_security/dast/"
        - name: "Calidad del código"
          link: "https://docs.gitlab.com/ee/ci/testing/code_quality"
        - name: "Detección de secretos"
          link: "https://docs.gitlab.com/ee/user/application_security/secret_detection/"
        - name: "SAST"
          link: "https://docs.gitlab.com/ee/user/application_security/sast/"
        - name: "Explicación de vulnerabilidades"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#vulnerability-explanation"
        - name: "Resolución de vulnerabilidades"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#vulnerability-resolution"
      replacement: Snyk
    - name: "Cumplimiento"
      icon: "protect-alt-2"
      link: "/solutions/continuous-software-compliance/"
      features:
        - name: "Evidencia de lanzamiento"
          link: "https://docs.gitlab.com/ee/user/project/releases/release_evidence"
        - name: "Gestión de cumplimiento"
          link: "https://docs.gitlab.com/ee/administration/compliance.html"
        - name: "Eventos de auditoría"
          link: "https://docs.gitlab.com/ee/administration/audit_events.html"
        - name: "Lista de materiales de software"
          link: "/solutions/supply-chain/"
        - name: "Gestión de dependencias"
          link: "https://docs.gitlab.com/ee/user/application_security/dependency_list/"
        - name: "Gestión de vulnerabilidades"
          link: "https://docs.gitlab.com/ee/user/application_security/security_dashboard/"
        - name: "Gestión de la política de seguridad"
          link: "https://docs.gitlab.com/ee/user/application_security/policies/"
    - name: "Registro de artefactos"
      icon: "package-alt-2"
      features:
        - name: "Registro virtual"
          link: "https://docs.gitlab.com/ee/user/packages/dependency_proxy/"
        - name: "Registro de contenedores"
          link: "https://docs.gitlab.com/ee/user/packages/container_registry/"
        - name: "Registro de Helm Chart"
          link: "https://docs.gitlab.com/ee/user/packages/helm_repository/#helm-charts-in-the-package-registry"
        - name: "Registro de paquetes"
          link: "https://docs.gitlab.com/ee/user/packages/"
      replacement: JFrog
    - name: "Entrega continua"
      icon: "continuous-delivery-alt"
      link: "/solutions/continuous-integration/"
      features:
        - name: "Orquestación de lanzamientos"
          link: "https://docs.gitlab.com/ee/user/project/releases/"
        - name: "Infraestructura como código"
          link: "https://docs.gitlab.com/ee/user/infrastructure/iac/index.html"
        - name: "Indicadores de funcionalidades"
          link: "https://docs.gitlab.com/ee/operations/feature_flags.html"
        - name: "Gestión del entorno"
          link: "https://docs.gitlab.com/ee/ci/environments/"
        - name: "Gestión de la implementación"
          link: "https://docs.gitlab.com/ee/topics/release_your_application.html"
        - name: "Auto DevOps"
          link: "/solutions/delivery-automation/"
      replacement: Arnés
    - name: "Observabilidad"
      icon: "monitor-alt-2"
      link: "/solutions/analytics-and-insights/"
      features:
        - name: "Service Desk"
          link: "https://docs.gitlab.com/ee/user/project/service_desk/"
        - name: "Gestión del calendario bajo demanda"
          link: "https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html"
        - name: "Gestión de incidentes"
          link: "https://docs.gitlab.com/ee/operations/incident_management/"
        - name: "Seguimiento de errores"
          link: "https://docs.gitlab.com/ee/operations/error_tracking.html"
        - name: "Visualización de análisis de producto"
          link: "https://docs.gitlab.com/ee/user/analytics/"
        - name: "Pronóstico del flujo de valor"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#value-stream-forecasting"
        - name: "Análisis de producto de IA"
          link: "https://docs.gitlab.com/ee/user/gitlab_duo/index.html#product-analytics"
      replacement: Sentry
  benefits:
    title: Una sola plataforma
    subtitle: para empoderar a los equipos de desarrollo, seguridad y operaciones
    image:
      image_url: "/nuxt-images/platform/one-platform.svg"
      alt: code source image
    is_accordion: true
    tabs:
      - tabButtonText: Desarrollo
        data_ga_name: development
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Flujo de trabajo con tecnología de IA
              text: Aumente la eficiencia y reduzca la duración del ciclo de cada usuario con la ayuda de la IA en cada fase del ciclo de vida de desarrollo del software, desde la planificación y la creación de código hasta las pruebas, la seguridad y la supervisión.
              headerCtas:
                - externalUrl: /gitlab-duo/
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
              ctaHeader: "Véalo en acción:"
              ctas:
                - externalUrl: https://player.vimeo.com/video/855805049
                  text: GitLab Duo
                  data_ga_name: GitLab Duo
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/894621401
                  text: Sugerencias de código
                  data_ga_name: Code Suggestions
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/927753737
                  text: Chat
                  data_ga_name: Chat
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Aplicación única
              text: GitLab reúne todas las capacidades de DevSecOps en una aplicación con un almacén de datos unificado para que todo esté en un solo lugar.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: Video sobre el uso de las métricas DORA en GitLab
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062?h=752d064728&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
                  text: Video sobre el panel de flujos de valor de GitLab
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Mayor productividad de los desarrolladores
              text: La aplicación única de GitLab ofrece una experiencia de usuario superior, lo que mejora la duración del ciclo y ayuda a evitar el cambio de contexto.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925629920
                  text: Video sobre la gestión de portafolios de GitLab
                  data_ga_name: GitLab's Portfolio Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925632272
                  text: Video sobre la gestión de OKR de GitLab
                  data_ga_name: GitLab's OKR Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/925633691
                  text: Video sobre diseño de cargas para tickets de GitLab
                  data_ga_name: Design Uploads to GitLab issues
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Mejor automatización
              text: Las herramientas de automatización de GitLab son más confiables y tienen muchas funcionalidades, lo que ayuda a eliminar la carga cognitiva y el trabajo pesado innecesario.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023715
                  text: Video de descripción general de la CD de GitLab
                  data_ga_name: GitLab's CD Overview
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://docs.gitlab.com/ee/operations/error_tracking.html
                  text: Documentación de seguimiento de errores
                  data_ga_name: Error tracking documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Documentación de gestión de incidentes
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/2DKPuM0RDt2jdfzIaTroKS/1aeee8ed2e7939523ef89e7046d5a468/iron-mountain.svg
                quote: La visión que tiene GitLab con respecto a vincular la estrategia al alcance y al código es muy poderosa. Aprecio el nivel de inversión que continúan haciendo en la plataforma.
                metrics:
                  - number: $150 000
                    text: de ahorro aproximado al año
                  - number: 20 horas
                    text: ahorradas en tiempo de incorporación por proyecto
                author:
                  headshot: https://images.ctfassets.net/xz1dnu24egyd/1G9lo7UyVxvW6m1CTNND9b/7b8b0449fc8ae1299e3b4fc56f4dd363/JasonManoharan.png
                  name: Jason Monoharan.
                  title: VP de Tecnología.
                  company: Iron Mountain
                cta: 
                  text: Leer el estudio
                  url: '/customers/iron-mountain/'
                  dataGaName: iron mountain case study
                  dataGaLocation: body
          cta:
            title: Aproveche el poder de la IA con
            highlight: GitLab Duo
            text: Más información
            externalUrl: /gitlab-duo/
            data_ga_name: GitLab Duo
            data_ga_location: body
            additionalIcon: verdadero
      - tabButtonText: Seguridad
        data_ga_name: security
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: La seguridad está integrada, no ensamblada
              text: Las capacidades de seguridad de GitLab, como DAST, pruebas fuzzing, análisis de contenedores y detección de API, están integradas de extremo a extremo.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925635707
                  text: Video sobre las pruebas dinámicas de seguridad de las aplicaciones (DAST)
                  data_ga_name: Dynamic Application Security Testing (DAST) video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925676815
                  text: Video sobre el análisis de contenedores
                  data_ga_name: Container scanning video
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925677603
                  text: Video sobre la seguridad de la API y el fuzzing de la API
                  data_ga_name: API security and web API Fuzzing video
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Cumplimiento y gestión precisa de la política
              text: GitLab ofrece una solución de gobernanza integral que permite la separación de funciones entre los equipos. El editor de políticas de GitLab permite obtener reglas de aprobación personalizadas adaptadas a los requisitos de cumplimiento de cada organización, lo que reduce el riesgo.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/administration/compliance.html
                  text: Documentación de gestión de cumplimiento
                  data_ga_name: Compliance Management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://player.vimeo.com/video/925679314
                  text: Video sobre los marcos de cumplimiento de GitLab
                  data_ga_name: GitLab's Compliance Frameworks
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
                - externalUrl: https://player.vimeo.com/video/925679982
                  text: Video sobre la gestión de requisitos de GitLab
                  data_ga_name: GitLab's Requirements Management
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Automatización de la seguridad
              text: Las herramientas de automatización avanzadas de GitLab permiten aumentar la velocidad con medidas de protección, lo que garantiza que el código se analice automáticamente en busca de vulnerabilidades.
              ctas:
                - externalUrl: https://player.vimeo.com/video/925680640
                  text: Video sobre el panel de seguridad de GitLab
                  data_ga_name: GitLab's Security Dashboard
                  data_ga_location: body
                  iconName: slp-play-circle
                  modal: true
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/4mEBdmMQrpjOn1VJ2idd9h/39fbbf0fc290dda417a35d1e437de4a3/hackerone.svg
                quote: GitLab nos ayuda a detectar fallas de seguridad temprano y lo integra en el flujo del desarrollador.Un ingeniero puede hacer push de código a GitLab CI, obtener comentarios inmediatos de uno de los muchos pasos de auditoría en cascada y ver si hay una vulnerabilidad de seguridad incorporada, e incluso crear su propio paso nuevo que podría probar un problema de seguridad muy específico.
                metrics:
                  - number: Tiempo de pipeline 7.5 veces
                    text: más rápido
                  - number: 4 horas
                    text: de tiempo de desarrollo por ingeniero ahorradas por semana
                author:
                  name: Mitch Trale
                  title: Jefe de Infraestructura
                  company: Hackerone
                cta: 
                  text: Leer el estudio
                  url: '/customers/hackerone/'
                  dataGaName: hackerone case study
                  dataGaLocation: body
          cta:
            title: Descubra cómo agregar análisis de seguridad a su
            highlight: Pipeline de CI
            text: Lanzar demostración
            data_ga_name: ci pipeline
            data_ga_location: body
            modal: true
            iconName: slp-laptop-video
            demo:
              subtitle: Agregue análisis de seguridad a su pipeline de CI/CD
              video:
                url: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
              scheduleButton:
                text: Programar una demostración personalizada
                href: /sales/
                data_ga_name: demo
                data_ga_location: body
      - tabButtonText: Operaciones
        data_ga_name: operations
        data_ga_location: body
        tabPanelContent:
          accordion:
            - header: Expandir las cargas de trabajo de Enterprise
              text: GitLab admite fácilmente a las empresas a cualquier escala con la capacidad de gestionar y actualizar casi sin tiempo de inactividad.
              ctas:
                - externalUrl: https://docs.gitlab.com/ee/user/infrastructure/iac/
                  text: Documentación de infraestructura como código (IaC)
                  data_ga_name: Infrastructure as code (IaC) documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: https://docs.gitlab.com/ee/operations/incident_management/
                  text: Documentación de gestión de incidentes
                  data_ga_name: Incident management documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Visibilidad incomparable de las métricas
              text: El almacén de datos unificado de GitLab proporciona análisis para todo el ciclo de vida de desarrollo del software en un solo lugar, lo que elimina la necesidad de integraciones de productos adicionales.
              ctas:
                - externalUrl: https://player.vimeo.com/video/892023781
                  text: Video sobre el uso de las métricas DORA en GitLab
                  data_ga_name: DORA metrics - User Analytics
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
                - externalUrl: https://player.vimeo.com/video/819308062
                  text: Video sobre el panel de flujos de valor de GitLab
                  data_ga_name: GitLab's Value Streams Dashboard
                  data_ga_location: body
                  modal: true
                  iconName: slp-play-circle
            - header: Asistencia nativa de la nube, multinube y heredada
              text: GitLab proporciona una plataforma de DevSecOps completa que permite a los equipos tener las mismas métricas de productividad y gobernanza, independientemente de su combinación de infraestructuras.
              ctas:
                - externalUrl: /topics/multicloud/
                  text: Documentación multinube
                  data_ga_name: Multicloud documentation
                  data_ga_location: body
                  iconName: gl-doc-text
                - externalUrl: /solutions/gitops/
                  text: Documentación de GitOps
                  data_ga_name: GitOps documentation
                  data_ga_location: body
                  iconName: gl-doc-text
            - header: Menor costo total de propiedad
              text: ''
              ctas:
                - description: "Descubra cómo el contratista de defensa más grande del mundo utiliza GitLab para reducir las cadenas de herramientas, acelerar la producción y mejorar la seguridad:"
                  externalUrl: /customers/lockheed-martin/
                  text: Estudio de caso de Lockheed Martin
                  data_ga_name: Lockheed Martin case study
                  data_ga_location: body
                  iconName: gl-doc-text
                - description: 'Descubra cómo CARFAX recortó su cadena de herramientas de DevSecOps y mejoró la seguridad con GitLab:'
                  externalUrl: /customers/carfax/
                  text: Estudio de caso de CARFAX
                  data_ga_name: CARFAX case study
                  data_ga_location: body
                  iconName: gl-doc-text
          case_study:
            darkMode: true
            cards:
              - logo: https://images.ctfassets.net/xz1dnu24egyd/rdWpo3pKJBUFLzDFmTNIE/d69e61550635ebba0556216c44d401a7/forrester-logo.svg
                quote: Mejoró el desarrollo y la eficiencia de la entrega en más del 87 %, lo que supuso un ahorro de más de $23 millones. GitLab permitió a las organizaciones reducir drásticamente el tiempo dedicado a cada fase de todo el ciclo de vida de DevOps.
                metrics:
                  - number: $200,5 millones
                    text: de beneficios totales en tres años
                  - number: 427 %
                    text: de retorno total de inversión (ROI)
                cta: 
                  text: Leer el estudio
                  url: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
                  dataGaName: resources study forrester
                  dataGaLocation: body
          cta:
            title: ¿Cuánto le cuesta su
            highlight: cadena de herramientas?
            text: Pruebe nuestra calculadora de ROI
            externalUrl: /calculator/
            data_ga_name: try our roi calculator
            data_ga_location: body
  video_spotlight:
    header: ¿Quiere aumentar la velocidad? Consolide su cadena de herramientas hoy mismo.
    list:
      - text: Mejore la colaboración
        iconName: gl-check
      - text: Reduzca la carga administrativa
        iconName: gl-check
      - text: Incremente la seguridad
        iconName: gl-check
      - text: Menor costo total de propiedad
        iconName: gl-check
      - text: Expanda sin problemas
        iconName: gl-check
    blurb: |
      **¿No sabe por dónde empezar?**
       Nuestro equipo de ventas puede ofrecerle orientación.
    video:
      title: Por qué elegir GitLab
      url: https://player.vimeo.com/video/799236905?h=4eee39a447
      text: Más información
      data_ga_name: Learn More
      data_ga_location: body
      image: https://images.ctfassets.net/xz1dnu24egyd/7KcXktuJTizzgMwF5o8n66/eddd4708263c41d1afa49399222d8f55/platform-video.png
    button:
      externalUrl: /sales/
      text: Hablar con Ventas
      data_ga_name: sales
      data_ga_location: body
  recognition:
    heading: Los líderes del sector confían en GitLab
    subtitle: GitLab se clasifica como líder G2 en todas las categorías de DevOps.
    badges:
          - src: https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/b3632635c9e3bf13a2d02ada6374029b/DevOpsPlatforms_Leader_Leader.png
            alt: G2 - Winter 2024 - Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/17EE9DHTQpLyGbJAQslSEk/e04bb1e6e71d895af94d10ffb85471e3/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 - Winter 2024 - Easiest to Use
          - src: https://images.ctfassets.net/xz1dnu24egyd/1SNrMzYNTrOvEnOuNucAMD/be22fc71855a96d9c05627c7085148a9/users-love-us.png
            alt: G2 - Winter 2024 - Users love us
          - src: https://images.ctfassets.net/xz1dnu24egyd/3UhXS8fSwKieddbliKZEGn/9bd6e04223769aa0869470befd78dc62/ValueStreamManagement_BestUsability_Total.png
            alt: G2 - Winter 2024 - Best Usability
          - src: https://images.ctfassets.net/xz1dnu24egyd/7AODDnwkskODEsylwXkBys/908eb94a40d8ab2f64bdcacbd18f35a3/DevOpsPlatforms_Leader_Europe_Leader.png
            alt: G2 - Winter 2024 - Leader Europe
          - src: https://images.ctfassets.net/xz1dnu24egyd/4xk6CmKqUndyuir0m4rF5b/b069fcc665b3b11c2a017fb36aa55130/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 - Winter 2024 - Leader Enterprise
          - src: https://images.ctfassets.net/xz1dnu24egyd/7jG9DylTxCnElENurGpocF/4241ca192ba6ec8c1d73d07c5a065d76/DevOpsPlatforms_MomentumLeader_Leader.png
            alt: G2 - Winter 2024 - Momentum Leader
          - src: https://images.ctfassets.net/xz1dnu24egyd/I7XhKHEf8rB7o1zYwYyBf/0b62ae05ecb749e83cd453de13972482/DevOps_Leader_Americas_Leader.png
            alt: G2 - Winter 2024 - Leader Americas
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab es líder en el Cuadrante Mágico 2024 de Gartner® para plataformas DevOps'
        link:
          text: Leer el informe
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab es el único líder en The Forrester Wave™: plataformas integradas de entrega de software, segundo trimestre de 2023"
        link:
          text: Leer el informe
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
  pricing:
    header: Descubra qué plan de precios funciona mejor para su equipo en crecimiento
    ctas:
      - button:
          externalUrl: /pricing/premium/
          text: ¿Por qué elegir GitLab Premium?
          data_ga_name: why gitlab premium
          data_ga_location: body
      - button:
          externalUrl: /pricing/ultimate/
          text: ¿Por qué elegir GitLab Ultimate?
          data_ga_name: why gitlab ultimate
          data_ga_location: body