---
  title: Precios
  description: "Consulte los precios de la plataforma de DevSecOps de GitLab, que combina la capacidad de desarrollar, asegurar y operar software en una aplicación única."
  schema_faq:
    - question: Ya tengo una cuenta, ¿cómo puedo cambiar de plan?
      answer: >-
        Diríjase a [https://customers.gitlab.com](https://customers.gitlab.com) y elija el plan adecuado para usted.
    - question: ¿Puedo agregar más usuarios a mi suscripción?
      answer: >-
        Sí. Tiene algunas opciones. Puede agregar usuarios a su suscripción en cualquier momento durante el período de suscripción. Puede iniciar sesión en su cuenta a través del [Portal de clientes de GitLab](https://customers.gitlab.com) y agregar más usuarios o [contactar a Ventas](/sales/) para solicitar un presupuesto. En cualquier caso, el costo se prorrateará desde la fecha de cotización/compra hasta el final del período de suscripción. También puede pagar las licencias adicionales según nuestro modelo de conciliación de licencias.
    - question: ¿Cómo se me cobrará por los usuarios de complementos?
      answer: |
        Si tiene habilitada la [reconciliación de suscripción trimestral](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html) (opción predeterminada para suscripciones nuevas y renovadas después del 1 de agosto de 2021), a los usuarios que se agregaron durante un trimestre solo se les cobrarán los trimestres restantes de su período de suscripción en lugar de las tarifas de suscripción anuales completas con conciliaciones anuales. Por ejemplo, si agrega 50 usuarios a su suscripción durante el tercer trimestre de su período de suscripción, a los 50 usuarios solo se les cobrará por el cuarto trimestre de su período de suscripción en lugar de los cuatro trimestres de conformidad con las conciliaciones de licencias anuales.

        Si no tiene habilitada la reconciliación de suscripción trimestral, a los usuarios de complementos se les cobrarán conciliaciones de licencias anuales por el período completo durante el cual se agregaron. Por ejemplo, si tiene 100 usuarios activos hoy, debe comprar una suscripción de 100 usuarios. Supongamos que cuando renueve el próximo año tiene 300 usuarios activos (200 usuarios adicionales). Cuando renueva, paga una suscripción de 300 usuarios y también paga la tarifa anual completa por los 200 usuarios que agregó durante el año.
    - question: ¿Qué sucede cuando mi suscripción está a punto de caducar o ha caducado?
      answer: >-
        Si la última vez utilizó un código de activación para activar su plan de pago de GitLab, una vez que renueve su suscripción, los nuevos términos de suscripción se sincronizarán con su instancia Self-Managed de GitLab durante la próxima [sincronización de licencias](https://docs.gitlab.com/ee/subscriptions/self_managed/#license-sync). Si la última vez utilizó una clave de licencia, recibirá una nueva licencia que deberá cargar en su instancia de GitLab. Para hacerlo, siga [estas instrucciones](https://docs.gitlab.com/ee/user/admin_area/license.html){: data-ga-name="licence"}{:data-ga-location="body"}.
    - question: ¿Qué pasa si decido no renovar mi suscripción?
      answer: >-
        14 días después del final de su suscripción, su clave ya no funcionará y GitLab Enterprise Edition ya no funcionará. Podrá cambiar a GitLab Community Edition, que es de uso gratuito.
    - question: ¿Puedo adquirir una combinación de licencias?
      answer: >-
        No, todos los usuarios del grupo GitLab.com o de la instancia Self-Managed deben estar en el mismo plan.
    - question: ¿Cómo funciona la clave de licencia?
      answer: >-
        La clave de la licencia es un archivo estático que, al cargarlo, permite a GitLab Enterprise Edition utilizar funcionalidades de pago. Durante la carga de la licencia, verificamos que los usuarios activos en su instancia de GitLab Enterprise Edition no excedan el nuevo número de usuarios. Durante el período de licencia, puede agregar tantos usuarios como quiera. La clave de la licencia caducará después de un año para los suscriptores de GitLab. A partir de 2022, las claves de licencia son un método heredado para activar los planes de pago de GitLab y fueron sustituidas por un código de activación para la mayoría de las suscripciones de pago.
    - question: ¿Qué es un código de activación?
      answer: >-
        Un código de activación hace referencia al método de GitLab para activar su suscripción self-managed con licencias en la nube, que proporciona una experiencia de suscripción más fluida. Esto es obligatorio para todos los clientes con la versión 14.1 o superior. Para obtener más información sobre las licencias en la nube, consulte [¿Qué es Cloud Licensing?](https://about.gitlab.com/pricing/licensing-faq/cloud-licensing/), así como [¿Cómo activo la licencia con un código de activación?](https://docs.gitlab.com/ee/user/admin_area/license.html).
    - question: ¿Qué funcionalidades y permisos están disponibles para los usuarios de Enterprise Agile Planning?
      answer: >-
        El complemento Enterprise Agile Planning está disponible para las suscripciones a GitLab Ultimate y se integra a la perfección en la plataforma GitLab DevSecOps, lo que permite a los usuarios no técnicos colaborar con los ingenieros a lo largo del ciclo de vida de desarrollo del software. Se debe invitar con acceso de informador a los usuarios del complemento Enterprise Agile Planning. Esto les proporciona la capacidad de ver e interactuar con la información del proyecto, incluidos tickets, hitos y tableros. Este nivel de acceso garantiza que las partes interesadas no técnicas puedan mantenerse informadas sobre el progreso del proyecto y proporcionar información valiosa. Ver los permisos para los usuarios con acceso de informador [aquí](https://docs.gitlab.com/ee/user/permissions.html).
    - question: ¿Qué es un usuario?
      answer: >-
        Usuario hace referencia a cada usuario final individual (persona o máquina) del Cliente y/o sus Afiliados (incluidos, entre otros, empleados, agentes y consultores de los mismos) con acceso al Software en virtud del presente. Cada uno de los usuarios (con las [siguientes excepciones](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined)) dentro de un espacio de nombres (grupo de nivel superior) se cuenta en una suscripción.
    - question: ¿Los precios indicados son con todo incluido?
      answer: >-
        Los precios indicados pueden estar sujetos a impuestos locales y de retención aplicables. Los precios pueden variar cuando se compran a través de un socio o revendedor.
    - question: ¿Qué funcionalidades se incluyen en GitLab.com, self-managed y dedicadas en todos los planes de precios?
      answer: >-
        Puede encontrar una lista actualizada en la [página de funcionalidades](/features/).
    - question: ¿Puedo importar mis proyectos desde otro proveedor?
      answer: >-
        Sí. Puede importar sus proyectos desde la mayoría de los proveedores existentes, incluidos GitHub y Bitbucket. [Consulte nuestra documentación](https://docs.gitlab.com/ee/user/project/import/index.html) para conocer todas sus opciones de importación.
    - question: ¿Tienen precios especiales para proyectos de código abierto, instituciones educativas o empresas emergentes?
      answer: >-
        ¡Sí! Ofrecemos licencias Ultimate gratuitas, junto con 50 000 minutos de cálculo al mes, para los proyectos de código abierto, las instituciones educativas y las empresas emergentes que reúnan los requisitos. Para obtener más información, visite nuestras páginas del programa [GitLab para el código abierto](/solutions/open-source/), [GitLab para la educación](/solutions/education/) y [GitLab para startups](/solutions/startups/).
    - question: ¿Cómo determina GitLab qué funcionalidades futuras se incluyen en determinados niveles?
      answer: >-
        En esta página representamos nuestras [capacidades](/company/pricing/#capabilities) y están pensadas como filtros en nuestro modelo de precios de [núcleo abierto basado en el comprador](/company/pricing/#buyer-based-tiering-clarification). Puede obtener más información sobre cómo tomamos decisiones de niveles en nuestra página del [manual de precios](https://handbook.gitlab.com/handbook/ceo/pricing).
    - question: ¿Cuáles son las diferencias entre los planes Gratis, Premium y Ultimate?
      answer: >-
        Todas las funcionalidades y beneficios de las diferentes ofertas de GitLab se pueden encontrar en las [páginas de comparación de funcionalidades](/features/). Obtenga más información sobre [Premium](/pricing/premium) y [Ultimate](/pricing/ultimate) para saber qué nivel es el adecuado para usted.
    - question: ¿Cuál es la diferencia entre GitLab y otras soluciones de DevSecOps?
      answer: >-
        Puede ver todas las diferencias entre GitLab y otras soluciones populares de DevSecOps en nuestras [páginas de comparación de la competencia](/devops-tools/).
    - question: ¿En qué consiste la asistencia?
      answer: >-
        Para los planes de pago, las horas en las que su solicitud de asistencia tiene un SLA dependen del [Impacto de la asistencia](/support/#definitions-of-support-impact) de la propia solicitud. Un problema en el nivel de emergencia (Gravedad 1) recibiría asistencia las 24 horas del día, los 7 días de la semana, mientras que otros niveles de impacto de asistencia recibirían asistencia las 24 horas del día. Para obtener más información sobre las horas de asistencia, consulte [Definiciones de las horas de asistencia global de GitLab](/support/#definitions-of-gitlab-global-support-hours) y [Efecto en las horas de asistencia si se elige una región preferida para la asistencia](/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen)
    - question: ¿Dónde está alojado GitLab.com?
      answer: >-
        Actualmente estamos alojados en Google Cloud Platform en los EE. UU.
    - question: ¿Qué funcionalidades no están disponibles en GitLab.com?
      answer: >-
        Algunas funcionalidades son exclusivas de self-managed y no se aplican a GitLab.com. Puede encontrar una lista actualizada en la [página de funcionalidades](/features/).
    - question: ¿Qué son los minutos de cálculo?
      answer: >-
        Los minutos de cálculo se utilizan durante el tiempo de ejecución para sus pipelines en nuestros runners alojados para GitLab.com. La ejecución en sus propios runners no usará sus minutos de cálculo y es ilimitada.
    - question: ¿Qué pasa si uso todos mis minutos de cálculo?
      answer: >-
        Si usa todos sus minutos de cálculo, puede [gestionar su uso de minutos de cálculo](/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage), [comprar minutos de cálculo adicionales](https://docs.gitlab.com/ee/ci/pipelines/compute_minutes.html#purchase-additional-compute-minutes) o cambiar el plan de su cuenta a Premium o Ultimate. Puede seguir utilizando sus propios runners aunque alcance sus límites.
    - question: ¿La cuota de minutos de cálculo se aplica a todos los runners?
      answer: >-
        No. Solo restringiremos el uso de nuestros runners alojados en GitLab.com. Si tiene una [configuración específica de runner para sus proyectos](https://docs.gitlab.com/runner/), no hay límite para su tiempo de compilación en GitLab.com.
    - question: ¿Aumenta la cuota de minutos de cálculo en función de la cantidad de usuarios del grupo o de la cantidad de usuarios de la suscripción?
      answer: >-
        No. La cuota se aplicará a un grupo, independientemente de la cantidad de usuarios del grupo o de la cantidad de usuarios de la suscripción.
    - question: ¿Por qué necesito ingresar los datos de la tarjeta de crédito/débito para obtener minutos de cálculo gratuitos?
      answer: >-
        Ha habido un aumento repentino masivo en el abuso de los minutos gratuitos de cálculo disponibles en GitLab.com para minar criptomonedas. Esto crea problemas intermitentes de rendimiento para los usuarios de GitLab.com. Para desalentar este tipo de abuso, los datos de la tarjeta de crédito/débito son obligatorios si decide usar los runners alojados para GitLab.com. Los datos de la tarjeta de crédito/débito no son obligatorios si usa su propio runner o desactiva los runners de instancia de GitLab.com. Cuando proporcione la información de la tarjeta, se verificará con una transacción de autorización por el monto de un dólar. No se efectuará ningún cargo y no se transferirá dinero. Obtenga más información [aquí](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="crypto mining"}{data-ga-location="body"}.
    - question: ¿Existe un límite de minutos de cálculo diferente para los proyectos públicos?
      answer: >-
        Sí. Los proyectos públicos que se crearon después del 17 de julio de 2021 tendrán una asignación de [minutos de cálculo](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#compute-minutes) de la siguiente manera: nivel gratuito: 50 000 minutos de cálculo, nivel Premium: 1 250 000 minutos de cálculo, nivel Ultimate: 6 250 000 minutos de cálculo.
    - question: ¿Puedo usar mis propios runners de CI de GitLab?
      answer: >-
        Sí, puede usar su propio runner de CI de GitLab con todos los planes.
    - question: ¿Qué es el límite de 5 usuarios en el nivel gratuito de GitLab.com?
      answer: El límite de 5 usuarios se aplica solo a los usuarios del nivel gratuito de la oferta de GitLab.com en un grupo principal con visibilidad privada. Estos cambios no se aplican a los usuarios del nivel gratuito de la oferta de GitLab.com en un grupo principal público, los niveles de pago, el nivel gratuito de la oferta self-managed y los [programas comunitarios](https://about.gitlab.com/community/), incluidos los usuarios de GitLab para el código abierto, GitLab para la educación y GitLab para startups. Más información [aquí](https://docs.gitlab.com/ee/user/free_user_limit.html).
  schema_org: '
    {
      "@context": "https://schema.org/",
      "@type": "Product",
      "name": "GitLab DevSecOps Platform",
      "description": "The most comprehensive AI-powered DevSecOps Platform that enables organizations to develop, secure, and operate software in a single application.",
      "image": "https://images.ctfassets.net/xz1dnu24egyd/1hnQd13UBU7n5V0RsJcbP3/769692e40a6d528e334b84f079c1f577/gitlab-logo-100.png",
      "brand": {
        "@type": "Corporation",
        "name": "GitLab",
        "logo": "https://images.ctfassets.net/xz1dnu24egyd/KpJoqcRLUFBL1AqKt9x0R/4fd439c21cecca4881106dd0069aa39c/gitlab-logo-extra-whitespace.png"
      },
      "offers": [{
          "@type": "Offer",
          "name": "Free",
          "description": "Use GitLab for personal projects",
          "Price": "0.00",
          "priceCurrency": "USD",
          "availability": "https://schema.org/InStock"
        },{
          "@type": "Offer",
          "name": "Premium",
          "description": "For scaling organizations and multi-team usage",
          "priceSpecification": {
            "@type": "UnitPriceSpecification",
            "price": "29.00",
            "priceCurrency": "USD",
            "unitCode": "MON",
            "unitText": "per user per month"
          },
          "url": "https://about.gitlab.com/pricing/premium/",
          "availability": "https://schema.org/InStock"
        },{
          "@type": "Offer",
          "name": "Ultimate",
          "description": "For enterprises looking to deliver software faster",
          "priceSpecification": {
            "@type": "PriceSpecification",
            "description": "Please contact for current pricing information",
            "priceCurrency": "USD"
          },
          "url": "https://about.gitlab.com/pricing/ultimate/",
          "availability": "https://schema.org/InStock"
        }
      ]
    }'
  banner:
    title: "GitLab Premium y GitLab Duo Pro: $19 para nuevos clientes de pequeñas empresas"
    primary_button:
      text: Más información
      href: /pricing/smb-promo/
      data_ga_name: smb-pricing # no need to translate
      data_ga_location: promo-banner # no need to translate
  heading: Obtenga la plataforma de DevSecOps con tecnología de IA
  modal_content:
    header: Seleccione su configuración de alojamiento preferida
    saas:
      header: GitLab.com
      subtitle: (Nosotros nos encargamos del alojamiento).
      experience_notice: No se requiere configuración técnica.
      description: No tiene que preocuparse por descargar e instalar GitLab por su cuenta.
    self_managed:
      header: GitLab Self-Managed
      subtitle: (Usted se encarga del alojamiento).
      experience_notice: Se requiere experiencia en Linux.
      description: Descargue e instale GitLab en su propia infraestructura o en nuestro entorno de nube pública.
  free_trial_cta:
    title: ¿Tiene más preguntas? Póngase en contacto.
    subtitle: Estamos aquí para ayudar. Hable con un experto hoy mismo.
  feature_comparison:
    header: Comparar todas las funcionalidades
    rows:
      - text: Calcular minutos por mes
        values:
          free:
            desktop: 400 minutos de cálculo
            mobile: "400"
          premium:
            desktop: 10 000 minutos de cálculo
            mobile: 10K
          ultimate:
            desktop: 50 000 minutos de cálculo
            mobile: 50K
      - text: Use sus propios runners de CI/CD de GitLab
        tier_level: 0
      - text: Incluye sitios web estáticos gratuitos
        tier_level: 0
      - text: Revisiones de código más rápidas
        link: /features/?stage=create#code_review_workflow
        tier_level: 1
      - text: CI/CD avanzadas
        link: https://about.gitlab.com/solutions/continuous-integration/
        tier_level: 1
      - text: Control de lanzamientos
        tier_level: 1
      - text: Confiabilidad autogestionada
        tier_level: 1
      - text: Asistencia
        tier_level: 1
      - text: Team planning
        tier_level: 1
      - text: Enterprise Agile Planning
        link: https://about.gitlab.com/solutions/agile-delivery/
        tier_level: 2
      - text: Pruebas de seguridad avanzadas
        link: https://about.gitlab.com/solutions/security-compliance/
        tier_level: 2
      - text: Mitigación de riesgos de seguridad
        tier_level: 2
      - text: Cumplimiento
        link: https://about.gitlab.com/solutions/compliance/
        tier_level: 2
      - text: Gestión de portafolios
        link: https://about.gitlab.com/solutions/portfolio-management/
        tier_level: 2
      - text: Gestión del flujo de valor
        link: https://about.gitlab.com/solutions/value-stream-management/
        tier_level: 2
      - text: Usuarios invitados gratis
        tier_level: 2
  plans_by_type:
    - name: SaaS
      label: GitLab.com
      id: saas
      description:
        text: Nosotros nos encargamos del alojamiento. No se requiere configuración técnica.
      tiers:
        - id: free
          level: 0
          title: Gratis
          description: Funcionalidades esenciales para usuarios individuales
          subtitle: Usar GitLab para proyectos personales
          monthly_price: 0
          price_text: "por usuario al mes,"
          billing_type: "no se requiere tarjeta de crédito"
          button:
            text: Comenzar
            url: https://gitlab.com/-/trial_registrations/new?_gl=1%2A1c025i%2A_ga%2AMTYyOTY3MzIyLjE2ODcyNzM0MTI.%2A_ga_ENFH3X7M5Y%2AMTY4NzI3MzQxMS4xLjAuMTY4NzI3MzQxNC4wLjAuMA
            data_ga_name: sign up
            data_ga_location: saas pricing
          links:
            hosted:
              url: https://gitlab.com/-/trial_registrations/new
              cta_text: Prueba gratuita de GitLab.com
            self_managed:
              url: /install/
              cta_text: Instalación self-managed
          button_copy: Comenzar
          table_button_copy: Comenzar
          data_ga_name: free-modal
          data_ga_location: pricing-tier
          features_id:
          features:
            header: "Funcionalidades gratuitas:"
            list:
              - text: 400 minutos de cálculo por mes
                links:
                  - url: "#why-do-i-need-to-enter-credit-debit-card-details-for-free-compute-minutes"
                    text: "[1]"
                    data_ga_name: "why enter credit debit"
                    data_ga_location: body
              - text: 5 usuarios por grupo principal
                links:
                  - url: "#what-is-the-5-user-limit-on-the-gitlab-com-free-tier"
                    text: "[4]"
                    data_ga_name: "what-is-the-5-user-limit-on-the-gitlab-com-saas-free-tier"
                    data_ga_location: body
        - id: silver-premium
          level: 2
          title: Premium
          description: Mejore la productividad y la coordinación del equipo
          subtitle: Para organizaciones de gran tamaño y uso de múltiples equipos
          pill: "Ideal para pequeñas empresas"
          monthly_price: 29
          annual_price: 348
          price_text: por usuario al mes,
          billing_type: facturación anual
          button:
            text: Comprar GitLab Premium
            url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
            data_ga_name: buy premium
            data_ga_location: saas pricing
          links:
            hosted:
              url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
              cta_text: Comprar SaaS
            self_managed:
              url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a01176f0d50a0176f3043c4d4a53&test=capabilities
              cta_text: Compre self-managed
          button_copy: Comprar GitLab Premium
          table_button_copy: Comprar Premium
          data_ga_location: pricing-tier
          data_ga_name: premium-modal
          features_id: gitlab_premium
          features:
            header: "Todo lo del plan Gratis, más: "
            learn_more:
              text: Más información sobre Premium
              url: '/pricing/premium/'
              data_ga_location: saas pricing
              data_ga_name: premium learn more
            list:
              - text: Propiedad de código y ramas protegidas
                information:
                - text: Reglas de aprobación para la revisión del código
                - text: Múltiples aprobadores en la revisión de código
                - text: Ramas protegidas
                - text: Reglas de push
                - text: Análisis de revisión de código
                - text: Informes de calidad del código
              - text: Solicitudes de fusión con reglas de aprobación
                information:
                  - text: Aprobadores requeridos
                  - text: Dependencias de solicitudes de fusión
                  - text: Acceso de fusión restringido
                  - text: Revisiones de solicitudes de fusión
              - text: Planificación de equipos
                information:
                  - text: Épicas de un solo nivel
                  - text: Planes de desarrollo
                  - text: Múltiples personas asignadas
                  - text: Promover el ticket a épica
                  - text: Listas de hitos del panel de tickets
                  - text: Etiquetas con alcance
                  - text: Panel de tickets de múltiples grupos
              - text: CI/CD avanzadas
                information:
                  - text: Panel de pipelines de CI/CD
                  - text: CI/CD para repositorio externo
                  - text: Trenes de fusión
                  - text: Plantillas externas
              - text: Gestión de incidentes y usuarios del plan Enterprise
                information:
                  - text: Entornos de trabajo de cumplimiento personalizados
                  - text: Configuraciones de sincronización LDAP y múltiples servidores LDAP
                  - text: Temporizador de cuenta regresiva y acciones del acuerdo de nivel de servicio
                  - text: Políticas de escalamiento de SLA y gestión de horarios
                  - text: Equilibrio de carga de la base de datos y registros geográficos
              - text: Asistencia
              - text: 10 000 minutos de cálculo por mes
        - id: gold-ultimate
          level: 3
          title: Ultimate
          description: Seguridad, cumplimiento y planificación en toda la organización
          subtitle: Para las empresas que buscan entregar software más rápido
          blurb: Para cuando su software de misión crítica requiera seguridad, cumplimiento y planificación en toda la organización
          pill: "Ideal para empresas"
          monthly_price: 99
          annual_price: 1188
          price_text: por usuario al mes,
          billing_type: Facturación anual a $1188 USD
          links:
            hosted:
              url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
              cta_text: Comprar GitLab.com
            self_managed:
              url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a00c76f0c6c20176f2f9328b33c9&test=capabilities
              cta_text: Compre self-managed
          button_copy: Comprar GitLab Ultimate
          table_button_copy: Comprar Ultimate
          data_ga_location: pricing-tier
          data_ga_name: ultimate-modal
          button:
            text: Comprar GitLab Ultimate
            url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
            data_ga_name: buy ultimate
            data_ga_location: saas pricing
          additonal_button:
            url: /sales/
            text: Hablar con el equipo de ventas
            data_ga_name: sales
            data_ga_location: saas pricing
            ld_variant: primary
          features_id: gitlab_ultimate
          features:
            header: "Todo lo del plan Premium, más: "
            learn_more:
              text: Más información sobre Ultimate
              data_ga_location: saas pricing
              data_ga_name: ultimate learn more
              url: '/pricing/ultimate/'
            list:
              - text: Pruebas dinámicas de seguridad de las aplicaciones
              - text: Paneles de seguridad
                information:
                  - text: Ver tendencias para todos los análisis por proyecto y grupo
                  - text: Alertas de seguridad
              - text: Gestión de vulnerabilidades
              - text: Análisis de dependencias
              - text: Análisis de contenedores
                links:
                  - url: "#features-and-benefits"
                    text: "[1]"
                    data_ga_name: "features and benefits"
                    data_ga_location: body
              - text: Pruebas estáticas de seguridad de las aplicaciones
                links:
                  - url: "#features-and-benefits"
                    text: "[2]"
                    data_ga_name: "features and benefits"
                    data_ga_location: body
              - text: Épicas multinivel
              - text: Enterprise Agile Planning
              - text: Gestión de portafolios
                information:
                  - text: Ayuda a organizar y gestionar grandes proyectos en toda la organización.
              - text: Roles personalizados
                information:
                  - text: Los roles personalizados permiten a los clientes adaptar los niveles de permisos para la finalización de tareas.
              - text: Gestión del flujo de valor
                information:
                  - text: Auditar flujos de eventos
                  - text: Flujos de valor personalizables e informes de análisis
                  - text: Métricas DORA-4
              - text: 50 000 minutos de cálculo por mes
              - text: Usuarios invitados gratis
    - name: Self Managed
      id: self-managed
      label: Self Managed
      description:
        text: Usted se encarga del alojamiento, instale GitLab en su propia configuración.
        tooltip: Se requiere experiencia en Linux.
      tiers:
        - id: free
          level: 0
          title: Gratis
          description: Funcionalidades esenciales para usuarios individuales
          subtitle: Usar GitLab para proyectos personales
          monthly_price: 0
          price_text: "por usuario al mes,"
          billing_type: "No se requiere tarjeta de crédito"
          button:
            text: Comenzar
            url: /install/
            data_ga_name: sign up
            data_ga_location: self managed pricing
          features_id:
          features:
            header: "Funcionalidades gratuitas: "
            list:
              - text: Control total sobre su implementación de GitLab
              - text: Use su propio almacenamiento y runners
        - id: silver-premium
          level: 2
          title: Premium
          description: Mejore la productividad y la coordinación del equipo
          subtitle: Para organizaciones de gran tamaño y uso de múltiples equipos
          pill: "Ideal para pequeñas empresas"
          monthly_price: 29
          annual_price: 348
          price_text: por usuario al mes,
          billing_type: facturación anual
          button:
            text: Comprar GitLab Premium
            url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a01176f0d50a0176f3043c4d4a53&test=capabilities
            data_ga_name: buy premium
            data_ga_location: self managed pricing
          features_id: gitlab_premium
          features:
            header: "Todo lo del plan Gratis, más: "
            learn_more:
              text: Más información sobre Premium
              url: '/pricing/premium/'
              data_ga_location: self managed pricing
              data_ga_name: premium learn more
            list:
              - text: Propiedad de código y ramas protegidas
                information:
                - text: Reglas de aprobación para la revisión del código
                - text: Múltiples aprobadores en la revisión de código
                - text: Ramas protegidas
                - text: Reglas de push
                - text: Análisis de revisión de código
                - text: Informes de calidad del código
              - text: Solicitudes de fusión con reglas de aprobación
                information:
                  - text: Aprobadores requeridos
                  - text: Dependencias de solicitudes de fusión
                  - text: Acceso de fusión restringido
                  - text: Revisiones de solicitudes de fusión
              - text: Enterprise Agile Planning
                information:
                  - text: Épicas de un solo nivel
                  - text: Planes de desarrollo
                  - text: Múltiples personas asignadas
                  - text: Promover el ticket a épica
                  - text: Listas de hitos del panel de tickets
                  - text: Etiquetas con alcance
                  - text: Panel de tickets de múltiples grupos
              - text: CI/CD avanzadas
                information:
                  - text: Panel de pipelines de CI/CD
                  - text: CI/CD para repositorio externo
                  - text: Trenes de fusión
                  - text: Plantillas externas
              - text: Gestión de incidentes y usuarios del plan Enterprise
                information:
                  - text: Entornos de trabajo de cumplimiento personalizados
                  - text: Configuraciones de sincronización LDAP y múltiples servidores LDAP
                  - text: Temporizador de cuenta regresiva y acciones del acuerdo de nivel de servicio
                  - text: Políticas de escalamiento de SLA y gestión de horarios
                  - text: Equilibrio de carga de la base de datos y registros geográficos
              - text: Asistencia
        - id: gold-ultimate
          level: 3
          title: Ultimate
          description: Seguridad, cumplimiento y planificación en toda la organización
          subtitle: Para las empresas que buscan entregar software más rápido
          blurb: Para cuando su software de misión crítica requiera seguridad, cumplimiento y planificación en toda la organización
          pill: "Ideal para empresas"
          monthly_price: 99
          annual_price: 1188
          price_text: por usuario al mes,
          billing_type: Facturación anual de $1188 USD
          button:
            text: Comprar GitLab Ultimate
            url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a00c76f0c6c20176f2f9328b33c9&test=capabilities
            data_ga_name: buy ultimate
            data_ga_location: self managed pricing
          additonal_button:
            url: /sales/
            text: Comuníquese con Ventas
            data_ga_name: sales
            data_ga_location: self managed pricing
          features_id: gitlab_ultimate
          features:
            header: "Todo lo del plan Premium, más: "
            learn_more:
              text: Más información sobre Ultimate
              data_ga_location: self managed pricing
              data_ga_name: ultimate learn more
              url: '/pricing/ultimate/'
            list:
              - text: Pruebas dinámicas de seguridad de las aplicaciones
              - text: Paneles de seguridad
                information:
                  - text: Ver tendencias para todos los análisis por proyecto y grupo
                  - text: Alertas de seguridad
              - text: Gestión de vulnerabilidades
              - text: Análisis de dependencias
              - text: Análisis de contenedores
              - text: Pruebas estáticas de seguridad de las aplicaciones
              - text: Épicas multinivel
              - text: Gestión de portafolios
                information:
                  - text: Ayuda a organizar y gestionar grandes proyectos en toda la organización.
              - text: Roles personalizados
                information:
                  - text: Los roles personalizados permiten a los clientes adaptar los niveles de permisos para la finalización de tareas.
              - text: Gestión del flujo de valor
                information:
                  - text: Auditar flujos de eventos
                  - text: Flujos de valor personalizables e informes de análisis
                  - text: Métricas DORA-4
              - text: Usuarios invitados gratis
    - name: Dedicated
      id: dedicated
      label: Dedicated
      description:
        text: SaaS para un solo inquilino, totalmente gestionado por nosotros.
        tooltip: Compromiso de 1000 cupos
      tiers:
        - id: dedicated
          title: GitLab Dedicated
          description: Para empresas que necesitan aislamiento, residencia y protección de datos.
          pill: "Ideal para empresas"
          features_id: gitlab_dedicated
          features:
            header: "Todo lo del plan Ultimate, más:"
            button:
              text: Comuníquese con nosotros para conocer los precios
              data_ga_location: dedicated pricing
              data_ga_name: dedicated pricing
              url: '/sales/'
            learn_more:
              text: Más información sobre Dedicated
              data_ga_location: dedicated pricing
              data_ga_name: dedicated learn more
              url: '/dedicated/'
            list:
              - text: Totalmente gestionado por GitLab
              - text: Residencia de datos en la región de su elección
              - text: Aislamiento completo de datos y del código fuente
              - text: Use su propio cifrado de claves
              - text: Seguridad de nivel empresarial
              - text: Cadencia de actualización normal
        - id: dedicated-government-tier
          title: GitLab Dedicated para organismos públicos
          description: Para organismos públicos y clientes en sectores muy regulados.
          pill: "Próximamente"
          features_id: gitlab_dedicated
          features:
            header: "Todo lo incluido en los planes Ultimate y Dedicated, más:"
            button:
              text: Póngase en contacto con nosotros para obtener más información
              data_ga_location: dedicated government pricing
              data_ga_name: contact us
              url: '/sales/'
            learn_more:
              text: Más información sobre GitLab para el sector público
              data_ga_location: dedicated government pricing
              data_ga_name: learn more
              url: '/solutions/public-sector/'
            list:
              - text: Alojado en una infraestructura como servicio compatible con FedRAMP
              - text: Cumple con los controles de seguridad de línea de base moderada de FedRAMP
              - text: Diseñado para organismos y proveedores de servicios del sector público de EE. UU.
              - text: Implementado en AWS GovCloud