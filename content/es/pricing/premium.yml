---
  title: ¿Por qué elegir GitLab Premium?
  description: Mejore la productividad y la coordinación del equipo con GitLab Premium.
  side_menu:
    anchors:
      text: En esta página
      data:
      - text: Descripción general
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Resumen
          href: "#wp-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Soluciones clave
          href: "#wp-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Estudios de caso de clientes
          href: "#wp-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: Calculadora de ROI
        href: "#wp-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Funcionalidades Premium
        href: "#wp-premium-features"
        data_ga_name: premium-features
        data_ga_location: side-navigation
        nodes:
        - text: Revisiones de código más rápidas
          href: "#wp-faster-code-reviews"
          data_ga_name: faster-code-reviews
          data_ga_location: side-navigation
        - text: CI/CD avanzadas
          href: "#wp-advanced-ci-cd"
          data_ga_name: advanced-ci-cd
          data_ga_location: side-navigation
        - text: Entrega ágil en empresas
          href: "#wp-enterprise-agile-planning"
          data_ga_name: nterprise-agile-planning
          data_ga_location: side-navigation
        - text: Controles de lanzamientos
          href: "#wp-release-controls"
          data_ga_name: release-controls
          data_ga_location: side-navigation
        - text: Confiabilidad autogestionada
          href: "#wp-self-managed-reliability"
          data_ga_name: self-managed-reliability
          data_ga_location: side-navigation
        - text: Otras funcionalidades Premium
          href: "#wp-other-premium-features"
          data_ga_name: other-premium-features
          data_ga_location: side-navigation
      - text: Póngase en contacto
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Próximos pasos
      data:
      - text: Comprar Premium ahora
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-premium
        data_ga_location: side-navigation
      - text: Más información sobre Ultimate
        href: /pricing/ultimate/
        variant: secondary
        icon: true
        data_ga_name: learn-about-ultimate
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wp-summary
      title: ¿Por qué elegir Premium?
      subtitle: Ideal para mejorar la productividad y la coordinación del equipo
      text: "Disponible tanto en SaaS como en opciones de implementación autogestionadas, GitLab Premium ayuda a mejorar la productividad y la colaboración del equipo a través de:"
      list: 
        - Revisiones de código más rápidas
        - Controles de lanzamientos
        - Asistencia prioritaria
        - CI/CD avanzadas
        - Asistencia para la actualización en tiempo real
        - Entrega ágil en empresas
        - Technical Account Manager para clientes elegibles
        - Alta disponibilidad, recuperación ante desastres para instancias self-managed
      cta: 
        text: Comparar todas las funcionalidades
        url: /pricing/feature-comparison/
      buttons:
        - variant: primary
          icon: false
          text: Comprar Premium ahora
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: buy-premium-now
          data_ga_location: body
        - variant: secondary
          icon: true
          text: Más información sobre Ultimate
          url: /pricing/ultimate/
          data_ga_name: learn-about-ultimate
          data_ga_location: body
  - name: guest-calculator
    data:
      id: wp-guest-calculator
      title: Calcule el costo para su organización
      input_title: GitLab ofrece usuarios invitados gratis ilimitados en los planes Ultimate
      caption: |
        *Todos los planes tienen facturación anual. Los precios indicados pueden estar sujetos a impuestos locales y de retención aplicables. Los precios pueden variar cuando se compran a través de un socio o revendedor. Consulte nuestra [página de precios](/pricing/){data-ga-name="pricing" data-ga-location="guest calculator"} para obtener más información.
      seats:
        title: Cantidad de cupos de GitLab
        tooltip: Los cupos de GitLab son personas con acceso de informador, desarrollador, encargado de mantenimiento o propietario. Los usuarios invitados no consumen un cupo en GitLab Ultimate. Más información sobre el uso de cupos <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined" data-ga-name="seat usage documentation" data-ga-location="user calculator tooltip">aquí</a>.
      guests:
        title: Cantidad de usuarios invitados
        tooltip: Los usuarios invitados pueden crear y asignar tickets, ver ciertos análisis, opcionalmente ver el código y mucho más. Más información <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions" data-ga-name="project members documentation" data-ga-location="user calculator tooltip">aquí</a>.
      premium:
        title: Costo mensual de Premium*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Comprar Premium
      ultimate:
        title: Costo mensual de Ultimate*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Comprar Ultimate
  - name: 'by-solution-value-prop'
    data:
      id: wp-key-solutions
      light_background: true
      small_margin: true
      large_card_on_bottom: true
      title: GitLab Premium te ayuda a
      cards:
        - icon:
            name: increase
            alt: Ícono de incremento
            variant: marketing
          title: Incrementar la eficiencia operativa
          description: GitLab Premium presenta capacidades que permiten a las empresas analizar las tendencias de equipos, proyectos y grupos para descubrir patrones y establecer estándares uniformes a fin de mejorar la productividad general.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Ícono de velocidad
          title: Ofrecer mejores productos más rápido
          description: Con CI/CD avanzadas y revisiones de código más rápidas, GitLab Premium lo ayuda a crear, mantener, implementar y supervisar mejor los pipelines de aplicaciones complejas para entregar productos más rápido.
        - icon:
            name: lock-alt-5
            alt: Ícono de candado
            variant: marketing
          title: Reducir el riesgo de seguridad y cumplimiento
          description: Los controles de lanzamientos en GitLab Premium garantizan que los equipos envíen código de alta calidad y seguro.
  - name: 'education-case-study-carousel'
    data:
      id: 'wp-customer-case-studies'
      case_studies:
        - logo_url: /nuxt-images/logos/nvidia-logo.svg
          text: Cómo GitLab Geo apoya la innovación de NVIDIA
          img_url: /nuxt-images/blogimages/nvidia.jpg
          case_study_url: /customers/nvidia/
          data_ga_name: Nvidia case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
          img_url: /nuxt-images/blogimages/fujitsu.png
          text: Fujitsu Cloud Technologies mejora la velocidad de implementación y los flujos de trabajo interfuncionales con GitLab
          case_study_url: /customers/fujitsu/
          data_ga_name: fujitsu case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/credit-agricole-logo-1.png
          img_url: /nuxt-images/blogimages/creditagricole-cover-image.jpg
          text: Cómo Crédit Agricole Corporate & Investment Bank (CACIB) transformó su flujo de trabajo global con GitLab
          case_study_url: /customers/credit-agricole
          data_ga_name: credit-agricole case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/esa-logo.svg
          img_url: /nuxt-images/blogimages/ESA_case_study_image.jpg
          text: Cómo la Agencia Espacial Europea utiliza GitLab para centrarse en las misiones espaciales
          case_study_url: /customers/european-space-agency
          data_ga_name: european-space-agency case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/sopra_steria-logo.wine.svg
          img_url: /nuxt-images/blogimages/soprasteria.jpg
          text: Cómo GitLab se convirtió en la piedra angular de la capacitación digital para Sopra Steria
          case_study_url: /customers/sopra_steria
          data_ga_name: sopra_steria case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/hemmersbach_logo.svg
          img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
          text: Hemmersbach reorganizó su cadena de compilación y aumentó la velocidad de compilación 59 veces
          case_study_url: /customers/hemmersbach
          data_ga_name: hemmersbach case study
          data_ga_location: case study carousel
  - name: roi-calculator-block
    data:
      id: wp-roi-calculator
      header:
        gradient_line: true
        title:
          text: Calculadora de ROI
          anchor: wp-roi-calculator
        subtitle: ¿Cuánto le cuesta su cadena de herramientas?
      calc_data_src: es/calculator/index
  features_block:
    id: wp-premium-features
    tier: premium
    header:
      gradient_line: true
      title:
        text: Funcionalidades Premium
        anchor: wp-premium-features
        button:
          text: Comparar todas las funcionalidades
          data_ga_name: Compare all features
          data_ga_location: body
          url: /es/pricing/feature-comparison/
    pricing_themes:
      - id: wp-faster-code-reviews
        theme: Las revisiones de código más rápidas
        text: garantizan un código de alta calidad en todos los equipos a través de flujos de trabajo fluidos de revisión de código.
        link:
          text: Más información
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/development/code_review.html
      - id: wp-advanced-ci-cd
        theme: Las CI/CD avanzadas
        text: le permiten crear, mantener, implementar y supervisar pipelines complejos.
        link:
          text: Más información
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: /solutions/continuous-integration/
      - id: wp-enterprise-agile-planning
        theme: La entrega ágil en empresas
        text: le ayuda a planificar y gestionar sus proyectos, programas y productos con soporte ágil integrado.
        link:
          text: Más información
          data_ga_name: Agile Planning learn more
          data_ga_location: body
          url: /solutions/agile-delivery/
      - id: wp-release-controls
        theme: Los controles de lanzamientos
        text: garantizan que los equipos envíen código de alta calidad y seguro.
        link:
          text: Más información
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
      - id: wp-self-managed-reliability
        theme: La confiabilidad autogestionada
        text: garantiza la recuperación ante desastres, la alta disponibilidad y el equilibrio de carga de su implementación autogestionada.
        link:
          text: Más información
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/administration/reference_architectures/#traffic-load-balancer
      - id: wp-other-premium-features
        text: Otras funcionalidades Premium