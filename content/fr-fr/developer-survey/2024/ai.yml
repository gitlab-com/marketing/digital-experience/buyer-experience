title: La maturité de l'IA dans l'approche DevSecOps
og_title: La maturité de l'IA dans l'approche DevSecOps
description: >-
  L'IA est de plus en plus largement utilisée, mais nos recherches montrent que la plupart des entreprises cherchent encore comment l'intégrer dans leur cycle de développement logiciel. Que vous ayez déjà adopté l'IA ou soyez en phase de réflexion, découvrez les avantages qu'elle peut apporter à votre entreprise.
twitter_description: >-
  L'IA est de plus en plus largement utilisée, mais nos recherches montrent que la plupart des entreprises cherchent encore comment l'intégrer dans leur cycle de développement logiciel. Que vous ayez déjà adopté l'IA ou soyez en phase de réflexion, découvrez les avantages qu'elle peut apporter à votre entreprise.
og_description: >-
  L'IA est de plus en plus largement utilisée, mais nos recherches montrent que la plupart des entreprises cherchent encore comment l'intégrer dans leur cycle de développement logiciel. Que vous ayez déjà adopté l'IA ou soyez en phase de réflexion, découvrez les avantages qu'elle peut apporter à votre entreprise.
og_image: /nuxt-images/developer-survey/2024/2024-devops-survey-ai-meta.jpg
twitter_image: /nuxt-images/developer-survey/2024/2024-devops-survey-ai-meta.jpg
form:
    tag: Gratuit
    header: Inscrivez-vous pour lire le rapport complet
    text: |
      Accédez gratuitement au rapport complet, comprenant des informations sur plus de 5 000 professionnels DevSecOps répartis dans 39 pays à travers le monde.

      **Contenu du rapport**

      - Les 4 étapes de la maturité de l'IA dans l'approche DevSecOps
      - L'engouement pour l'IA est terminé : le moment est venu de l'adopter de manière réfléchie
      - La question n'est pas de savoir s'il faut adopter l'IA, mais quand
      - Mise en œuvre de l'IA à chaque étape du cycle du développement logiciel
      - Sécurité et compétences : deux obstacles à une adoption plus large de l'IA
      - Comprendre l'impact de l'IA : le prochain grand défi

    formId: 1002 # Localized versions use a separate form ID
    formDataLayer: resources
    submitted:
      tag: Merci pour votre inscription
      text: Le Rapport Global DevSecOps 2024 sur l'IA vous sera envoyé sous peu.
      img_src: '/nuxt-images/developer-survey/2024/ai-report-img.png'
hero:
  year: Rapport Global DevSecOps 2024
  title: La maturité de l'IA dans l'approche DevSecOps
  img_src: '/nuxt-images/developer-survey/2024/ai-report-img.png'
  mobile_img_src: '/nuxt-images/developer-survey/2024/ai-mobile-hero-img.svg'
intro:
  survey_header:
    text: >-
      L'IA est de plus en plus largement utilisée, mais nos recherches montrent que la plupart des entreprises cherchent encore comment l'intégrer dans leur cycle de développement logiciel. Que vous ayez déjà adopté l'IA ou soyez en phase de réflexion, découvrez les avantages qu'elle peut apporter à votre entreprise.
    img_variant: 1
  summary:
    title: Synthèse du rapport
    text: |
      Notre rapport analyse les résultats d'une enquête menée par Omdia et GitLab. Nous avons interrogé plus de 5 000 professionnels du DevSecOps dans le monde entier pour évaluer comment les principes et pratiques DevSecOps sont perçus et intégrés au sein de leur entreprise.

      L'enquête de cette année révèle que si l'intelligence artificielle (IA) est en passe de faire partie intégrante du développement logiciel, son adoption varie en fonction des entreprises. Suite à l'engouement autour de cette tendance, la majorité des entreprises ont commencé à explorer l'IA pour des cas d'utilisation spécifiques, tandis que les pionnières dans ce domaine s'efforcent désormais d'affiner leur stratégie et de démontrer l'impact de l'IA sur leurs processus de développement logiciel.

  highlights:
    title: Points forts
    type: barGraph
    buttons:
      previous: Précédent
      next: Suivant
    gaLocation: summary
    blocks:
      - text: >-
          <div class="stat orange"><span class="number">60</span> %</div> des personnes interrogées estiment qu'il est essentiel pour elles d'intégrer l'IA dans leurs processus de développement logiciel afin de ne pas être distanciées
        dataGaName: ai is essential
      - text: >-
          <div class="stat teal"><span class="number">59</span> %</div> des personnes interrogées déclarent que leur entreprise est prête à adopter l'IA
        dataGaName: ai preparedness
      - text: >-
          <div class="stat red"><span class="number">39</span> %</div> des personnes interrogées indiquent désormais utiliser l'IA pour développer des logiciels, soit une hausse de 16 % par rapport à 2023
        dataGaName: ai usage
      - text: >-
          <div class="stat purple"><span class="number">55</span> %</div> des personnes interrogées estiment que l'utilisation de l'IA dans le cycle du développement logiciel présente des risques
        dataGaName: ai risk