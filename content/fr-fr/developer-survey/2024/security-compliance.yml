title: La sécurité des applications à l'ère du numérique
og_title: La sécurité des applications à l'ère du numérique
description: >-
  Les entreprises doivent faire face à un nombre croissant de surfaces d'attaque et d'incertitudes liées à l'IA. Découvrez les conclusions de notre enquête menée auprès de plus de 5 000 professionnels DevSecOps dans le monde entier.
twitter_description: >-
  Les entreprises doivent faire face à un nombre croissant de surfaces d'attaque et d'incertitudes liées à l'IA. Découvrez les conclusions de notre enquête menée auprès de plus de 5 000 professionnels DevSecOps dans le monde entier.
og_description: >-
  Les entreprises doivent faire face à un nombre croissant de surfaces d'attaque et d'incertitudes liées à l'IA. Découvrez les conclusions de notre enquête menée auprès de plus de 5 000 professionnels DevSecOps dans le monde entier.
og_image: /nuxt-images/developer-survey/2024/2024-devops-survey-security-compliance-meta.png
twitter_image: /nuxt-images/developer-survey/2024/2024-devops-survey-security-compliance-meta.png
form:
    tag: Gratuit
    header: Inscrivez-vous pour lire le rapport complet
    text: |
      Accédez gratuitement au rapport complet, comprenant des informations sur plus de 5 000 professionnels DevSecOps répartis dans 39 pays à travers le monde.

      **Thèmes abordés :**

      - Le boom du développement d'applications
      - Le statu quo de l'IA : quel impact en matière de sécurité ?
      - L'équilibre entre rapidité et sécurité
    formId: 1002 # Localized versions use a separate form ID
    formDataLayer: resources
    submitted:
      tag: Merci pour votre inscription
      text: Nous vous envoyons le rapport Global DevSecOps 2024 sur la sécurité et la conformité. Vous devriez le recevoir sous peu par e-mail
      img_src: '/nuxt-images/developer-survey/2024/security-hero.png'
hero:
  year: Rapport Global DevSecOps 2024
  title: La sécurité des applications à l'ère du numérique
  img_src: '/nuxt-images/developer-survey/2024/security-hero.png'
  mobile_img_src: '/nuxt-images/developer-survey/2024/sec-com-mobile-hero-img.svg'
intro:
  survey_header:
    text: >-
      Alors que le développement de logiciels est en plein essor, les entreprises doivent faire face à un nombre croissant de surfaces d'attaque et d'incertitudes liées à l'IA. Découvrez les conclusions de notre enquête menée auprès de plus de 5 000 professionnels DevSecOps dans le monde entier.
    img_variant: 1
  summary:
    title: Synthèse du rapport
    text: |
      Ce rapport analyse les résultats d'une enquête menée par Omdia et GitLab en avril 2024. Nous avons interrogé plus de 5 000 professionnels du DevSecOps dans le monde entier pour évaluer comment les principes et pratiques DevSecOps sont perçus et intégrés au sein de leur entreprise.

      L'enquête de cette année révèle un environnement propice aux failles de sécurité : les entreprises ressentent une pression accrue pour livrer des logiciels plus rapidement que jamais, et se tournent donc vers l'intelligence artificielle (IA) et les bibliothèques open source pour accélérer le cycle de développement. Cela augmente la surface d'attaque et suscite de nouvelles préoccupations concernant la sécurité et la confidentialité des outils d'IA. Toutefois, les résultats de notre enquête montrent également que les entreprises parviennent à concilier rapidité et sécurité en mettant en place des programmes stratégiques de sécurité des applications. Cette approche permet aux équipes DevSecOps de progresser plus rapidement tout au long du cycle de développement, sans compromettre la sécurité.
  highlights:
    title: Points forts
    type: barGraph
    buttons:
      previous: Précédent
      next: Suivant
    gaLocation: summary
    blocks:
      - text: >-
          <div class="stat orange"><span class="number">66 </span>%</div> des personnes interrogées déclarent que leur entreprise publie des logiciels plus rapidement, voire deux fois plus rapidement, comparé à l'année dernière
        dataGaName: rapidité de livraison
      - text: >-
          <div class="stat teal"><span class="number">67 </span>%</div> des développeurs déclarent que plus d'un quart du code sur lequel ils travaillent provient de bibliothèques open source. Seulement une entreprise sur cinq utilise actuellement une nomenclature logicielle (SBOM) pour documenter les composants des logiciels qu'elle développe
        dataGaName: bibliothèques open source
      - text: >-
          <div class="stat red"><span class="number">55 </span>%</div> des personnes interrogées estiment que l'introduction de l'IA dans le cycle du développement logiciel présente des risques, les principales préoccupations portant sur la confidentialité et la sécurité des données
        dataGaName: préoccupations ia
      - text: >-
          <div class="stat purple"><span class="number">34 </span>%</div> des personnes interrogées indiquent que leur entreprise a mis en place des tests dynamiques de sécurité des applications (DAST), soit une hausse de 8 % par rapport à 2023
        dataGaName: tests de sécurité
