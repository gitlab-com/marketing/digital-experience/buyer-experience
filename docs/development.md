# Setting up your development environment


## Dependencies

1. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. Create a GitLab account (internal employees and contractors will have one created through onboarding)
3. Install [Node and NPM](https://nodejs.org/en/download)
4. Install [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#mac-stable)
5. Ensure that you have VS Code or a text editor/IDE of your choice installed
6. Set up a SSH key pair with your GitLab account. [Configure your SSH client to point to the directory where the](https://docs.gitlab.com/ee/user/ssh.html#configure-ssh-to-point-to-a-different-directory) private key is stored


## Buyer Experience repository setup
1. In your terminal, run `git clone git@gitlab.com:gitlab-com/marketing/digital-experience/buyer-experience.git`
2. Create your `.env` file.
3. The following Contentful tokens are required to be added to the `.env` file:

```
CTF_SPACE_ID
CTF_CDA_ACCESS_TOKEN
CTF_PREVIEW_ACCESS_TOKEN
CTF_NAV_SPACE_ID
CTF_NAV_CDA_ACCESS_TOKEN
CTF_NAV_PREVIEW_ACCESS_TOKEN
```

4. Run `yarn install` or simply run `yarn` 
5. Run `yarn dev` for local development. This renders the site via server-side rendering
6. For additional troubleshooting or testing, you can run `yarn generate && yarn start`. This renders the site via static site generation, which is how we deploy our site to production

If this is your first time contributing to this project, it is highly recommend to look through our `nuxt.config.js`, various Typescript config and constant files, helpful utilities located at `/lib` and `/mixins`, our `/plugin` folder, and our documentation files (`/docs`).

## Code standards

1. We use Prettier and ESLint to maintain code standards
2. We run a Husky pre-push hook that will check for linting warnings and errors. Any errors will block you from pushing your code.
3. It's a good habit to run `yarn lintfix` before you committing your code

## Useful VS Code Extensions to enhance your productivity

1. [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
2. [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
3. [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow)

## Directories

Information about the directories in this project. 

### `assets`

The assets directory contains un-compiled assets, which only includes sass files for this project. 

More information about the usage of this directory in [Nuxt 2 the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains our Vue.js components. Components make up the different parts our page and can be reused and imported into pages, layouts and even other components.

More information about the usage of this directory in [Nuxt 2 the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

In a our Nuxt 2 application, the layouts directory is used to define the layout structure of the application. Layouts are a way to create different template structures that can be reused across multiple pages. They provide a consistent look and feel by allowing us to define headers, footers, sidebars, and other common elements in one place.

More information about the usage of this directory in [Nuxt 2 the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

### `pages`

The pages directory houses files that represent the pages of our application. While Nuxt automatically generates routes based on the files within this directory, we also incorporate extensive custom routing configurationsin the nuxt.config.js to manage more complex navigation scenarios. This combination allows us to leverage the simplicity of file-based routing while maintaining the flexibility needed for our advanced routing requirements.

More information about the usage of this directory in [Nuxt 2 the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory is used primarily for global helpers. This directory is where we register and configure custom JavaScript plugins that can be used throughout the application. These plugins might include utility functions, custom directives, or third-party library integrations, providing a centralized way to manage and access these global helpers across different components and pages.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

The static directory contains our static files, such as images not managed by Contentful and fonts. Each file inside this directory is directly mapped to the root URL (/), allowing these assets to be easily accessed without any additional processing or bundling by Nuxt. This setup ensures that our static assets are served efficiently and reliably.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

We use the store directory exclusively for integrating with Greenhouse. This directory contains all the Vuex store modules and configurations necessary to manage state related to our Greenhouse integration, ensuring that data and state management specific to this integration is centralized and efficiently handled.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).





