export interface Breadcrumb {
  title: string;
  href?: string;
  data_ga_name?: string;
  data_ga_location?: string;
  hidden?: boolean;
}

export interface BaseForm {
  formId: number;
  datalayer?: string;
  formDataLayer?: string;
  dataLayer?: string;
  form_header?: string;
  form_required_text?: string;
  all_required?: boolean;
  submitted_message?: {
    header?: string;
    body?: string;
  };
  header_config?: {
    text_tag?: string;
    text_variant?: string;
    text_align?: string;
  };
  external_form?: {
    url: string;
    width?: string;
    height?: string;
  };
}
