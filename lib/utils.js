/**
 * Utility function to parse a string into kebab-case
 *
 * @param {String} str - A String to be converted to kebab-case
 */
const toKebabCase = (str) =>
  str &&
  str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map((x) => x.toLowerCase())
    .join('-');

const toKebabCaseNonEnglish = (str) => {
  // Normalize the string to remove accents
  const normalizedStr = str
    .normalize('NFD') // Normalize to decomposed form
    .replace(/[\u0300-\u036f]/g, ''); // Remove accent marks

  // Replace spaces with hyphens and remove any remaining unwanted characters
  return normalizedStr
    .trim() // Remove leading and trailing whitespace
    .replace(/\s+/g, '-') // Replace one or more whitespace characters with '-'
    .toLowerCase(); // Convert the whole string to lowercase
};

module.exports = { toKebabCase, toKebabCaseNonEnglish };
