import Vue from 'vue';

export const AvailableLanguagesMixin = Vue.extend({
  /**
   * The validate method runs after the "middleware" method
   * Each component should handle the logic to populate the $availableLanguages in the "middleware" method
   * @param $availableLanguages
   * @param i18n
   */
  validate({ $availableLanguages, i18n }: any) {
    if (process.client) {
      return true;
    }

    return $availableLanguages?.some((locale) => locale === i18n.locale);
  },
});
