import Vue from 'vue';

export const JihuLocationMixin = Vue.extend({
  data() {
    return {
      geoIP: '',
    };
  },
  beforeMount() {
    this.getData();
  },
  methods: {
    async getData() {
      const response = await fetch(
        'https://epsilon.6sense.com/v3/company/details',
        {
          method: 'GET',
          headers: {
            Authorization: `Token 715ff02645f8e09a174845255d544dac8c178c02`,
          },
        },
      );

      const user = await response.json();

      if (user?.company?.geoIP_country !== '') {
        this.geoIP = String(user?.company?.geoIP_country);
      }

      return Promise.resolve(this.geoIP);
    },
  },
});
