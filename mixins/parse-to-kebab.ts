import Vue from 'vue';

export const toKebabCaseMixin = Vue.extend({
  methods: {
    anchorID(string) {
      return encodeURIComponent(
        String(string)
          .trim()
          .toLowerCase()
          .replace(/[^a-zA-Z ]/g, '')
          .replace(/\s+/g, '-'),
      );
    },
  },
});
