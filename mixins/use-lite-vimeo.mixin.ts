import Vue from 'vue';
import { UseLocalizedVideoSubtitles } from '~/mixins/use-localized-video-subtitles.mixin';
import Player from '@vimeo/player';
import { get } from 'lodash';

export const UseLiteVimeo = Vue.extend({
  mixins: [UseLocalizedVideoSubtitles],
  methods: {
    // Method to extract the Vimeo video ID from the URL
    getVimeoVideoId(url) {
      if (!url) {
        return null;
      }
      const regex = /vimeo\.com\/(?:video\/)?(\d+)/;
      const match = url.match(regex);
      const videoId = match ? match[1] : null;
      return videoId;
    },
    // Method to extract the Vimeo hash from the URL
    getVimeoHash(url) {
      if (url.includes('?')) {
        const queryString = url.split('?')[1];
        const urlParams = new URLSearchParams(queryString);
        const hash = urlParams.get('h');
        return hash;
      }
      return;
    },
    // Method to wait for the iframe to be available in the DOM
    waitForIframe(liteVimeo, callback) {
      const checkIframe = () => {
        const iframe = liteVimeo.shadowRoot.querySelector('iframe');
        if (iframe) {
          callback(iframe);
        } else {
          requestAnimationFrame(checkIframe);
        }
      };
      checkIframe();
    },

    async initializeVimeoPlayers() {
      this.$nextTick(async () => {
        const liteVimeoElements = document.querySelectorAll('lite-vimeo');
        if (liteVimeoElements.length === 0) {
          return;
        }
        for (let liteVimeo of liteVimeoElements) {
          // Prevent double initialization
          if (liteVimeo.playerInitialized) {
            continue;
          }

          // Get video ID from the lite-vimeo element
          const videoId = liteVimeo.getAttribute('videoid');
          if (!liteVimeo.style.backgroundImage) {
            // Set a default placeholder image to "What is GirLab placeholder" if thumbnail is missing
            liteVimeo.style.backgroundImage = `url('https://i.vimeocdn.com/video/1958210076-aa5457030b94b2e8bb28eb87c27e73261c0fc8f289b331314375761fd22e5c65-d.jpg')`;
          }
          // Ensure the thumbnail fits the container
          liteVimeo.style.backgroundSize = 'cover';
          liteVimeo.style.backgroundPosition = 'center';
          // hide broken image icon
          liteVimeo.shadowRoot.querySelector('picture').style.display = 'none';
          liteVimeo.shadowRoot.querySelector(
            '.lvo-playbtn',
          ).style.backgroundColor = 'white';
          const shadowRoot = liteVimeo.shadowRoot;

          // Create a new style element if one doesn't already exist
          let styleElement = shadowRoot.querySelector('style');
          if (!styleElement) {
            styleElement = document.createElement('style');
            shadowRoot.appendChild(styleElement);
          }

          // Add or modify the rule for .lvo-playbtn::before
          styleElement.sheet.insertRule(
            `
    .lvo-playbtn::before {
        border-color: transparent transparent transparent black;
    }
`,
            styleElement.sheet.cssRules.length,
          );

          // Function to initialize the Vimeo player
          const initializePlayer = (iframe) => {
            const player = new Player(iframe);
            const videoTitle = liteVimeo.getAttribute('videotitle');
            let eventLabel = '';

            if (videoTitle) {
              eventLabel = `${videoTitle} | ${videoId}`;
            } else {
              eventLabel = `${videoId}`;
            }

            let milestones = [0.25, 0.5, 0.75, 1];
            let milestonesReached = new Set();

            const pushToDataLayer = (eventAction) => {
              window.dataLayer = window.dataLayer || [];
              const existingEvent = window.dataLayer.find(
                (event) =>
                  event.event_label === eventLabel &&
                  event.event_action === eventAction,
              );
              if (!existingEvent) {
                window.dataLayer.push({
                  event_action: eventAction,
                  event_category: 'Video',
                  event_label: eventLabel,
                  event: 'vimeo',
                  videoId: videoId,
                });
                if (typeof window.vimeoGA === 'function') {
                  window.vimeoGA(
                    'send',
                    'event',
                    'Video',
                    eventAction,
                    eventLabel,
                  );
                }
              }
            };

            // Push the load event to dataLayer (triggered only once per video)
            pushToDataLayer('load');

            // Add event listeners for various player events
            player.on('play', () => {
              pushToDataLayer('play');
            });

            player.on('pause', () => {
              pushToDataLayer('pause');
            });

            player.on('ended', () => {
              pushToDataLayer('ended');
            });

            player.on('timeupdate', ({ percent }) => {
              milestones.forEach((milestone) => {
                if (percent >= milestone && !milestonesReached.has(milestone)) {
                  milestonesReached.add(milestone);
                  pushToDataLayer(`progress_${milestone * 100}`);
                }
              });
            });

            // Mark the player as initialized to avoid re-initialization
            liteVimeo.playerInitialized = true;
          };

          // Add event listener for pointerdown to initialize the player
          liteVimeo.addEventListener('pointerdown', () => {
            if (!liteVimeo.playerInitialized) {
              this.waitForIframe(liteVimeo, initializePlayer);
            }
          });
        }
      });
    },
  },
  // Initialize Vimeo players when the component is mounted
  mounted() {
    this.initializeVimeoPlayers();
  },
});
