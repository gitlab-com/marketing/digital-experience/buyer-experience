<script lang="ts">
import Vue from 'vue';
import { getPageMetadata } from '../../common/meta';
import SurveyIntro from '~/components/developer-survey/2024/survey-intro.vue';
import SurveySection from '~/components/developer-survey/2024/survey-section.vue';
import { MrktoForm } from '~/components/common/index.vue';
import { OneTrustTroubleWarning } from '~/components/common/index.vue';
import getCookieValue from '~/lib/getCookieValue';

export default Vue.extend({
  components: {
    SurveySection,
    SurveyIntro,
    MrktoForm,
    OneTrustTroubleWarning,
  },
  nuxtI18n: {
    locales: ['de-de', 'fr-fr', 'en-us', 'ja-jp'],
  },
  async asyncData({ $content, i18n }: any) {
    try {
      const basePath = `${i18n.locale}/developer-survey`;

      const survey = await $content(`${basePath}/index`).fetch();

      await Promise.all(
        survey.sections.map(async (section) => {
          await Promise.all(
            section.subsections.map(async (subsection) => {
              if (subsection.results_file) {
                const resultsFilePath = `shared/${basePath}/data/${subsection.results_file}`;
                const resultsData = await $content(resultsFilePath).fetch();

                subsection.data = resultsData.data;
              }
            }),
          );
        }),
      );

      return { survey, locale: i18n.locale };
    } catch (e) {
      throw new Error(e);
    }
  },

  data() {
    return {
      gated: true,
    };
  },
  head() {
    return getPageMetadata((this as any).survey, this.$route.path);
  },
  mounted() {
    const cookieValue = this.getCookieValue('2024-gitlab-devsecops-survey');
    if (cookieValue) {
      this.gated = false;
    }
  },
  methods: {
    getCookieValue,
    setCookie(name, value) {
      const date = new Date();
      date.setFullYear(date.getFullYear() + 2);
      const expires = `; expires=${date.toUTCString()}`;
      document.cookie = `${name}=${value}${expires}; path=/`;
    },
    onFormSubmit() {
      this.gated = false;
      this.setCookie('2024-gitlab-devsecops-survey', true);
      if (window.innerWidth <= 768) {
        // This is needed to help with the content shift on mobile once the form is filled out
        const firstH3 = document.querySelector('h3');
        if (firstH3) {
          firstH3.scrollIntoView({ behavior: 'smooth' });
        }
      }
    },
  },
});
</script>

<template>
  <div class="developer-survey">
    <SurveyIntro :hero="survey.hero" :intro="survey.intro" />
    <div v-if="gated" class="gate">
      <SurveySection
        tabindex="-1"
        :data="survey.sections[0]"
        style="margin-bottom: -100px"
      />
      <section class="form-container">
        <SlpContainer>
          <SlpRow>
            <SlpColumn :cols="6" class="text-wrapper">
              <SlpTypography variant="body3-bold" tag="p" class="slp-mb-8">
                {{ survey.form.tag }}
              </SlpTypography>
              <SlpTypography variant="heading2-bold" tag="h2" class="slp-mb-8">
                {{ survey.form.header }}
              </SlpTypography>
              <SlpTypography variant="body2" tag="div" class="text-block">
                <span v-html="$md.render(survey.form.text)" />
              </SlpTypography>
            </SlpColumn>
            <SlpColumn :cols="6" class="form-wrapper">
              <div
                :class="`form-wrapper__inner ${locale == 'en-us' ? 'english' : ''}`"
              >
                <MrktoForm :data="survey.form" @submit="onFormSubmit" />
                <OneTrustTroubleWarning />
              </div>
            </SlpColumn>
          </SlpRow>
        </SlpContainer>
      </section>
    </div>
    <div v-else>
      <SurveySection
        v-for="section in survey.sections"
        :key="section.title"
        :data="section"
        :inverse-layout="section.inverse"
      />
      <section v-if="survey.all_reports" class="previous-links">
        <SlpContainer>
          <SlpTypography variant="heading3-bold" tag="h2" class="slp-mb-32">
            {{ survey.all_reports.header }}
          </SlpTypography>
          <div class="links">
            <a
              v-for="link in survey.all_reports.reports"
              :key="link.label"
              :data-ga-name="`${link.label} report`"
              data-ga-location="body"
              :href="link.link"
              class="link-button"
            >
              <div class="link">
                <SlpIcon
                  name="external-link"
                  variant="marketing"
                  alt=""
                  size="md"
                  hex-color="#232150"
                />
                <SlpTypography class="link-text" variant="heading5-bold">{{
                  link.label
                }}</SlpTypography>
              </div>
            </a>
          </div>
        </SlpContainer>
      </section>
    </div>
  </div>
</template>

<style lang="scss" scoped>
$primary-survey-text-color: #232150;
$checkmark-svg: 'data:image/svg+xml,%3Csvg%20width%3D%2220%22%20height%3D%2220%22%20viewBox%3D%220%200%2020%2020%22%20fill%3D%22none%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%3E%3Crect%20width%3D%2220%22%20height%3D%2220%22%20rx%3D%2210%22%20fill%3D%22%236FDAC9%22/%3E%3Cpath%20d%3D%22M4.5%2010.325L7.95%2013.775L15.5%206.22498%22%20stroke%3D%22%232F2A6B%22%20stroke-width%3D%222.2%22%20stroke-miterlimit%3D%2210%22/%3E%3C/svg%3E';

.developer-survey {
  overflow: clip;
  background: #f2f1f5;

  :deep {
    .slp-container {
      position: relative;
    }
  }
}

.form-wrapper {
  z-index: 5;
  color: $primary-survey-text-color;

  &__inner {
    background: $color-surface-800;
    padding: 24px;
    border-radius: 8px;
    box-shadow: 0px 1px 4px 0px rgba(0, 0, 0, 0.1);
    h2 {
      font-family: 'GitLab Mono';
      font-size: 14px;
      line-height: 22px;
    }

    &:deep {
      details {
        text-align: center;
      }
      // The following styles overrides the MrktoForm component style sheet
      ul,
      ol {
        padding-left: 1.5em;
      }
      li {
        list-style-type: disc;
        margin-bottom: 8px;
      }
      .form_column .form_container {
        background: $color-surface-800;
        border: none;
      }

      .mktoForm .mktoRequiredField label.mktoLabel,
      .mktoForm_container .mktoFieldWrap label {
        color: $primary-survey-text-color !important;
      }

      #LblCountry {
        margin-top: 0 !important;
      }

      &.english {
        .mktoButtonWrap button {
          color: #171321 !important;
          font-size: 0 !important;
          position: relative;
        }

        .mktoButtonWrap button:before {
          content: 'Unlock the full report below';
          font-size: 18px;
          color: #fff !important;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 10%;
          right: 0%;
        }

        .mktoButtonWrap button:hover:before {
          color: #171321 !important;
        }
      }

      .mktoForm_container {
        @media (min-width: $breakpoint-lg) {
          display: grid;
          grid-template-columns: 1fr 1fr;
          column-gap: 30px;
          .mktoFormRow:nth-of-type(11) {
            grid-column: 1/3;
          }
          .mktoButtonRow {
            grid-column: 1/3;
          }
        }
      }

      .form_column.slp-px-md-32 {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }

      .form_column .form_container {
        padding: 0 !important;
      }
    }
  }
}

.gate {
  position: relative;

  &:deep {
    .section {
      position: relative;

      &:before {
        content: '';
        position: absolute;
        bottom: 68px;
        left: 0;
        right: 0;
        height: 300px;
        background: linear-gradient(
          to bottom,
          rgba(255, 255, 255, 0),
          #f8f8f8 100%
        );
        z-index: 10;

        @media (max-width: $breakpoint-md) {
          bottom: 60px;
        }
      }

      .subsection {
        display: none;
      }
    }
  }

  .text-wrapper {
    padding-right: 64px;

    @media (max-width: $breakpoint-md) {
      padding-right: 0;
    }
  }

  .form-container {
    color: $primary-survey-text-color !important;
    width: 100%;
    position: relative;
    padding: 96px 0;
    background: linear-gradient(179deg, #f8f8f8 0.88%, #d7fefe 99.07%);

    &:before {
      @media (min-width: $breakpoint-lg) {
        content: '';
        background: url('/nuxt-images/developer-survey/2024/2024-survey-hero-bg-pills.png');
        background-size: contain;
        background-repeat: no-repeat;
        position: absolute;
        bottom: 0;
        left: 20%;
        z-index: 3;
        width: 3000px;
        height: 600px;
      }
    }

    @media (max-width: $breakpoint-md) {
      padding: 64px 0 96px 0;
    }

    .text-block {
      &:deep {
        p {
          margin-bottom: 16px;
        }

        strong {
          font-weight: $font-weight-bold;
        }

        li {
          margin-bottom: 8px;
          padding-left: 30px;
          position: relative;
        }

        li:before {
          content: url($checkmark-svg);
          position: absolute;
          left: 0;
          top: 0;
          width: 20px;
          height: 20px;
          z-index: 5;
        }
      }
    }
  }
}

.previous-links {
  background: $color-surface-100;
  padding: 96px 0;

  .links {
    display: flex;
    gap: 24px;
    flex-wrap: wrap;

    .link-button {
      border-radius: 4px;
      border: 1px solid #232150;
      display: block;
      padding: 0;
      background: $color-surface-50;
      transition: all 0.2s ease-in-out;

      @media (max-width: $breakpoint-md) {
        flex: 1 0 150px;
      }

      .link {
        padding: $spacing-16 $spacing-32;
        display: flex;
        justify-content: center;
        color: #232150;
      }
      span {
        margin-left: $spacing-16;
        margin-top: 5px;
      }

      &:hover {
        background: #232150;

        .link {
          color: #fdf1dd;
        }
      }
    }
  }
}
</style>
