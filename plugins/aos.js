import AOS from 'aos';
import 'aos/dist/aos.css';

export default (_, inject) => {
  if (typeof window !== 'undefined') {
    try {
      const aos = AOS.init({ disable: 'phone', once: true });
      inject('AOS', aos);
    } catch (e) {
      console.error('AOS initialization failed:', e);
      applyFallbackStyles();
    }
  }
};

/**
 * Removes AOS styles if the library fails to initialize
 */
function applyFallbackStyles() {
  document.querySelectorAll('[data-aos]').forEach((element) => {
    element.style.opacity = '1';
    element.style.transform = 'none';
    element.style.transition = 'none';
  });
}
