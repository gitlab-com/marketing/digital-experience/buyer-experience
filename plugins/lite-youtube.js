//from https://github.com/paulirish/lite-youtube-embed/blob/master/src/lite-yt-embed.js
class LiteYTEmbed extends HTMLElement {
  connectedCallback() {
    this.videoId = this.getAttribute('videoid');
    let playBtnEl = this.querySelector('.lty-playbtn');

    this.playLabel =
      (playBtnEl && playBtnEl.textContent.trim()) ||
      this.getAttribute('playlabel') ||
      'Play';

    if (!this.style.backgroundImage) {
      this.style.backgroundImage = `url("https://i.ytimg.com/vi/${this.videoId}/hqdefault.jpg")`;
      this.upgradePosterImage();
    }

    if (!playBtnEl) {
      playBtnEl = document.createElement('button');
      playBtnEl.type = 'button';
      playBtnEl.classList.add('lty-playbtn');
      this.append(playBtnEl);
    }
    if (!playBtnEl.textContent) {
      const playBtnLabelEl = document.createElement('span');
      playBtnLabelEl.className = 'lyt-visually-hidden';
      playBtnLabelEl.textContent = this.playLabel;
      playBtnEl.append(playBtnLabelEl);
    }

    if (playBtnEl.nodeName === 'A') {
      playBtnEl.removeAttribute('href');
      playBtnEl.setAttribute('tabindex', '0');
      playBtnEl.setAttribute('role', 'button');
      playBtnEl.addEventListener('keydown', (e) => {
        if (e.key === 'Enter' || e.key === ' ') {
          e.preventDefault();
          this.activate();
        }
      });
    }

    this.addEventListener('pointerover', LiteYTEmbed.warmConnections, {
      once: true,
    });
    this.addEventListener('focusin', LiteYTEmbed.warmConnections, {
      once: true,
    });

    this.addEventListener('click', this.activate);

    // Replacing deprecated navigator.vendor usage with a feature detection or user-agent check
    this.needsYTApi = this.shouldUseYTApi();

    this.fetchYTPlayerApi().then(() => {
      this.createYTPlayer();
    });
  }

  shouldUseYTApi() {
    // Use feature detection or user-agent string carefully
    const userAgent = navigator.userAgent.toLowerCase();
    const isSafari = /^((?!chrome|android).)*safari/i.test(userAgent);
    const isIOS = /iphone|ipad|ipod/i.test(userAgent);

    return this.hasAttribute('js-api') || isSafari || isIOS;
  }

  async activate() {
    if (this.classList.contains('lyt-activated')) return;
    this.classList.add('lyt-activated');
    const iframeEl = this.createBasicIframe();
    this.append(iframeEl);
    iframeEl.focus();
  }

  static addPrefetch(kind, url, as) {
    const linkEl = document.createElement('link');
    linkEl.rel = kind;
    linkEl.href = url;
    if (as) {
      linkEl.as = as;
    }
    document.head.append(linkEl);
  }

  fetchYTPlayerApi() {
    this.ytApiPromise = new Promise((res, rej) => {
      const el = document.createElement('script');
      el.src = 'https://www.youtube.com/iframe_api';
      el.async = true;
      el.onload = () => {
        YT.ready(res);
      };
      el.onerror = rej;
      this.append(el);
    });
    return this.ytApiPromise;
  }

  createYTPlayer() {
    const params = this.getParams();
    this.player = new YT.Player(this.createBasicIframe(), {
      videoId: this.videoId,
      playerVars: {
        ...Object.fromEntries(params),
      },
      events: {
        onStateChange: this.onPlayerStateChange.bind(this),
      },
    });
  }

  onPlayerStateChange(event) {
    // Only push events for specific states, ignoring 'seeking' or other unwanted states
    const videoTitle = this.dataset.title || `Video ${this.videoId}`;
    const eventLabel = `${videoTitle} | ${this.videoId}`;

    switch (event.data) {
      case YT.PlayerState.PLAYING:
        this.pushToDataLayer('play', eventLabel, this.videoId);
        break;
      case YT.PlayerState.ENDED:
        this.pushToDataLayer('complete', eventLabel, this.videoId);
        break;
    }
  }

  getParams() {
    const params = new URLSearchParams(this.getAttribute('params') || []);
    params.append('autoplay', '1');
    params.append('playsinline', '1');
    return params;
  }

  createBasicIframe() {
    const iframeEl = document.createElement('iframe');
    iframeEl.width = 560;
    iframeEl.height = 315;
    iframeEl.title = this.playLabel;
    iframeEl.allow =
      'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture';
    iframeEl.allowFullscreen = true;
    iframeEl.src = `https://www.youtube-nocookie.com/embed/${encodeURIComponent(this.videoId)}?${this.getParams().toString()}`;
    return iframeEl;
  }

  upgradePosterImage() {
    setTimeout(() => {
      const webpUrl = `https://i.ytimg.com/vi_webp/${this.videoId}/sddefault.webp`;
      const img = new Image();
      img.fetchPriority = 'low';
      img.referrerpolicy = 'origin';
      img.src = webpUrl;
      img.onload = (e) => {
        const noAvailablePoster =
          e.target.naturalHeight == 90 && e.target.naturalWidth == 120;
        if (noAvailablePoster) return;

        this.style.backgroundImage = `url("${webpUrl}")`;
      };
    }, 100);
  }
}

customElements.define('lite-youtube', LiteYTEmbed);
