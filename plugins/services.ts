import { Context } from '@nuxt/types';
import { AiTransparencyCenterService } from '~/services/ai-transparency-center.service';
import { GitlabTechnicalDemoSeriesService } from '../services/gitlab-technical-demo-series.service';

import {
  GetStartedService,
  DevSecOpsService,
  DedicatedService,
  SolutionsService,
  FreeTrialService,
  SecurityService,
  InstallService,
  ContentfulService,
  IntegrationsService,
  EnvironmentalSocialGovernanceService,
  EventsService,
  GartnerMagicQuadrantService,
  WorldTourEventsService,
  PricingService,
  CustomerSuccessManagementService,
  TheSourceService,
  MarketoFormPageService,
  WhyUpgradeService,
} from '../services';

export default ($ctx: Context, inject: any) => {
  const devSecOpsService = new DevSecOpsService($ctx);
  const getStartedService = new GetStartedService($ctx);
  const dedicatedService = new DedicatedService($ctx);
  const freeTrialService = new FreeTrialService($ctx);
  const securityService = new SecurityService($ctx);
  const solutionsService = new SolutionsService($ctx);
  const installService = new InstallService($ctx);
  const contentfulService = new ContentfulService($ctx);
  const integrationsService = new IntegrationsService($ctx);
  const environmentalSocialGovernanceService =
    new EnvironmentalSocialGovernanceService($ctx);
  const eventsService = new EventsService($ctx);
  const gartnerMagicQuadrant = new GartnerMagicQuadrantService($ctx);
  const worldTourEventsService = new WorldTourEventsService($ctx);
  const gitlabTechnicalDemoSeriesService = new GitlabTechnicalDemoSeriesService(
    $ctx,
  );
  const customerSuccessManagementService = new CustomerSuccessManagementService(
    $ctx,
  );
  const aiTransparencyCenterService = new AiTransparencyCenterService($ctx);
  const pricingService = new PricingService($ctx);
  const theSourceService = new TheSourceService($ctx);
  const marketoFormPageService = new MarketoFormPageService($ctx);
  const whyUpgradeService = new WhyUpgradeService($ctx);

  inject('devSecOpsService', devSecOpsService);
  inject('getStartedService', getStartedService);
  inject('dedicatedService', dedicatedService);
  inject('freeTrialService', freeTrialService);
  inject('securityService', securityService);
  inject('solutionsService', solutionsService);
  inject('installService', installService);
  inject('contentfulService', contentfulService);
  inject('integrationsService', integrationsService);
  inject(
    'environmentalSocialGovernanceService',
    environmentalSocialGovernanceService,
  );
  inject('eventsService', eventsService);
  inject('gartnerMagicQuadrantService', gartnerMagicQuadrant);
  inject('worldTourEventsService', worldTourEventsService);
  inject('gitlabTechnicalDemoSeriesService', gitlabTechnicalDemoSeriesService);
  inject('aiTransparencyCenterService', aiTransparencyCenterService);
  inject('pricingService', pricingService);
  inject('customerSuccessManagementService', customerSuccessManagementService);
  inject('theSourceService', theSourceService);
  inject('marketoFormPageService', marketoFormPageService);
  inject('whyUpgradeService', whyUpgradeService);
};
