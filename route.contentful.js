// File used to generate routes for content files living in Contentful
// This is done to generate those routes during yarn generate
import { createClient } from 'contentful';
import { CONTENT_TYPES } from './common/content-types';
import { getClient, getNavigationClient } from './plugins/contentful';
import { LOCALES } from './common/constants';
import {
  convertToCaseSensitiveLocale,
  getAvailableLanguagesFromTags,
} from './common/util';
import {
  FOOTER_CTF_ID,
  NAVIGATION_CTF_ID,
  NAVIGATION_VARIANT_CTF_ID,
} from './common/constants';
import {
  validateFooterResponse,
  validateNavigationResponse,
} from './common/validation';
const isContentPreview = process.env.USE_CONTENTFUL_PREVIEW_API === 'true';
const host = isContentPreview ? 'preview.contentful.com' : 'cdn.contentful.com';

const client = createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: isContentPreview
    ? process.env.CTF_PREVIEW_ACCESS_TOKEN
    : process.env.CTF_CDA_ACCESS_TOKEN,
  host,
  retryLimit: 1000,
  logHandler: (level, data) => {
    if (data.includes('Rate limit')) {
      return;
    }
  },
});

/**
 * Return the navigationData needed for the Nav and the footer separated by each locale the application supports.
 * Note: When an A/B test is added to the navigation, that ID and variable should be added here. See https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/3648/diffs#a4e6fb36c426a4084b8de63168786d0c9749631b_19_30 for an example.
 * @returns {Promise<T>}
 */
export async function getAllLocalesNavData() {
  try {
    const ctfConfig = {
      'sys.id[in]': `${NAVIGATION_CTF_ID},${FOOTER_CTF_ID},${NAVIGATION_VARIANT_CTF_ID}`,
      order: 'sys.id',
      include: 10,
    };

    const localePromises = Object.values(LOCALES).map((locale) =>
      getNavigationClient().getEntries({
        ...ctfConfig,
        locale: convertToCaseSensitiveLocale(locale),
      }),
    );

    const responses = await Promise.all(localePromises);

    const foundIds = responses.flatMap((response) =>
      response.items.map((item) => item.sys.id),
    );

    // Verify that the Contentful response contains entries with these IDs
    const missingIds = Object.values(ctfConfig['sys.id[in]'].split(',')).filter(
      (id) => !foundIds.includes(id),
    );
    if (missingIds.length > 0) {
      throw new Error(`Entries with IDs ${missingIds.join(',')} not found`);
    }

    const data = responses.reduce((acumObj, { items }) => {
      const [navigation, navigationVariant, footer] = items;
      const locale = navigation.sys.locale;
      acumObj[locale.toLowerCase()] = {
        navigation,
        navigationVariant,
        footer,
      };
      return acumObj;
    }, {});

    // Validate navigation and footer data
    const validatedNavigation = validateNavigationResponse(
      data['en-us']['navigation'],
    );

    const validatedFooter = validateFooterResponse(data['en-us']['footer']);

    const validatedNavigationVariant = validateNavigationResponse(
      data['en-us']['navigationVariant'],
    );

    if (validatedNavigation && validatedFooter && validatedNavigationVariant) {
      return data;
    } else {
      // eslint-disable-next-line
      console.error('Main navigation or footer response was not valid.');
      throw new Error();
    }
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching navigation data:', error);
    throw new Error();
  }
}

export async function fetchPages() {
  try {
    let totalPages = 0;
    let pages = [];

    do {
      const { total, items } = await getClient().withAllLocales.getEntries({
        content_type: CONTENT_TYPES.PAGE,
        select:
          'fields.slug, fields.description, fields.pageContent, metadata.tags',
        limit: 1000,
      });
      totalPages = total;

      const filteredResponse = items.filter(
        (page) =>
          (page.fields.pageContent && page.fields.pageContent['en-US']) ||
          page.metadata.tags.length > 1,
      );

      const baseSlugs = filteredResponse.map((page) => ({
        route: `/${page.fields.slug['en-US']}`,
      }));

      const taggedPages = filteredResponse.filter(
        (page) => page.metadata?.tags.length,
      );

      const untaggedPages = filteredResponse.filter(
        (page) => !page.metadata?.tags.length,
      );

      const localizedSlugs = taggedPages.flatMap(({ fields, metadata }) => {
        const availableLanguages = getAvailableLanguagesFromTags(metadata.tags);
        return availableLanguages.map((locale) => ({
          route: `${locale !== LOCALES.DEFAULT ? `/${locale}` : ''}/${fields.slug['en-US']}`,
        }));
      });

      untaggedPages.forEach((page) => {
        if (page.fields?.description) {
          const locales = Object.keys(page.fields.description);
          locales.forEach((locale) => {
            if (locale !== 'en-US' && page.fields.slug['en-US'] !== '/') {
              localizedSlugs.push({
                route: `/${locale.toLowerCase()}/${page.fields.slug['en-US']}`,
              });
            }
          });
        }
      });
      pages = [...pages, ...baseSlugs, ...localizedSlugs];
    } while (pages.length < totalPages);
    return pages.map((page) => page.route.replace('//', '/'));
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching page routes from Contentful:', error);
    throw error;
  }
}

export async function fetchCustomerRoutes() {
  try {
    const { items } = await client.getEntries({
      content_type: CONTENT_TYPES.CASE_STUDY,
      select: ['fields.slug', 'metadata.tags'],
      limit: 1000,
    });

    return items.flatMap(({ fields, metadata }) => {
      const availableLanguages = getAvailableLanguagesFromTags(metadata.tags);
      return availableLanguages.map(
        (locale) =>
          `${locale !== LOCALES.DEFAULT ? `/${locale}` : ''}/customers/${fields.slug}`,
      );
    });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching customer routes from Contentful:', error);
    throw error;
  }
}

function getTopicsPages() {
  return getClient().withAllLocales.getEntries({
    content_type: CONTENT_TYPES.TOPICS,
    select: 'fields.slug, fields.topicName, metadata.tags',
    limit: 1000,
  });
}

export async function fetchTopicsRoutes() {
  try {
    const { items } = await getTopicsPages();

    return items
      .filter((page) => page.fields.topicName && page.fields.topicName['en-US'])
      .flatMap((page) => {
        const availableLanguages = getAvailableLanguagesFromTags(
          page.metadata.tags,
        );

        return availableLanguages.map((locale) => {
          if (locale !== LOCALES.DEFAULT) {
            return `/${locale}${page.fields.slug['en-US']}`;
          }
          return `${page.fields.slug['en-US']}`; // TODO: Remove trailing and leading slashes in Contentful entries
        });
      });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching topics routes from Contentful:', error);
    throw error;
  }
}

async function getArticlesPages() {
  const { items } = await getClient().getEntries({
    content_type: CONTENT_TYPES.THE_SOURCE,
    select: 'fields.slug, metadata.tags',
    limit: 1000,
  });

  return items;
}

async function getTheSourceAuthors() {
  const { items } = await getClient().getEntries({
    content_type: CONTENT_TYPES.THE_SOURCE_AUTHOR,
    select: 'fields.slug, metadata.tags',
    limit: 1000,
  });

  return items;
}

async function getTheSourceCategories() {
  const { items } = await getClient().getEntries({
    content_type: CONTENT_TYPES.THE_SOURCE_CATEGORY,
    select: 'fields.slug, metadata.tags',
    limit: 1000,
  });

  return items;
}

export async function fetchTheSourceArticlesRoutes() {
  try {
    const [articles, authors, categories] = await Promise.all([
      getArticlesPages(),
      getTheSourceAuthors(),
      getTheSourceCategories(),
    ]);

    const entries = [...articles, ...authors, ...categories];

    return entries.flatMap(({ fields, metadata }) => {
      const availableLanguages = getAvailableLanguagesFromTags(metadata.tags);
      return availableLanguages.map(
        (locale) =>
          `${locale !== LOCALES.DEFAULT ? `/${locale}` : ''}/${fields.slug}`,
      );
    });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching "The Source" routes from Contentful:', error);
    throw error;
  }
}

function getCustomPages(limit, skip) {
  return getClient().getEntries({
    content_type: CONTENT_TYPES.CUSTOM_PAGE,
    select: 'fields.slug',
    limit,
    skip,
  });
}

export async function fetchCustomRoutes() {
  try {
    let skip = 0;
    const limit = 100;
    const { items, total } = await getCustomPages(limit, skip);
    let allEntries = [...items];

    if (total > 0) {
      while (skip < total) {
        skip += limit;
        const { items: batchEntries } = await getCustomPages(limit, skip);
        allEntries = [...allEntries, ...batchEntries];
      }

      // eslint-disable-next-line
      console.log(
        `Custom page routes fetching completed: ${allEntries.length} routes have been generated`,
      );

      return allEntries
        .filter((entry) => entry.fields)
        .map((entry) => `${entry.fields.slug}`);
    }
    return [];
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching custom pages routes from Contentful:', error);
    throw error;
  }
}

export async function fetchEventRoutes() {
  try {
    const eventPages = await client.getEntries({
      content_type: CONTENT_TYPES.EVENT,
      select: ['fields.slug', 'fields.eventType', 'metadata.tags'], // include metadata.tags to get localization info
      limit: 1000,
    });
    return eventPages.items
      .filter((event) => event.fields && event.fields.slug)
      .flatMap(({ fields, metadata }) => {
        const { eventType } = fields;

        let route = `/events/${fields.slug}`;

        if (eventType === 'Connect') {
          route = `/events/connect/${fields.slug}`;
        }

        // Only World Tour is i18n enabled righ tnow
        if (eventType === 'World Tour') {
          // Grab available languages for World Tour event
          const availableLanguages = getAvailableLanguagesFromTags(
            metadata.tags,
          );

          // Generate routes for each available language
          return availableLanguages.map(
            (locale) =>
              `${locale !== LOCALES.DEFAULT ? `/${locale}` : ''}/events/devsecops-world-tour/${fields.slug}`,
          );
        }

        // Default case for other events
        return route;
      });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching event routes from Contentful:', error);
    throw error;
  }
}

export async function fetchMicrositeRoutes() {
  try {
    const microsites = await client.getEntries({
      content_type: CONTENT_TYPES.MICROSITE,
      select: 'fields.slug',
    });

    return microsites.items.map((site) => `/partner/${site.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching microsite routes from Contentful:', error);
    throw error;
  }
}

export default [
  '/analysts',
  '/customers/',
  '/company/',
  '/company/all-remote/',
  '/company/contact/',
  '/company/preference-center/',
  '/company/visiting/',
  '/community/early-access/',
  '/dedicated/',
  '/environmental-social-governance/',
  '/free-trial/',
  '/free-trial/devsecops/',
  '/gartner-magic-quadrant/',
  '/get-started/continuous-integration/',
  '/get-started/build-business-case/',
  '/get-started/enterprise/',
  '/get-started/small-business/',
  '/install/ce-or-ee/',
  '/security/',
  '/seventeen/',
  '/solutions/',
  '/solutions/slack/',
  '/solutions/kubernetes/',
  '/solutions/github/',
  '/solutions/jenkins/',
  '/solutions/jira/',
  '/solutions/faster-software-delivery/',
  '/solutions/analytics-and-insights/',
  '/solutions/cloud-native/',
  '/solutions/code-suggestions/',
  '/solutions/education/',
  '/solutions/education/edu-survey/',
  '/solutions/education/join/',
  '/solutions/gitlab-duo-pro/sales/',
  '/solutions/devops-platform/',
  '/solutions/digital-transformation/',
  '/solutions/visibility-measurement/',
  '/topics/',
  '/support',
  '/support/us-government-support',
  '/support/statement-of-support',
  '/support/providing-large-files',
  '/support/general-policies',
  '/support/customer-satisfaction',
  '/support/definitions',
  '/support/scheduling-upgrade-assistance',
  '/support/enhanced-support-offerings',
  '/support/sensitive-information',
  '/support/managing-support-contacts',
  '/support/portal',
  '/support/gitlab-com-policies',
  '/jobs/all-jobs',
];
