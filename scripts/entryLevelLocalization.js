/* eslint @typescript-eslint/no-var-requires: 0 */
/* eslint no-console: 0 */

const contentful = require('contentful-management');
const process = require('process');
const accessToken = process.argv[2]; // CMA access token
const spaceId = process.argv[3];
const id = process.argv[4];
const page = process.argv[5];
const verifiedContentTypes = [];

async function createLocalizedEntries(auth, page = 1, id) {
  const client = contentful.createClient({
    accessToken: auth.accessToken,
  });
  if (!id) {
    return;
  }

  // This script makes two big assumptions:
  // 1. The english entry is always the source entry
  // 2. The other languages have the exact same components and in the exact same order
  try {
    const space = await client.getSpace(auth.spaceId);
    const environment = await space.getEnvironment(auth.env);
    console.log('id:', id);
    // If the parent entry ID is present, do it only for that entry, if not fetch all entries from that content type.
    const { items } = await environment.getEntries({
      limit: 500,
      skip: (page - 1) * 500,
      content_type: 'page',
      'sys.id': id,
      include: 10,
    });
    for (const entry of items) {
      if (entry.isPublished() || isChanged(entry)) {
        // For each page, get it's content references
        const englishContent = entry.fields.pageContent['en-US'];
        const frenchContent = entry.fields.pageContent['fr-FR'];
        const japaneseContent = entry.fields.pageContent['ja-JP'];
        const germanContent = entry.fields.pageContent['de-DE'];
        const localizedContent = [
          { code: 'fr-FR', content: frenchContent },
          { code: 'ja-JP', content: japaneseContent },
          { code: 'de-DE', content: germanContent },
        ];

        // for every localized array that has less entries than the english one, add them
        for (const locale of localizedContent) {
          if (locale.content && locale.content.length > 0) {
            if (locale.content.length !== englishContent.length) {
              // Abort, manual processing is needed
              console.log(
                `skipping ${locale.code} as it has different components`,
              );
            } else {
              // Link entries
              // go index by index adding the source entry reference to the english one.
              //  In this process, if the content model does not support the source entry field, add it

              await linkEntries(
                locale.code,
                locale.content,
                environment,
                englishContent,
              );
            }
          }
        }

        // Do the same for SEO (?)
      }
    }
  } catch (error) {
    console.error(error);
  }

  console.log('Update finished');
  process.exit(0);
}

async function linkEntries(localeCode, content, environment, sourceContent) {
  for (const [index, component] of content.entries()) {
    // First check if the content type supports the field, if not, add it.
    const fullComponent = await environment.getEntry(component.sys.id);
    const fullSourceComponent = await environment.getEntry(
      sourceContent[index].sys.id,
    );
    const contentTypeId = fullComponent.sys.contentType.sys.id;
    await addFieldToContentType(environment, contentTypeId);

    // Now I add the source entry reference to the field, based on the element in the same position of the english array
    fullComponent.fields.sourceEntry = {
      'en-US': {
        sys: {
          type: 'Link',
          linkType: 'Entry',
          id: sourceContent[index].sys.id,
        },
      },
    };
    fullComponent.metadata.tags.push({
      sys: {
        type: 'Link',
        linkType: 'Tag',
        id: `language_${localeCode}`,
      },
    });
    const tags = fullComponent.metadata.tags.map((tag) => tag.sys.id);
    const hasAllTags = [
      'language_de-DE',
      'language_fr-FR',
      'language_ja-JP',
    ].every((tag) => tags.includes(tag));
    if (hasAllTags) {
      console.log('incorrect tagging detected clearing tags', tags);
      fullComponent.metadata.tags = fullComponent.metadata.tags.filter(
        (tag) =>
          !['language_de-DE', 'language_fr-FR', 'language_ja-JP'].includes(
            tag.sys.id,
          ),
      );
      fullComponent.fields.sourceEntry = {
        'en-US': null,
      };
    }
    try {
      await fullComponent.update();
      // await updatedEntry.publish();
      console.log(`Entry updated`);
      await delay(150);
    } catch (error) {
      console.error(
        `Error Updating Entry 
                    ${error}
                              `,
      );
    }

    // Repeat this process for all nested entries
    // Look for link type fields, which would be references to another entry
    const entryFields = filterByReferenceField(fullComponent.fields);
    const sourceEntryFields = filterByReferenceField(
      fullSourceComponent.fields,
    );
    if (entryFields.length && entryFields.length === sourceEntryFields.length) {
      console.log('Child reference entries found, recurring');
      await linkEntries(
        localeCode,
        entryFields,
        environment,
        sourceEntryFields,
      );
    } else if (
      entryFields.length &&
      entryFields.length !== sourceEntryFields.length
    ) {
      console.log('Different ammount of link entries detected, skipping');
      console.log(entryFields);
      console.log(sourceEntryFields);
    } else if (entryFields.length === 0) {
      console.log('no further nesting in this component');
    }
  }
}

const filterByReferenceField = (content) => {
  return Object.keys(content)
    .filter(
      (key) =>
        key !== 'sourceEntry' &&
        ((content[key]['en-US'].sys &&
          content[key]['en-US'].sys.type === 'Link' &&
          content[key]['en-US'].sys.linkType === 'Entry') ||
          (Array.isArray(content[key]['en-US']) &&
            content[key]['en-US'].every(
              (item) => item.sys && item.sys.type === 'Link',
            ))),
    )
    .map((key) => content[key]['en-US'])
    .flat(1);
};

async function addFieldToContentType(environment, contentTypeId) {
  if (verifiedContentTypes.includes(contentTypeId)) {
    console.log('content type already processed, skipping');
    return;
  }
  try {
    const contentType = await environment.getContentType(contentTypeId);

    // Check if the field already exists
    const fieldExists = contentType.fields.some(
      (field) => field.id === 'sourceEntry',
    );
    if (fieldExists) {
      console.log('Field already exists');
      verifiedContentTypes.push(contentTypeId);
      return;
    }
    console.log(`field not found for ${contentTypeId}, creating..`);
    // Add new field
    contentType.fields.push({
      id: 'sourceEntry',
      name: 'Source Entry',
      type: 'Link',
      required: false,
      linkType: 'Entry',
    });

    // Update the content type
    const updatedContentType = await contentType.update();
    console.log('Content type updated:', updatedContentType);

    // Publish the content type
    const publishedContentType = await updatedContentType.publish();
    console.log('Content type published:', publishedContentType);
    verifiedContentTypes.push(contentTypeId);
  } catch (error) {
    console.error('Error updating content type:', error);
  }
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function isChanged(entity) {
  return (
    !!entity.sys.publishedVersion &&
    entity.sys.version >= entity.sys.publishedVersion + 2
  );
}

(async () => {
  await createLocalizedEntries(
    {
      accessToken,
      spaceId,
      env: 'master',
    },
    page,
    id,
  );
})();
