const currentDate = new Date();
const firstDayOfYear = new Date(currentDate.getFullYear(), 0, 1);
const days = Math.floor((currentDate - firstDayOfYear) / (24 * 60 * 60 * 1000));

const weekNumber = Math.ceil(days / 7);

// eslint-disable-next-line no-console
console.log(`The current week number is: ${weekNumber}`);

// If it's an odd week (not sprint release week)
if (weekNumber % 2 !== 0) {
  // eslint-disable-next-line no-console
  console.log('This is an odd week, terminating process');
  process.exit(1);
}
