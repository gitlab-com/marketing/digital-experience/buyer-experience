import { Context } from '@nuxt/types';
import { COMPONENT_NAMES } from '~/common/constants';
import { CONTENT_TYPES } from '~/common/content-types';
import { CtfCard, CtfEntry, CtfFaq, CtfHeaderAndText, CtfPage } from '~/models';
import { getClient } from '~/plugins/contentful';
import { metaDataHelper } from '~/services/events/events.helpers';
import { mapBySolutionValueProp } from '~/services/solutions/solutions-by-solution.helper';
import { mapSolutionsHero } from '~/services/solutions/solutions-default.helper';
import { convertToCaseSensitiveLocale } from '~/common/util';

export class AiTransparencyCenterService {
  private readonly $ctx: Context;

  readonly componentNames = {
    ATC_COPY: 'atc-copy',
    FAQ: 'faq',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  private async getContentfulData(slug: string): Promise<CtfEntry<CtfPage>> {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      include: 10,
      'fields.slug': slug,
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });

    if (items.length === 0) {
      throw new Error('Ai Transparency Center page not found in Contentful');
    }

    const [contentfulPage] = items;

    return {
      ...contentfulPage,
      spaceId: items[0].sys.space.sys.id,
      entryId: items[0].sys.id,
    } as CtfEntry<CtfPage>;
  }

  async getContent() {
    const { fields, spaceId, entryId } = await this.getContentfulData(
      'ai-transparency-center',
    );
    return {
      ...this.transformContentfulData(fields),
      spaceId,
      entryId,
    };
  }

  transformContentfulData(page: CtfPage) {
    const { pageContent, seoMetadata } = page;

    const components = this.getPageComponents(pageContent);
    const metadata = metaDataHelper(seoMetadata[0]);

    return {
      metadata,
      components,
    };
  }

  private getPageComponents(pageContent: CtfEntry<any>[]) {
    return pageContent
      .map((ctfComponent) => this.mapCtfComponent(ctfComponent))
      .filter((component) => component);
  }

  private mapCtfComponent({ fields, sys }: CtfEntry<any>) {
    let component;

    const { id } = sys.contentType.sys;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = mapSolutionsHero(fields);
        break;
      }
      case CONTENT_TYPES.CARD: {
        component = this.mapAtcCopy(fields);
        break;
      }
      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(fields);
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        component = mapBySolutionValueProp(fields);
        break;
      }
      case CONTENT_TYPES.FAQ: {
        component = this.mapFAQ(fields);
        break;
      }
      case CONTENT_TYPES.NEXT_STEPS: {
        component = this.mapNextStep();
        break;
      }
      default:
        break;
    }

    return component;
  }

  private mapSideNavigation(ctfSideMenu: any) {
    const links = ctfSideMenu.anchors
      .map((anchor: any) => anchor.fields)
      .map((anchor: any) => ({
        title: anchor.linkText,
        href: anchor.anchorLink,
        data_ga_name: anchor.dataGaName,
        data_ga_location: anchor.dataGaLocation,
      }));

    return {
      ...ctfSideMenu.customFields,
      name: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
      links,
      slot_content: ctfSideMenu.content
        .map((item) => this.mapCtfComponent(item))
        .filter((item) => item),
    };
  }

  private mapAtcCopy(card: CtfCard) {
    return {
      name: this.componentNames.ATC_COPY,
      data: {
        title: card.title,
        subtitle: card.subtitle,
        description: card.description,
        link: card.button && {
          text: card.button.fields.text,
          href: card.button.fields.externalUrl,
          ga_name: card.button.fields.dataGaName,
          ga_location: card.button.fields.dataGaLocation,
        },
        ...card.customFields,
      },
    };
  }

  private mapFAQ(faq: CtfFaq) {
    const content = {
      name: this.componentNames.FAQ,
      data: {
        header: faq.title,
        groups: [
          {
            header: faq.title,
            questions: faq.singleAccordionGroupItems.map(
              (item: CtfEntry<CtfHeaderAndText>) => ({
                answer: item.fields.text,
                question: item.fields.header,
                id: item.fields.headerAnchorId,
              }),
            ),
          },
        ],
      },
    };

    return content;
  }

  private mapNextStep() {
    return {
      name: COMPONENT_NAMES.NEXT_STEP,
    };
  }
}
