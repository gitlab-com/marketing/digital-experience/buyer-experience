import { Context } from '@nuxt/types';
import { getUrlFromContentfulImage } from '../common/util';
import { convertToCaseSensitiveLocale } from '~/common/util';
import { DevSecOpsDTO } from '../models';
import { CONTENT_TYPES } from '../common/content-types';
import {
  CtfCard,
  CtfCardGroup,
  CtfHero,
  CtfTwoColumnBlock,
} from '~/models/content-types.dto';
import { getClient } from '~/plugins/contentful.js';

export class DevSecOpsService {
  private readonly $ctx: Context;

  private readonly CONTENT_IDS = {
    hero: CONTENT_TYPES.HERO,
    banner: CONTENT_TYPES.CARD,
    badges: CONTENT_TYPES.TWO_COLUMN_BLOCK,
    pricing: CONTENT_TYPES.CARD_GROUP,
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * This is the main method.
   * It returns data from contentful.
   * @param slug
   */
  getContent(slug: string): Promise<DevSecOpsDTO> {
    try {
      return this.getContentfulData(slug);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(slug: string) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    const props = {
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 3,
    };

    if (!isDefaultLocale) {
      props['locale'] = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    }

    const devSecOpsEntry = await getClient().getEntries(props);

    const [devSecOps] = devSecOpsEntry.items;

    return this.transformContentfulData(devSecOps);
  }

  /**
   * Main page mapper that converts contentful object structure to devSecOpsDTO object structure
   * @param ctfData
   * @private
   */
  private transformContentfulData(ctfData: any): DevSecOpsDTO {
    const { pageContent, seoMetadata } = ctfData.fields;

    const { ctfHero, ctfBanner, ctfCategoriesBlock, ctfBadges, ctfPricing } =
      this.getCtfComponents(pageContent);

    const transformedData: DevSecOpsDTO = {
      title: seoMetadata[0]?.fields?.ogTitle,
      description: seoMetadata[0]?.fields?.ogDescription,
      hero: this.mapCtfHero(ctfHero),
      categories_block: ctfCategoriesBlock.data,
      badges: this.mapCtfBadges(ctfBadges),
      pricing: this.mapCtfPricing(ctfPricing),
    };

    if (ctfBanner) transformedData.banner = this.mapCtfBanner(ctfBanner);

    return transformedData;
  }

  /**
   * This method obtains every component from the `pageContent` array from the "PAGE" content type.
   * This method prevents the page from breaking if anyone changes the order of the components in Contentful
   * @param pageContent
   * @private
   */
  private getCtfComponents(pageContent: any) {
    const result: {
      ctfHero?: CtfHero;
      ctfBanner?: CtfCard;
      ctfCategoriesBlock?: any;
      ctfBadges?: CtfTwoColumnBlock;
      ctfPricing?: CtfCardGroup;
    } = {};

    for (const pageComponent of pageContent) {
      switch (pageComponent.sys.contentType.sys.id) {
        case this.CONTENT_IDS.hero:
          result.ctfHero = pageComponent.fields;
          break;
        case this.CONTENT_IDS.banner:
          result.ctfBanner = pageComponent.fields;
          break;
        case this.CONTENT_IDS.badges:
          result.ctfBadges = pageComponent.fields;
          break;
        case this.CONTENT_IDS.pricing:
          result.ctfPricing = pageComponent.fields;
          break;
        default:
          result.ctfCategoriesBlock = pageComponent.fields;
      }
    }
    return result;
  }

  private mapCtfHero(ctfHero: CtfHero) {
    return {
      header: ctfHero?.title,
      description: ctfHero?.description,
      primaryButton: {
        text: ctfHero?.primaryCta?.fields?.text,
        href: ctfHero?.primaryCta?.fields?.externalUrl,
      },
      video: {
        title: ctfHero?.video?.fields?.title,
        url: ctfHero?.video?.fields?.url,
        image:
          ctfHero?.video?.fields?.thumbnail?.fields?.image?.fields?.file?.url,
      },
    };
  }

  private mapCtfBanner(ctfBanner: CtfCard) {
    return {
      badge_text: ctfBanner?.pills[0],
      headline: ctfBanner?.title,
      blurb: ctfBanner?.description,
      button: {
        text: ctfBanner?.button?.fields.text,
        url: ctfBanner?.button?.fields.externalUrl,
        data_ga_name: ctfBanner?.button?.fields.dataGaName,
        data_ga_location: ctfBanner?.button?.fields.dataGaLocation,
      },
      features: {
        list: ctfBanner?.column2List?.map((item: string) => ({
          text: item,
        })),
      },
    };
  }

  private mapCtfBadges(ctfBadges: CtfTwoColumnBlock) {
    return {
      header: ctfBadges.header,
      description: ctfBadges.text,
      badges: ctfBadges.assets?.map((badge) => ({
        src: getUrlFromContentfulImage(badge.fields?.image),
        alt: badge.fields?.altText,
      })),
    };
  }

  private mapCtfPricing(ctfPricing: CtfCardGroup) {
    return {
      header: ctfPricing.header,
      footnote: ctfPricing.description,
      ctaText: ctfPricing.cta?.fields?.text,
      tiers: ctfPricing.card?.map((tier) => ({
        name: tier.fields?.title,
        benefits: tier.fields?.list,
        ctaPrimary: tier.fields?.button?.fields?.text,
        ctaSecondary: tier.fields?.secondaryButton?.fields?.text,
        purchaseUrl: tier.fields?.button?.fields?.externalUrl,
        learnMoreUrl: tier.fields?.secondaryButton?.fields?.externalUrl,
      })),
    };
  }
}
