import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { getUrlFromContentfulImage } from '~/common/util';
import { COMPONENT_NAMES } from '~/common/constants';
import { mapSolutionsHero } from './solutions/solutions-default.helper';
export class GitlabTechnicalDemoSeriesService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the /gitlab-technical-demo-series-XXX pages
   * @param slug
   */
  async getContent(slug: string) {
    try {
      return this.getContentfulData(slug);
    } catch (e) {
      throw new Error(e);
    }
  }

  async getPage(_slug: string) {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': _slug,
      include: 5,
    });

    if (!items.length) {
      throw new Error('Page not found');
    }

    const [page] = items;

    return { page: this.transformContentfulData(page) };
  }

  transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const mappedContent = this.mapPageContent(pageContent);
    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    return {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      ...mappedContent,
    };
  }

  private mapPageContent(pageContent: any[]) {
    const resourcesComponent = pageContent.find(
      (item) => item.fields.componentName === 'resources',
    );

    const ctfComponents = pageContent.filter(
      (item) => item.fields.componentName !== 'resources',
    );

    const components = ctfComponents
      .map((component) => this.getComponentFromEntry(component))
      .filter((component) => component);

    const resources: any = resourcesComponent
      ? this.mapCardGroupEntry(resourcesComponent.fields, 'resources')
      : {};

    return { components, resources: resources.data };
  }

  /**
   * Main method that switches which components should be used depending on the Content Type and Component Name
   * @param ctfEntry
   */
  private getComponentFromEntry(ctfEntry: any) {
    let component;

    switch (ctfEntry.sys.contentType.sys.id) {
      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapHeaderText(ctfEntry.fields);
        break;
      }

      case CONTENT_TYPES.HERO: {
        component = mapSolutionsHero(ctfEntry.fields);
        break;
      }

      case CONTENT_TYPES.CARD: {
        component = this.mapCardComponent(ctfEntry.fields);
        break;
      }

      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroupComponent(ctfEntry.fields);
        break;
      }

      default:
        component = null;
        break;
    }

    return component;
  }

  private mapHeaderText(ctfHeaderAndText: any) {
    const { header, text } = ctfHeaderAndText;

    return {
      name: 'ResourcesHero',
      data: {
        title: header,
        subtitle: text,
      },
    };
  }

  private mapCardComponent(ctfCard: any) {
    let component;
    switch (ctfCard.componentName) {
      case COMPONENT_NAMES.BY_SOLUTION_LINK: {
        component = this.mapBySolutionLink(ctfCard);
        break;
      }

      default:
        component = null;
        break;
    }

    return component;
  }

  private mapCardGroupComponent(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.componentName) {
      case COMPONENT_NAMES.UPCOMING_DEMO_SERIES: {
        component = this.mapCardGroupEntry(
          ctfCardGroup,
          ctfCardGroup.componentName,
        );
        break;
      }
      case COMPONENT_NAMES.UPCOMING_DEMO_SERIES_APAC: {
        component = this.mapCardGroupEntry(
          ctfCardGroup,
          ctfCardGroup.componentName,
        );
        break;
      }
      case COMPONENT_NAMES.ON_DEMAND_DEMOS: {
        component = this.mapCardGroupEntry(
          ctfCardGroup,
          ctfCardGroup.componentName,
        );
        break;
      }

      case 'resources': {
        component = this.mapCardGroupEntry(
          ctfCardGroup,
          ctfCardGroup.componentName,
        );
        break;
      }

      default:
        component = null;
        break;
    }
    return component;
  }

  mapCardGroupEntry(ctfCardGroup: any, componentName: string) {
    return {
      name: componentName,
      data: { ...ctfCardGroup, ...ctfCardGroup?.customFields },
    };
  }

  mapBySolutionLink(ctfCardGroup: any) {
    return {
      name: COMPONENT_NAMES.BY_SOLUTION_LINK,
      data: {
        alt: '',
        description: ctfCardGroup?.description,
        icon: ctfCardGroup?.iconName,
        image: ctfCardGroup?.image?.fields?.file?.url,
        link: ctfCardGroup?.button?.fields?.externalUrl,
        title: ctfCardGroup?.title,
        ...ctfCardGroup?.customFields,
      },
    };
  }
}
