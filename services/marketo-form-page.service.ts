import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful.js';
import {
  CtfCard,
  CtfEntry,
  CtfHeaderAndText,
  CtfHero,
  CtfForm,
  CtfCustomerLogo,
} from '~/models';
export class MarketoFormPageService {
  private readonly $ctx: Context;
  private readonly CONTENT_IDS = {
    hero: CONTENT_TYPES.HERO,
    form: CONTENT_TYPES.FORM,
    headerAndText: CONTENT_TYPES.HEADER_AND_TEXT,
    card: CONTENT_TYPES.CARD,
    logos: CONTENT_TYPES.CUSTOMER_LOGOS,
  };
  constructor($context: Context) {
    this.$ctx = $context;
  }
  async getContent(slug: string) {
    try {
      return this.getContentfulData(slug);
    } catch (e) {
      throw new Error(e);
    }
  }
  private async getContentfulData(_slug: string) {
    try {
      const props = {
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      };

      const content = await getClient().getEntries(props);

      if (content.items.length === 0) {
        throw new Error('Not found');
      }
      const [items] = content.items;
      return this.transformContentfulData(items);
    } catch (e) {
      throw new Error(e);
    }
  }
  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const { ctfHero, ctfCards, ctfForm, ctfHeaderAndText, ctfCustomerLogo } =
      this.getCtfComponents(pageContent);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      hero: this.mapHero(ctfHero),
      cards: ctfCards,
      form: ctfForm,
      headerAndText: ctfHeaderAndText,
      logos: ctfCustomerLogo,
    };

    return transformedPage;
  }
  private getCtfComponents(ctfComponents: any) {
    const result: {
      ctfHero?: CtfHero;
      ctfCards?: CtfCard[];
      ctfHeaderAndText?: CtfHeaderAndText;
      ctfForm?: CtfForm;
      ctfCustomerLogo?: CtfCustomerLogo;
    } = {
      ctfCards: [],
    };

    for (const pageComponent of ctfComponents) {
      switch (pageComponent.sys.contentType.sys.id) {
        case this.CONTENT_IDS.hero:
          result.ctfHero = pageComponent;
          break;
        case this.CONTENT_IDS.card:
          result.ctfCards.push(pageComponent);
          break;
        case this.CONTENT_IDS.form:
          result.ctfForm = pageComponent.fields;
          break;
        case this.CONTENT_IDS.headerAndText:
          result.ctfHeaderAndText = pageComponent;
          break;
        case this.CONTENT_IDS.logos:
          result.ctfCustomerLogo = pageComponent.fields && {
            ...pageComponent.fields,
            logos: pageComponent.fields?.logo,
          };
          break;
      }
    }

    return result;
  }

  private mapHero(ctfHero: CtfEntry<CtfHero>) {
    const mapping = {
      subheader: ctfHero.fields.subheader,
      title: ctfHero.fields.title,
      image: {
        image_url: `${ctfHero.fields.backgroundImage?.fields?.file?.url}?w=1000`,
        alt: ctfHero.fields.backgroundImage?.fields?.description,
      },
      bordered: true,
    };

    return mapping;
  }
}
