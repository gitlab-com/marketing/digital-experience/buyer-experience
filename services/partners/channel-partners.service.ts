export function channelPartnersService(data: any[]) {
  const pageData = {
    components: [],
    next_steps_variant: null,
  };

  // Hero
  const heroData = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );

  const hero = {
    name: 'hero',
    data: {
      title: heroData[0].fields.title,
      text: heroData[0].fields.description,
      aos_animation: 'fade-down',
      aos_duration: 500,
      img_animation: 'zoom-out-left',
      img_animation_duration: 1600,
      primary_btn: {
        text: heroData[0].fields.primaryCta.fields.text,
        url: heroData[0].fields.primaryCta.fields.externalUrl,
        data_ga_name: heroData[0].fields.primaryCta.fields.dataGaName
          ? heroData[0].fields.primaryCta.fields.dataGaName
          : null,
        data_ga_location: heroData[0].fields.primaryCta.fields.dataGaLocation
          ? heroData[0].fields.primaryCta.fields.dataGaLocation
          : 'hero',
      },
      image: {
        url: heroData[0].fields.backgroundImage.fields.file.url,
        alt: '',
      },
    },
  };

  // Next Steps Variant
  const nextSteps = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'nextSteps',
  );

  pageData.next_steps_variant = nextSteps[0].fields.variant;

  // Create body
  const rawContent = data.filter(
    (obj) =>
      obj.sys.contentType.sys.id != 'eventHero' &&
      obj.sys.contentType.sys.id != 'nextSteps',
  );

  pageData.components = rawContent.map((section, index) => {
    if (section.sys.contentType.sys.id === 'customComponent') {
      return {
        name: section.fields.name,
        data: {
          scriptSrc: section.fields.data?.scriptSrc,
          dataConfig: { ...section.fields.data?.dataConfig },
        },
      };
    } else return {};
  });

  pageData.components.unshift(hero);
  return pageData;
}
