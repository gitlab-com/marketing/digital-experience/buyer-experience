import { CONTENT_TYPES } from '~/common/content-types';

interface CSSalesData {
  title?: string;
  description?: string;
  duoProHero?: any;
  duoEnterpriseHero?: any;
  resources?: any;
  duoProTextBlock?: any;
  duoEnterpriseTextBlock?: any;
  duoProForm?: any;
  duoEnterpriseForm?: any;
  form?: any;
  navigation?: any;
  nextSteps?: any;
  submittedHero?: any;
  formTabs?: any;
  duoTabs?: any;
  cards?: any[];
  duoCombinedForm?: any;
  duoCombinedHero?: any;
}

function resourcesCards(resources) {
  const cards = resources.card.map((card) => {
    return {
      header: card.fields.title,
      icon: {
        name: card.fields.iconName,
        variant: 'marketing',
        alt: `${card.fields.iconName} icon`,
      },
      image: card.fields.image.fields.file.url,
      link_text: card.fields.button.fields.text,
      href: card.fields.cardLink && card.fields.cardLink,
      data_ga_name:
        card.fields.cardLinkDataGaName && card.fields.cardLinkDataGaName,
      data_ga_location:
        card.fields.cardLinkDataGaLocation &&
        card.fields.cardLinkDataGaLocation,
      event_type: card.fields.subtitle && card.fields.subtitle,
    };
  });

  return {
    data: {
      title: resources.header && resources.header,
      column_size: 4,
      cards,
    },
  };
}

function heroBuilder(hero, id) {
  const {
    title,
    description,
    backgroundImage,
    video,
    primaryCta,
    customFields,
    subheader,
  } = hero;
  return {
    title,
    description,
    id,
    backgroundImage: backgroundImage.fields && {
      src: backgroundImage.fields.file.url,
      alt: backgroundImage.fields.file.title,
    },
    video: video && {
      url: video.fields.url,
    },
    icon: customFields?.icon && customFields?.icon,
    subheader,
    button: primaryCta && {
      text: primaryCta.fields.text,
      data_ga_name: primaryCta.fields.dataGaName,
      data_ga_location: primaryCta.fields.dataGaLocation,
    },
  };
}
function formTabBuilder(content) {
  return {
    formTabs: true,
    tabs: content?.tabs?.map((tab) => ({
      buttonText: tab.fields?.tabButtonText,
      tabId: tab.fields?.tabId,
      content: tab.fields?.tabPanelContent.reduce((acc, item) => {
        let itemContent = {};
        if (item.sys.contentType?.sys.id === 'headerAndText') {
          if (item.fields?.header) {
            itemContent.header = item.fields.header;
          }
          if (item.fields?.text) {
            itemContent.text = item.fields.text;
          }
        }

        if (
          item.sys.contentType?.sys.id === 'cardGroup' &&
          item.fields?.componentName === 'cards'
        ) {
          itemContent.cards = item.fields.card?.map((singleCard) => ({
            title: singleCard.fields?.title,
            text: singleCard.fields?.description,
            link: singleCard.fields?.cardLink,
            dataGaName: singleCard.fields?.cardLinkDataGaName,
            dataGaLocation: singleCard.fields?.cardLinkDataGaLocation,
          }));
        }

        if (
          item.sys.contentType?.sys.id === 'twoColumnBlock' &&
          item.fields?.componentName === 'form-accordion'
        ) {
          itemContent.formAccordion = {
            title: item.fields?.header,
            text: item.fields?.text,
            form: item.fields?.blockGroup[0]?.fields,
          };
        }

        if (item.sys.contentType?.sys.id === 'form') {
          itemContent.form = item.fields;
        }

        return { ...acc, ...itemContent };
      }, {}),
    })),
  };
}

function duoTabBuilder(content) {
  const tabs = content?.tabs?.map((tab) => ({
    buttonText: tab.fields?.tabButtonText,
    tabId: tab.fields?.tabId,
  }));
  return tabs;
}

function pageBuilder(page) {
  const result: CSSalesData = {};
  result.cards = [];

  for (const section of page) {
    const id = section.sys.contentType?.sys.id;
    switch (id) {
      case CONTENT_TYPES.HEADER_AND_TEXT:
        if (
          section.fields.componentName &&
          section.fields.componentName === 'gitlab-duo-pro-features'
        ) {
          result.duoProTextBlock = { ...section.fields };
        } else if (
          section.fields.componentName &&
          section.fields.componentName === 'gitlab-duo-enterprise-features'
        ) {
          result.duoEnterpriseTextBlock = { ...section.fields };
        }
        break;
      case CONTENT_TYPES.TAB_CONTROLS_CONTAINER:
        if (
          section.fields.componentName &&
          section.fields.componentName === 'form-tabs'
        ) {
          result.formTabs = { ...formTabBuilder(section.fields) };
        } else if (
          section.fields.componentName &&
          section.fields.componentName === 'gitlab-duo-tabs'
        ) {
          result.duoTabs = { ...duoTabBuilder(section.fields) };
        }
        break;
      case CONTENT_TYPES.HERO:
        if (
          section.fields.componentName &&
          section.fields.componentName === 'thank-you-hero'
        ) {
          result.submittedHero = {
            ...heroBuilder(section.fields, 'thank-you-hero'),
          };
        } else if (
          section.fields.componentName &&
          section.fields.componentName === 'gitlab-duo-enterprise-hero'
        ) {
          result.duoEnterpriseHero = {
            ...heroBuilder(section.fields, 'gitlab-duo-enterprise-hero'),
          };
        } else if (
          section.fields.componentName &&
          section.fields.componentName === 'gitlab-duo-pro-hero'
        ) {
          result.duoProHero = {
            ...heroBuilder(section.fields, 'gitlab-duo-pro-hero'),
          };
        } else {
          result.duoCombinedHero = {
            ...heroBuilder(section.fields, 'gitlab-duo-combined-hero'),
          };
        }

        break;
      case CONTENT_TYPES.CARD_GROUP:
        result.resources = { ...resourcesCards(section.fields) };
        break;
      case CONTENT_TYPES.FORM:
        if (
          section.fields.componentName &&
          section.fields.componentName === 'GitLab Duo Pro Form'
        ) {
          result.duoProForm = { ...section.fields };
        } else if (
          section.fields.componentName &&
          section.fields.componentName === 'GitLab Duo Enterprise Form'
        ) {
          result.duoEnterpriseForm = { ...section.fields };
        } else if (section.fields.componentName) {
          result.duoCombinedForm = { ...section.fields };
        }
        break;
      case CONTENT_TYPES.CARD:
        section?.fields ? result.cards.push(section?.fields) : '';
        break;
      case CONTENT_TYPES.NEXT_STEPS:
        result.nextSteps = section.fields.variant;
        break;
    }
  }

  return result;
}

function metadataHelper(data) {
  const seo = data[0].fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    noIndex: seo.noIndex,
  };
}

export function GDSalesHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageBuilder(pageContent);

  const metadata = metadataHelper(seoMetadata);
  const transformedData: CSSalesData = {
    ...metadata,
    ...cleanPagecontent,
  };
  return transformedData;
}
