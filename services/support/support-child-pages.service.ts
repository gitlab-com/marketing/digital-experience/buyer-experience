import { SupportChildData } from './support-interfaces';

export function supportChildDataHelper(data: any[]) {
  const supportData: SupportChildData = {
    supportHero: {
      title: undefined,
      content: undefined,
    },
    side_menu: {
      anchors: {
        text: '',
        data: [],
      },
      hyperlinks: {
        text: '',
        data: [],
      },
    },
    components: [],
  };

  // Take in raw data from Contentful.
  const heroObjects = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );
  const sideNavObjects = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'sideMenu',
  );
  const rawSections = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'headerAndText',
  );

  const createHero = (content: any) => {
    supportData.supportHero = {
      title: content.fields.title || null,
      content: content.fields.description || null,
    };
  };

  const createSideNav = (rawObj: any) => {
    const content = rawObj.fields;

    supportData.side_menu = {
      anchors: {
        text: content.anchorsText,
        data: content.anchors.map((anchor) => {
          const anchorData = {
            text: anchor.fields.linkText,
            href: anchor.fields.anchorLink,
            nodes: [],
          };

          if (anchor.fields.nodes) {
            anchorData.nodes = anchor.fields.nodes.nodes.map((node) => ({
              text: node.text,
              href: node.href,
            }));
          }

          return anchorData;
        }),
      },
      hyperlinks: {
        text: '',
        data: [],
      },
    };
  };

  const createSections = (rawSections: any[]) => {
    const sections = rawSections.map((section) => ({
      header: section.fields.header || null,
      headerId: section.fields.headerAnchorId || null,
      text: section.fields.text,
    }));
    supportData.components = sections;
  };

  // Build out the page structure (different for each page)
  if (heroObjects[0]) createHero(heroObjects[0]);
  if (sideNavObjects[0]) createSideNav(sideNavObjects[0]);
  createSections(rawSections);

  // Return data to Vue template
  return supportData;
}
