import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import {
  CtfEntry,
  CtfPage,
  CtfTheSourceArticlePage,
  CtfTheSourceCategory,
} from '~/models';
import { convertToCaseSensitiveLocale, getTagFromLocale } from '~/common/util';
import { THE_SOURCE_TYPES } from '~/common/constants';

export class TheSourceService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  async getArticleBySlug(
    slug: string,
  ): Promise<CtfEntry<CtfTheSourceArticlePage>> {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE,
      'fields.slug': slug,
      include: 10,
      locale,
    });

    if (items.length === 0) {
      throw new Error(`"The Source" Article not found: ${slug}`);
    }

    const [page] = items;

    return page;
  }

  async getLandingContent(): Promise<CtfEntry<CtfPage>> {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': 'the-source',
      include: 10,
      locale,
    });

    const [page] = items;

    return page as CtfPage;
  }

  async getMostRecentArticles(): Promise<CtfEntry<CtfTheSourceArticlePage>[]> {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const tag = getTagFromLocale(this.$ctx.i18n.locale);

    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE,
      order: '-fields.date',
      include: 10,
      limit: 5,
      locale,
      'metadata.tags.sys.id[in]': tag,
      'fields.type[ne]': THE_SOURCE_TYPES.GUIDE,
    });

    const featuredIndex = items.findIndex(
      (article) => article.fields.featured === true,
    );

    if (featuredIndex !== -1) {
      items.splice(featuredIndex, 1);
    }

    return items.slice(0, 3);
  }

  async getArticlesByCategory(
    categoryId: string,
    limit?: number = 100,
  ): Promise<CtfEntry<CtfTheSourceArticlePage>[]> {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const tag = getTagFromLocale(this.$ctx.i18n.locale);

    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE,
      order: '-fields.date',
      include: 10,
      limit,
      locale,
      'fields.category.sys.id': categoryId,
      'metadata.tags.sys.id[in]': tag,
    });

    return items;
  }

  async getArticlesByAuthor(
    authorId: string,
  ): Promise<CtfEntry<CtfTheSourceArticlePage>[]> {
    const tag = getTagFromLocale(this.$ctx.i18n.locale);
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const commonOptions = {
      content_type: CONTENT_TYPES.THE_SOURCE,
      order: '-fields.date',
      include: 10,
      locale,
      'metadata.tags.sys.id[in]': tag,
    };

    // Contentful does not have an "OR" filter
    const [authorResults, speakerResults] = await Promise.all([
      getClient().getEntries({
        ...commonOptions,
        'fields.author.sys.id': authorId,
      }),
      getClient().getEntries({
        ...commonOptions,
        'fields.speakers.sys.id': authorId,
      }),
    ]);

    return [...authorResults.items, ...speakerResults.items];
  }

  async getFeaturedArticle(): Promise<CtfEntry<CtfTheSourceArticlePage>> {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE,
      order: '-fields.date',
      'fields.featured': true,
      limit: 1,
      'metadata.tags.sys.id[in]': getTagFromLocale(this.$ctx.i18n.locale),
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });

    if (items.length === 0) {
      return null;
    }

    const [page] = items;

    return page;
  }

  async getCategories(): Promise<CtfEntry<CtfTheSourceCategory>[]> {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE_CATEGORY,
      order: 'fields.order',
      include: 10,
      locale,
    });

    return items;
  }

  async getPreviewContent(
    id: number,
    locale,
    config,
  ): Promise<CtfEntry<CtfTheSourceArticlePage>> {
    const { items } = await getClient(config).getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE,
      'sys.id': id,
      include: 10,
      locale,
    });

    if (items.length === 0) {
      throw new Error(`Article not found: ${id}`);
    }

    const [page] = items;

    return page;
  }

  async getAuthor(slug: string) {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);

    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE_AUTHOR,
      'fields.slug': slug,
      include: 10,
      locale,
    });

    if (!items.length) {
      throw new Error(`The Source: Author not found in Contentful ${slug}`);
    }

    const [authorPage] = items;

    return authorPage;
  }

  async getCategory(slug: string) {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);

    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.THE_SOURCE_CATEGORY,
      'fields.slug': slug,
      include: 10,
      locale,
    });

    if (!items.length) {
      throw new Error(`The Source: Category not found in Contentful ${slug}`);
    }

    const [categoryPage] = items;

    return categoryPage;
  }
}
